#include "AccountPointsMgr.h"
#include "DatabaseEnv.h"
#include "Errors.h"
#include "GameTime.h"
#include "Log.h"
#include "Timer.h"

AccountPointsMgr* AccountPointsMgr::instance()
{
    static AccountPointsMgr instance;
    return &instance;
}

inline void AddPointInfo(AccountPointsMap& theMap, Field* fields)
{
    AccountPointsInfo info;
    info.points = fields[1].GetUInt32();
    info.lastChanged = fields[2].GetUInt64();
    theMap.emplace(fields[0].GetUInt32(), info);
}

void AccountPointsMgr::Load()
{
    uint32 oldMSTime = getMSTime();

    for (uint8 i = 0; i < ACCOUNT_POINTS_MAX; ++i)
    {
        m_points[i].clear();
        UpdateType(AccountPointsType(i), true, true);
    }

    TC_LOG_INFO("server.loading", ">> Loaded AccountPointsMgr in %u ms", GetMSTimeDiffToNow(oldMSTime));
}

void AccountPointsMgr::Update()
{
    for (uint8 i = 0; i < ACCOUNT_POINTS_MAX; ++i)
        UpdateType(AccountPointsType(i));
}

void AccountPointsMgr::UpdateType(AccountPointsType type, bool force, bool load)
{
    ASSERT(type < ACCOUNT_POINTS_MAX);
    if (load)
        ASSERT(m_points[type].empty());

    if (m_lastUpdate[type] + (force ? MIN_POINTS_UPDATE_DELAY : POINTS_UPDATE_DELAY) > GameTime::GetGameTime())
        return;

    std::lock_guard<std::mutex> guard(m_lock);
    PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(AccountPointsQueries[type][0]);
    stmt->setUInt64(0, m_lastUpdate[type]);
    PreparedQueryResult result = LoginDatabase.Query(stmt);
    if (!result)
    {
        if (!load)
            m_lastUpdate[type] = GameTime::GetGameTime();

        return;
    }

    do
    {
        Field *fields = result->Fetch();

        if (load)
        {
            AddPointInfo(m_points[type], fields);
            continue;
        }

        auto it = m_points[type].find(fields[0].GetUInt32());
        if (it != m_points[type].end())
        {
            it->second.points = fields[1].GetUInt32();
            it->second.lastChanged = fields[2].GetUInt64();
        }
        else
            AddPointInfo(m_points[type], fields);
    } while (result->NextRow());

    m_lastUpdate[type] = GameTime::GetGameTime();
}

bool AccountPointsMgr::GetPoints(AccountPointsType type, uint32 account_id, AccountPointsInfo& result)
{
    ASSERT(type < ACCOUNT_POINTS_MAX);

    std::lock_guard<std::mutex> guard(m_lock);
    auto it = m_points[type].find(account_id);
    if (it != m_points[type].end())
    {
        result = it->second;
        return true;
    }
    else
        return false;
}

void AccountPointsMgr::SetPoints(AccountPointsType type, uint32 account_id, uint32 points)
{
    ASSERT(type < ACCOUNT_POINTS_MAX);

    std::lock_guard<std::mutex> guard(m_lock);
    auto it = m_points[type].find(account_id);

    // add
    if (it == m_points[type].end())
    {
        AccountPointsInfo info;
        info.points = points;
        info.lastChanged = GameTime::GetGameTime();

        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(AccountPointsQueries[type][2]);
        stmt->setUInt32(0, account_id);
        stmt->setUInt32(1, points);
        stmt->setUInt64(2, GameTime::GetGameTime());
        LoginDatabase.Execute(stmt);
    }
    // change
    else
    {
        it->second.points = points;
        it->second.lastChanged = GameTime::GetGameTime();

        PreparedStatement* stmt = LoginDatabase.GetPreparedStatement(AccountPointsQueries[type][1]);
        stmt->setUInt32(0, points);
        stmt->setUInt64(1, GameTime::GetGameTime());
        stmt->setUInt32(2, account_id);
        LoginDatabase.Execute(stmt);
    }
}
