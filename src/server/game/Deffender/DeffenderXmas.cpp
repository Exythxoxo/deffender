#include "Chat.h"
#include "DeffenderXmas.h"
#include "LootMgr.h"
#include "Player.h"

TC_GAME_API bool Deffender::Xmas::GenerateGift(Player* player)
{
    uint32 count = 1;
    uint32 noSpaceForCount = 0;
    ItemPosCountVec dest;
    InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, DEFFENDER_GIFT_ITEM, count, &noSpaceForCount);
    if (msg != EQUIP_ERR_OK)
        count -= noSpaceForCount;

    if (count == 0 || dest.empty())
    {
        /// @todo Send to mailbox if no space
        ChatHandler(player->GetSession()).PSendSysMessage("You don't have any space in your bags.");
        return false;
    }

    if (Item* present = player->StoreNewItem(dest, DEFFENDER_GIFT_ITEM, true))
    {
        player->SendNewItem(present, count, true, false);
        present->SetBinding(true);
        present->m_lootGenerated = true;
        present->loot.FillLoot(Deffender::Xmas::DEFFENDER_GIFT_LOOT, LootTemplates_Item, player, true, false, LOOT_MODE_KEEP_CURRENCY_IN_LOOT);
        return true;
    }
    return false;
}
