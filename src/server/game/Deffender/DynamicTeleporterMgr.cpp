#include "DatabaseEnv.h"
#include "DynamicTeleporterMgr.h"
#include "Log.h"
#include "Timer.h"

DynamicTeleporterMgr* DynamicTeleporterMgr::instance()
{
    static DynamicTeleporterMgr instance;
    return &instance;
}

bool DynamicTeleporterMgr::IsLoaded()
{
    SharedLock guard(m_loadLock);
    return m_loaded;
}

MenuVisibility DynamicTeleporterMgr::GetMinVisibility() const
{
    return m_minVisibility;
}

void DynamicTeleporterMgr::Load()
{
    if (m_loaded)
        return;

    m_minVisibility = MENU_VISIBILITY_MAX;
    uint32 oldMSTime = getMSTime();

    QueryResult result = WorldDatabase.Query(
        //        0         1          2       3       4      5     6       7           8           9           10         11       12         13         14
        "SELECT entry, menu_parent, menu_sub, icon, faction, name, map, position_x, position_y, position_z, position_o, realm_id, quest_id, event_id, visibility "
        "FROM dynamic_teleporter ORDER BY entry"
    );

    if (!result)
        return;

    do
    {
        Field *fields = result->Fetch();

        TeleportData td;
        td.entry = fields[0].GetUInt32();
        td.menu_sub = fields[2].GetInt32();
        td.icon = std::min(uint8(9), fields[3].GetUInt8());
        td.faction = fields[4].GetUInt32();
        td.name = fields[5].GetString();
        td.realmId = fields[11].GetUInt32();
        td.questId = fields[12].GetUInt32();
        td.eventId = fields[13].GetInt32();
        td.visibility = MenuVisibility(fields[14].GetUInt8());

        if (fields[1].GetUInt32() == 0 && m_minVisibility > td.visibility)
            m_minVisibility = td.visibility;

        if (td.menu_sub < 0)
            m_teleportLocs[td.entry] = WorldLocation(fields[6].GetUInt32(), fields[7].GetFloat(), fields[8].GetFloat(), fields[9].GetFloat(), fields[10].GetFloat());

        m_teleportData[fields[1].GetUInt32()].push_back(td);
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u Dynamic Teleporter locations in %u ms", m_teleportData.size(), GetMSTimeDiffToNow(oldMSTime));
    m_loaded = true;
}

void DynamicTeleporterMgr::Reload()
{
    UniqueLock guard(m_loadLock);
    m_loaded = false;
    m_teleportData.clear();
    m_teleportLocs.clear();
    Load();
}

const WorldLocation* DynamicTeleporterMgr::FindLocation(uint32 id) const
{
    auto res = m_teleportLocs.find(id);
    return res != m_teleportLocs.end() ? &res->second : nullptr;
}

bool DynamicTeleporterMgr::HasSubmenu(uint32 id) const
{
    return m_teleportData.find(id) != m_teleportData.end();
}

const std::vector<TeleportData>& DynamicTeleporterMgr::GetSubmenuData(uint32 id) const
{
    return m_teleportData.at(id);
}
