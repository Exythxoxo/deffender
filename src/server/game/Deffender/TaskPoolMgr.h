#include <functional>
#include <boost/asio/io_service.hpp>
#include <boost/asio/deadline_timer.hpp>
#include "Common.h"
#include "World.h"

/**
 * New tasks can be registered during world startup - World::SetInitialWorldSettings or WorldScript::OnStartup
 * Borrow and Return should only be called from worldserver's Main method
 */
class TC_GAME_API TaskPoolMgr
{
public:
    using callback_type = std::function<void()>;

private:
    class TaskHolder
    {
        callback_type _callback;
        time_t _frequency;
        boost::asio::deadline_timer* _timer;

        void _Schedule();

    public:
        TaskHolder() {}
        template<typename Callback>
        TaskHolder(Callback&& callback, time_t frequency, boost::asio::io_service& ioService) :
            _callback(callback), _frequency(frequency), _timer(new boost::asio::deadline_timer(ioService)) { _Schedule(); }

        ~TaskHolder()
        {
            delete _timer;
        }

        void Run(boost::system::error_code const& error);
    };

    std::deque<TaskHolder> _taskStore;
    boost::asio::io_service* _ioService;

    TaskPoolMgr() : _ioService(nullptr) {}

public:
    static TaskPoolMgr* instance();

    TaskPoolMgr(TaskPoolMgr& other) = delete;

    void Borrow(boost::asio::io_service* ioService);
    void Return();

    // Register callable of type TaskPoolMgr::callback_type called every frequency
    template<typename TaskType>
    void Register(TaskType&& task, time_t frequency)
    {
        if (_ioService)
            _taskStore.emplace_back(std::forward(task), frequency, *_ioService);
    }
};

#define sTaskPoolMgr TaskPoolMgr::instance()
