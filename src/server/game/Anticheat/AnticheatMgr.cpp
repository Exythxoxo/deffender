/*
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "AccountMgr.h"
#include "AnticheatMgr.h"
#include "AnticheatScripts.h"
#include "AnticheatData.h"
#include "MapManager.h"
#include "Transaction.h"
#include "Player.h"
#include "Spell.h"
#include "Auras/SpellAuras.h"
#include "Transport.h"
#include "Vehicle.h"

#define CLIMB_ANGLE 1.9f

AnticheatMgr::AnticheatMgr()
{
}

AnticheatMgr::~AnticheatMgr()
{
    m_Players.clear();
}

AnticheatMgr* AnticheatMgr::instance()
{
    static AnticheatMgr instance;
    return &instance;
}

void AnticheatMgr::AirWalkHackDetection(Player* player, MovementInfo movementInfo, bool isFlying)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & AIR_WALK_HACK_DETECTION) == 0)
        return;

    if (player->IsInWater())
        return;

    float z = movementInfo.pos.GetPositionZ();
    if (z == player->GetPositionZ())
    if (player->GetDistance2d(movementInfo.pos.m_positionX, movementInfo.pos.m_positionY) > 2.0f)
    {
        float x = player->GetPositionX();
        float y = player->GetPositionY();
        bool hack = player->GetMap()->isInLineOfSight(x, y, z, x, y, z-1.5f, player->GetPhaseMask(), LINEOFSIGHT_ALL_CHECKS, VMAP::ModelIgnoreFlags::Nothing);
        if (hack)
        {
            float height = player->GetMap()->GetHeight(player->GetPositionX(), player->GetPositionY(), z);
            if (isFlying) // if player has enabled flying, check if he is not under map && staying on the same height
                hack = height < INVALID_HEIGHT;
            else // else just check if we are above ground or not (1.5yd tolerance)
                hack = height < z - 1.5f;
        }

        if (hack)
        {
            BuildReport(player,AIR_WALK_HACK_REPORT);
            TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Air Walk-Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
        }
    }
}

void AnticheatMgr::JumpHackDetection(Player* player, uint32 opcode)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & JUMP_HACK_DETECTION) == 0)
        return;

    uint32 key = player->GetGUID().GetCounter();
    uint32 lastopcode = m_Players[key].GetLastOpcode();
                                        // falling down & pressed jump
    MovementInfo lastInfo = m_Players[key].GetLastMovementInfo();

    if ((lastopcode == MSG_MOVE_JUMP || lastInfo.jump.zspeed < -7.0f) && opcode == MSG_MOVE_JUMP)
    {
        BuildReport(player,MULTI_JUMP_HACK_REPORT);
        TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Multi Jump-Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
    }

    /*if (movementInfo.jump.zspeed < -8.0f)
    {
        BuildReport(player,SUPER_JUMP_HACK_REPORT);
        TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Super Jump-Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
    }*/
}

void AnticheatMgr::WalkOnWaterHackDetection(Player* player, MovementInfo /* movementInfo */)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & WALK_WATER_HACK_DETECTION) == 0)
        return;

    uint32 key = player->GetGUID().GetCounter();
    if (!m_Players[key].GetLastMovementInfo().HasMovementFlag(MOVEMENTFLAG_WATERWALKING))
        return;

    // if we are a ghost we can walk on water
    if (!player->IsAlive())
        return;

    if (player->HasAuraType(SPELL_AURA_FEATHER_FALL) ||
        player->HasAuraType(SPELL_AURA_SAFE_FALL) ||
        player->HasAuraType(SPELL_AURA_WATER_WALK))
        return;

    TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Walk on Water - Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
    BuildReport(player,WALK_WATER_HACK_REPORT);

}

void AnticheatMgr::FlyHackDetection(Player* player, MovementInfo movementInfo)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & FLY_HACK_DETECTION) == 0)
        return;

    bool hack = true;
    uint32 key = player->GetGUID().GetCounter();
    if (!m_Players[key].GetLastMovementInfo().HasMovementFlag(MOVEMENTFLAG_FLYING))
        hack = false;

    bool fly = false;
    if (player->HasAuraType(SPELL_AURA_FLY) ||
        player->HasAuraType(SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED) ||
        player->HasAuraType(SPELL_AURA_MOD_INCREASE_FLIGHT_SPEED) ||
        (player->GetVehicle() && player->GetVehicle()->GetBase()->CanFly()))
    {
        fly = true;
        hack = false;
    }

    if (hack)
    {
        TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Fly-Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
        BuildReport(player,FLY_HACK_REPORT);
    }
    else AirWalkHackDetection(player, movementInfo, fly);
}

void AnticheatMgr::TeleportPlaneHackDetection(Player* player, MovementInfo movementInfo)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & TELEPORT_PLANE_HACK_DETECTION) == 0)
        return;

    uint32 key = player->GetGUID().GetCounter();

    if (m_Players[key].GetLastMovementInfo().pos.GetPositionZ() != 0 ||
        movementInfo.pos.GetPositionZ() != 0)
        return;

    if (movementInfo.HasMovementFlag(MOVEMENTFLAG_FALLING))
        return;

    //DEAD_FALLING was deprecated
    //if (player->getDeathState() == DEAD_FALLING)
    //    return;
    float x, y, z;
    player->GetPosition(x, y, z);
    float ground_Z = player->GetMap()->GetHeight(x, y, z);
    float z_diff = fabs(ground_Z - z);

    // we are not really walking there
    if (z_diff > 1.0f)
    {
        TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Teleport To Plane - Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
        BuildReport(player,TELEPORT_PLANE_HACK_REPORT);
    }
}

bool AnticheatMgr::IsValidOpCode(uint32 opcode)
{
    switch (opcode)
    {
    case MSG_MOVE_START_TURN_LEFT:
    case MSG_MOVE_START_TURN_RIGHT:
    case MSG_MOVE_STOP_TURN:
    case MSG_MOVE_SET_FACING:
        return false;
    default:
        return true;
    }
}

void AnticheatMgr::StartHackDetection(Player* player, MovementInfo movementInfo, uint32 opcode)
{
    if (!sWorld->getBoolConfig(CONFIG_ANTICHEAT_ENABLE))
        return;

    if (player->GetSession()->GetSecurity() > SEC_GM_PLAY_ACC)
        return;

    uint32 key = player->GetGUID().GetCounter();
    AnticheatData& data = m_Players[key];
    // if flag is violated by client - it is already removed by TC

    bool controllingVehicle = player->GetVehicle() && (player->GetVehicle()->GetSeatForPassenger(player)->m_flags & VEHICLE_SEAT_FLAG_CAN_CONTROL);
    if (player->IsInFlight() || (player->HasUnitMovementFlag(MOVEMENTFLAG_ONTRANSPORT) && !player->GetTransport() && !controllingVehicle))
    {
        data.SetLastMovementInfo(movementInfo);
        // ignored opcodes by jump hack detection
        if (IsValidOpCode(opcode))
            data.SetLastOpcode(opcode);
        return;
    }

    FlyHackDetection(player,movementInfo);
    WalkOnWaterHackDetection(player,movementInfo);
    TeleportPlaneHackDetection(player, movementInfo);
    ClimbHackDetection(player,movementInfo);
    SpeedHackDetection(player,movementInfo);
    // ignored opcodes by jump hack detection
    if (IsValidOpCode(opcode))
    {
        JumpHackDetection(player, opcode);
        data.SetLastOpcode(opcode);
    }
    data.SetLastMovementInfo(movementInfo);
}

// basic detection
void AnticheatMgr::ClimbHackDetection(Player *player, MovementInfo movementInfo)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & CLIMB_HACK_DETECTION) == 0)
        return;

    // in this case we don't care if they are "legal" flags, they are handled in another parts of the Anticheat Manager.
    if (player->IsInWater() ||
        player->IsFlying() ||
        player->IsFalling())
        return;

    Position playerPos;
    float deltaZ = fabs(playerPos.GetPositionZ() - movementInfo.pos.GetPositionZ());
    float deltaXY = movementInfo.pos.GetExactDist2d(&playerPos);

    float angle = Position::NormalizeOrientation(tan(deltaZ/deltaXY));

    if (angle > CLIMB_ANGLE)
    {
        TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Climb-Hack detected player GUID (low) %u", player->GetGUID().GetCounter());
        BuildReport(player,CLIMB_HACK_REPORT);
    }
}

void AnticheatMgr::UpdateSpeedHistory(Player* player, float speed)
{
    m_Players[player->GetGUID().GetCounter()].UpdateSpeedHistory(speed);
}

void AnticheatMgr::SpeedHackDetection(Player* player, MovementInfo movementInfo)
{
    if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & SPEED_HACK_DETECTION) == 0)
        return;

    if (player->GetVehicle())
    {
        if (player->GetVehicle()->GetBase()->IsFalling())
            return;
    }
    else if (player->IsFalling())
        return;

    uint32 key = player->GetGUID().GetCounter();

    AnticheatData& data = m_Players[key];

    uint8 moveType = 0;

    // we need to know HOW is the player moving
    // TO-DO: Should we check the incoming movement flags?
    if (player->HasUnitMovementFlag(MOVEMENTFLAG_SWIMMING))
        moveType = MOVE_SWIM;
    else if (player->IsFlying())
        moveType = MOVE_FLIGHT;
    else if (player->HasUnitMovementFlag(MOVEMENTFLAG_WALKING))
        moveType = MOVE_WALK;
    else
        moveType = MOVE_RUN;

                                // player might have been moved by core                    // last movement from player
    float distance2D = std::min(movementInfo.pos.GetExactDist2d(player), movementInfo.pos.GetExactDist2d(&data.GetLastMovementInfo().pos));

    // how many yards the player can do in one sec.
    float speedRate;
    if (Unit* vehicle = player->GetVehicleBase())
        speedRate = vehicle->IsFlying() ? vehicle->GetSpeed(MOVE_FLIGHT) : vehicle->GetSpeed(MOVE_RUN);
    else
        speedRate = std::max(player->GetSpeed(UnitMoveType(moveType)), movementInfo.jump.xyspeed);

    float transportDiff;
    if (movementInfo.transport.guid && data.GetLastMovementInfo().transport.guid && player->GetTransport())
        transportDiff = std::max(movementInfo.transport.pos.GetExactDist2d(player->GetTransport()), movementInfo.transport.pos.GetExactDist2d(&data.GetLastMovementInfo().transport.pos));
    else
        transportDiff = 0;

    // save current speed to history
    data.UpdateSpeedHistory(speedRate);
    // select highest history speed for use
    speedRate = data.GetMaxSpeed() + transportDiff;

    // how long the player took to move to here.
    uint32 timeDiff = getMSTimeDiff(data.GetLastMovementInfo().time,movementInfo.time);

    if (!timeDiff)
        timeDiff = 1;
    else if (timeDiff > 1500)
        timeDiff = 1500;


    // this is the distance doable by the player in 1 ms, using the time done to move to this point.
    float clientSpeedRate = (uint32)distance2D / (float)timeDiff;

    if (clientSpeedRate * sWorld->getIntConfig(CONFIG_ANTICHEAT_MULTIPLIER) > speedRate)
    {
        if (distance2D < clientSpeedRate * 1500 && timeDiff < 1500)
            BuildReport(player,SPEED_HACK_REPORT);
        else
            BuildReport(player,TELE_HACK_REPORT);


        // show debug info for speed hack to GMs while debug is on
        if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & ANTICHEAT_SHOW_DEBUG_INFO))
        {
            std::string str = "player " + player->GetName() + " timeDiff = " + std::to_string(getMSTimeDiff(data.GetLastMovementInfo().time,movementInfo.time))
                    + ", distance2D  = " + std::to_string(distance2D) + ", speed = "
                        /* distance doable in one second */
                    + std::to_string(clientSpeedRate * 1000) + ", allowed = " + std::to_string(speedRate);
            WorldPacket data(SMSG_NOTIFICATION, (str.size()+1));
            data << str;
            sWorld->SendGlobalGMMessage(&data);
        }
        TC_LOG_DEBUG("entities.player.character", "AnticheatMgr:: Speed-Hack detected player GUID (low) %u",player->GetGUID().GetCounter());
    }
}

void AnticheatMgr::InterruptHackDetection(Player* caster, Spell* spell)
{
    if (!(sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & PQR_HACK_DETECTION))
        return;

    int32 timeToCast = spell->GetCastTimeDelay();
    int32 castTime = spell->GetCastTime();
    AnticheatData& data = m_Players[caster->GetGUID().GetCounter()];
    data.UpdatePQRInterruptLogs(castTime, castTime - timeToCast, timeToCast, static_cast<uint32>(time(nullptr)), spell->GetId());
}

void AnticheatMgr::DispellHackDetection(Player* caster, Aura* aura)
{
    if (!(sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & PQR_HACK_DETECTION))
        return;

    int32 delay = aura->GetMaxDuration() - aura->GetDuration();
    int32 duration = aura->GetMaxDuration();
    AnticheatData& data = m_Players[caster->GetGUID().GetCounter()];
    data.UpdatePQRDispellLogs(delay, duration, static_cast<uint32>(time(nullptr)), aura->GetId());
}

void AnticheatMgr::StartScripts()
{
    new AnticheatScripts();
}

void AnticheatMgr::HandlePlayerLogin(Player* player)
{
    // we must delete this to prevent errors in case of crash
    LogsDatabase.PExecute("DELETE FROM players_reports_status WHERE guid=%u",player->GetGUID().GetCounter());
    // we initialize the pos of lastMovementPosition var.
    m_Players[player->GetGUID().GetCounter()].SetPosition(player->GetPositionX(),player->GetPositionY(),player->GetPositionZ(),player->GetOrientation());
    QueryResult resultDB = LogsDatabase.PQuery("SELECT * FROM daily_players_reports WHERE guid=%u;",player->GetGUID().GetCounter());

    if (resultDB)
        m_Players[player->GetGUID().GetCounter()].SetDailyReportState(true);
}

void AnticheatMgr::HandlePlayerLogout(Player* player)
{
    // TO-DO Make a table that stores the cheaters of the day, with more detailed information.

    // We must also delete it at logout to prevent have data of offline players in the db when we query the database (IE: The GM Command)
    LogsDatabase.PExecute("DELETE FROM players_reports_status WHERE guid=%u",player->GetGUID().GetCounter());
    // Delete not needed data from the memory.
    m_Players.erase(player->GetGUID().GetCounter());
}

void AnticheatMgr::SavePlayerData(Player* player)
{
    AnticheatData& data = m_Players[player->GetGUID().GetCounter()];
    LogsDatabase.PExecute("REPLACE INTO players_reports_status (guid,average,total_reports,speed_or_tele_reports,fly_reports,jump_reports,waterwalk_reports,teleportplane_reports,climb_reports,air_walk_reports,creation_time)"
                          " VALUES (%u,%f,%u,%u,%u,%u,%u,%u,%u,%u,%u);",player->GetGUID().GetCounter(), data.GetAverage(),data.GetTotalReports(),
                          data.GetTypeReports(SPEED_HACK_REPORT)+data.GetTypeReports(TELE_HACK_REPORT),data.GetTypeReports(FLY_HACK_REPORT),
                          data.GetTypeReports(MULTI_JUMP_HACK_REPORT),data.GetTypeReports(WALK_WATER_HACK_REPORT),data.GetTypeReports(TELEPORT_PLANE_HACK_REPORT),
                          data.GetTypeReports(CLIMB_HACK_REPORT),data.GetTypeReports(AIR_WALK_HACK_REPORT),data.GetCreationTime());
    SavePQRData(player);
}

uint32 AnticheatMgr::GetTotalReports(uint32 lowGUID)
{
    return m_Players[lowGUID].GetTotalReports();
}

float AnticheatMgr::GetAverage(uint32 lowGUID)
{
    return m_Players[lowGUID].GetAverage();
}

uint32 AnticheatMgr::GetTypeReports(uint32 lowGUID, uint8 type)
{
    return m_Players[lowGUID].GetTypeReports(type);
}

bool AnticheatMgr::MustCheckTempReports(uint8 type)
{
    switch(type)
    {

    case MULTI_JUMP_HACK_REPORT:
    case SUPER_JUMP_HACK_REPORT:
    case TELE_HACK_REPORT:
        return false;
    default:
        return true;
    }
}

std::string AnticheatMgr::GetReportName(uint8 reportType)
{
    std::string name = "";
    switch(reportType)
    {
    case SPEED_HACK_REPORT:
        name += "speed";
        break;
    case FLY_HACK_REPORT:
        name += "fly";
        break;
    case WALK_WATER_HACK_REPORT:
        name += "water walk";
        break;
    case MULTI_JUMP_HACK_REPORT:
        name += "multi jump";
        break;
    case TELEPORT_PLANE_HACK_REPORT:
        name += "teleport to plane";
        break;
    case CLIMB_HACK_REPORT:
        name += "climb";
        break;
    case INTERRUPT_HACK_REPORT:         // cannot be automatically banned
        name += "interrupt";
        break;
    case AIR_WALK_HACK_REPORT:          // some errors while walking on gameobjects - need fixed vmap height calculation
        name += "air walk";
        break;
    case SUPER_JUMP_HACK_REPORT:
        name += "super jump";
        break;
    case TELE_HACK_REPORT:
        name += "teleport";
    default:
        break;
    }
    name += " hack!";
    return name;
}

bool AnticheatMgr::CanBeAutomaticallyBanned(uint8 reportType)
{
    switch(reportType)
    {
    case SPEED_HACK_REPORT:
    case FLY_HACK_REPORT:
    case MULTI_JUMP_HACK_REPORT:
    case TELEPORT_PLANE_HACK_REPORT:
    case CLIMB_HACK_REPORT:
    case SUPER_JUMP_HACK_REPORT:
        return true;
    default:
        return false;
    }
}

void AnticheatMgr::DebugLog(Player* player, uint8 reportType)
{
    std::string banmsg = "You have been banned for ";
    banmsg += GetReportName(reportType);
    player->GetSession()->SendNotification("%s",banmsg.c_str());
}

void AnticheatMgr::BuildReport(Player* player,uint8 reportType)
{
    uint32 key = player->GetGUID().GetCounter();

    if (MustCheckTempReports(reportType))
    {
        uint32 actualTime = getMSTime();

        if (!m_Players[key].GetTempReportsTimer(reportType))
            m_Players[key].SetTempReportsTimer(actualTime,reportType);

        if (getMSTimeDiff(m_Players[key].GetTempReportsTimer(reportType),actualTime) < 3000)
        {
            m_Players[key].SetTempReports(m_Players[key].GetTempReports(reportType)+1,reportType);

            if (m_Players[key].GetTempReports(reportType) < 3)
                return;
        } else
        {
            m_Players[key].SetTempReportsTimer(actualTime,reportType);
            m_Players[key].SetTempReports(1,reportType);
            return;
        }
    }

    // generating creationTime for average calculation
    if (!m_Players[key].GetTotalReports())
        m_Players[key].SetCreationTime(getMSTime());

    // increasing total_reports
    m_Players[key].SetTotalReports(m_Players[key].GetTotalReports()+1);
    // increasing specific cheat report
    m_Players[key].SetTypeReports(reportType,m_Players[key].GetTypeReports(reportType)+1);

    // diff time for average calculation
    uint32 diffTime = getMSTimeDiff(m_Players[key].GetCreationTime(),getMSTime()) / IN_MILLISECONDS;

    if (diffTime > 0)
    {
        // Average == Reports per second
        float average = float(m_Players[key].GetTotalReports()) / float(diffTime);
        m_Players[key].SetAverage(average);
    }

    if (sWorld->getIntConfig(CONFIG_ANTICHEAT_MAX_REPORTS_FOR_DAILY_REPORT) < m_Players[key].GetTotalReports())
    {
        if (!m_Players[key].GetDailyReportState())
        {
            LogsDatabase.PExecute("REPLACE INTO daily_players_reports (guid,average,total_reports,speed_or_tele_reports,fly_reports,jump_reports,waterwalk_reports,teleportplane_reports,climb_reports,air_walk_reports,creation_time)"
                                  " VALUES (%u,%f,%u,%u,%u,%u,%u,%u,%u,%u,%u);",player->GetGUID().GetCounter(),m_Players[player->GetGUID().GetCounter()].GetAverage(),m_Players[player->GetGUID().GetCounter()].GetTotalReports(),
                    m_Players[player->GetGUID().GetCounter()].GetTypeReports(SPEED_HACK_REPORT)+m_Players[player->GetGUID().GetCounter()].GetTypeReports(TELE_HACK_REPORT),m_Players[player->GetGUID().GetCounter()].GetTypeReports(FLY_HACK_REPORT),
                    m_Players[player->GetGUID().GetCounter()].GetTypeReports(MULTI_JUMP_HACK_REPORT),m_Players[player->GetGUID().GetCounter()].GetTypeReports(WALK_WATER_HACK_REPORT),m_Players[player->GetGUID().GetCounter()].GetTypeReports(TELEPORT_PLANE_HACK_REPORT),
                    m_Players[player->GetGUID().GetCounter()].GetTypeReports(CLIMB_HACK_REPORT),m_Players[player->GetGUID().GetCounter()].GetTypeReports(AIR_WALK_HACK_DETECTION),m_Players[player->GetGUID().GetCounter()].GetCreationTime());
            m_Players[key].SetDailyReportState(true);
        }
    }

    if (m_Players[key].GetTotalReports() >= sWorld->getIntConfig(CONFIG_ANTICHEAT_REPORTS_INGAME_NOTIFICATION))
    {
        if ((sWorld->getIntConfig(CONFIG_ANTICHEAT_DETECTIONS_ENABLED) & ANTICHEAT_AUTO_BAN) && CanBeAutomaticallyBanned(reportType))
        {
            std::string acc;
            sAccountMgr->GetName(player->GetSession()->GetAccountId(), acc);
            sWorld->BanAccount(BAN_ACCOUNT, acc, MONTH, GetReportName(reportType), "Anticheat");
            player->GetSession()->KickPlayer();
        }
        else
        {
            // display warning at the center of the screen, hacky way?
            std::string str = "|cFFFFFC00[AC]|cFF00FFFF[|cFF60FF00" + std::string(player->GetName().c_str()) + "|cFF00FFFF] Possible " + GetReportName(reportType) + ".";
            WorldPacket data(SMSG_NOTIFICATION, (str.size()+1));
            data << str;
            sWorld->SendGlobalGMMessage(&data);
        }
    }
}

void AnticheatMgr::AnticheatGlobalCommand(ChatHandler* handler)
{
    // MySQL will sort all for us, anyway this is not the best way we must only save the anticheat data not whole player's data!.
    ObjectAccessor::SaveAllPlayers();

    QueryResult resultDB = LogsDatabase.Query("SELECT guid,average,total_reports FROM players_reports_status WHERE total_reports != 0 ORDER BY average ASC LIMIT 3;");
    if (!resultDB)
    {
        handler->PSendSysMessage("No players found.");
        return;
    } else
    {
        handler->SendSysMessage("=============================");
        handler->PSendSysMessage("Players with the lowest averages:");
        do
        {
            Field *fieldsDB = resultDB->Fetch();

            uint32 guid = fieldsDB[0].GetUInt32();
            float average = fieldsDB[1].GetFloat();
            uint32 total_reports = fieldsDB[2].GetUInt32();

            if (Player* player = ObjectAccessor::FindPlayerByLowGUID(guid))
                handler->PSendSysMessage("Player: %s Average: %f Total Reports: %u",player->GetName().c_str(),average,total_reports);

        } while (resultDB->NextRow());
    }

    resultDB = LogsDatabase.Query("SELECT guid,average,total_reports FROM players_reports_status WHERE total_reports != 0 ORDER BY total_reports DESC LIMIT 3;");

    // this should never happen
    if (!resultDB)
    {
        handler->PSendSysMessage("No players found.");
        return;
    } else
    {
        handler->SendSysMessage("=============================");
        handler->PSendSysMessage("Players with the more reports:");
        do
        {
            Field *fieldsDB = resultDB->Fetch();

            uint32 guid = fieldsDB[0].GetUInt32();
            float average = fieldsDB[1].GetFloat();
            uint32 total_reports = fieldsDB[2].GetUInt32();

            if (Player* player = ObjectAccessor::FindPlayerByLowGUID(guid))
                handler->PSendSysMessage("Player: %s Total Reports: %u Average: %f",player->GetName().c_str(),total_reports,average);

        } while (resultDB->NextRow());
    }
}

void AnticheatMgr::AnticheatDeleteCommand(uint32 guid)
{
    if (!guid)
    {
        for (AnticheatPlayersDataMap::iterator it = m_Players.begin(); it != m_Players.end(); ++it)
        {
            (*it).second.SetTotalReports(0);
            (*it).second.SetAverage(0);
            (*it).second.SetCreationTime(0);
            for (uint8 i = 0; i < MAX_REPORT_TYPES; i++)
            {
                (*it).second.SetTempReports(0,i);
                (*it).second.SetTempReportsTimer(0,i);
                (*it).second.SetTypeReports(i,0);
            }
        }
        LogsDatabase.PExecute("DELETE FROM players_reports_status;");
    }
    else
    {
        m_Players[guid].SetTotalReports(0);
        m_Players[guid].SetAverage(0);
        m_Players[guid].SetCreationTime(0);
        for (uint8 i = 0; i < MAX_REPORT_TYPES; i++)
        {
            m_Players[guid].SetTempReports(0,i);
            m_Players[guid].SetTempReportsTimer(0,i);
            m_Players[guid].SetTypeReports(i,0);
        }
        LogsDatabase.PExecute("DELETE FROM players_reports_status WHERE guid=%u;",guid);
    }
}

void AnticheatMgr::ResetDailyReportStates()
{
     for (AnticheatPlayersDataMap::iterator it = m_Players.begin(); it != m_Players.end(); ++it)
         m_Players[(*it).first].SetDailyReportState(false);
}

void AnticheatMgr::SavePQRData(Player* player)
{
    SQLTransaction trans = LogsDatabase.BeginTransaction();
    SaveInterruptHackLogs(player, trans);
    SaveDispellHackLogs(player, trans);
    LogsDatabase.CommitTransaction(trans);
}

void AnticheatMgr::SaveInterruptHackLogs(Player* caster, SQLTransaction& trans)
{
    std::list<AnticheatData::PQR::Spell*>& interrupts = m_Players[caster->GetGUID().GetCounter()].GetPQRData()->interrupts;
    if (interrupts.empty())
        return;

    for (std::list<AnticheatData::PQR::Spell*>::iterator itr = interrupts.begin(); itr != interrupts.end(); ++itr)
    {
        AnticheatData::PQR::Spell* spell = *itr;
        PreparedStatement* stmt = LogsDatabase.GetPreparedStatement(LOGS_INS_INTERRUPT);
        stmt->setUInt32(0, caster->GetGUID().GetCounter());
        stmt->setString(1, caster->GetName().c_str());
        stmt->setUInt32(2, caster->GetSession()->GetAccountId());
        stmt->setString(3, caster->GetSession()->GetRemoteAddress().c_str());
        stmt->setUInt32(4, int32(spell->time));
        stmt->setUInt16(5, int32(spell->start));
        stmt->setUInt16(6, int32(spell->end));
        stmt->setUInt16(7, int32(spell->cast));
        stmt->setUInt32(8, uint32(spell->spellId));
        trans->Append(stmt);
    }
    interrupts.clear();
}

void AnticheatMgr::SaveDispellHackLogs(Player* caster, SQLTransaction& trans)
{
    std::list<AnticheatData::PQR::Aura*>& dispells = m_Players[caster->GetGUID().GetCounter()].GetPQRData()->dispells;
    if (dispells.empty())
        return;

    for (std::list<AnticheatData::PQR::Aura*>::iterator itr = dispells.begin(); itr != dispells.end(); ++itr)
    {
        AnticheatData::PQR::Aura* aura = *itr;
        PreparedStatement* stmt = LogsDatabase.GetPreparedStatement(LOGS_INS_DISPELL);
        stmt->setUInt32(0, caster->GetGUID().GetCounter());
        stmt->setString(1, caster->GetName().c_str());
        stmt->setUInt32(2, caster->GetSession()->GetAccountId());
        stmt->setString(3, caster->GetSession()->GetRemoteAddress().c_str());
        stmt->setUInt32(4, int32(aura->time));
        stmt->setUInt16(5, int32(aura->delay));
        stmt->setUInt16(6, int32(aura->duration));
        stmt->setUInt32(7, uint32(aura->spellId));
        trans->Append(stmt);
    }
    dispells.clear();
}
