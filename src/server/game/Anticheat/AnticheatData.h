#ifndef SC_ACDATA_H
#define SC_ACDATA_H

#include "Common.h"
#include "SharedDefines.h"
#include "SpellScript.h"
#include "Chat.h"

enum ReportTypes
{
    SPEED_HACK_REPORT = 0,
    FLY_HACK_REPORT,
    WALK_WATER_HACK_REPORT,
    MULTI_JUMP_HACK_REPORT,
    TELEPORT_PLANE_HACK_REPORT,
    CLIMB_HACK_REPORT,
    INTERRUPT_HACK_REPORT,
    AIR_WALK_HACK_REPORT,
    SUPER_JUMP_HACK_REPORT,
    TELE_HACK_REPORT,
    MAX_REPORT_TYPES,
};

enum DetectionTypes
{
    SPEED_HACK_DETECTION            = 0x001,
    FLY_HACK_DETECTION              = 0x002,
    WALK_WATER_HACK_DETECTION       = 0x004,
    JUMP_HACK_DETECTION             = 0x008,
    TELEPORT_PLANE_HACK_DETECTION   = 0x010,
    CLIMB_HACK_DETECTION            = 0x020,
    INTERRUPT_HACK_DETECTION        = 0x040,
    AIR_WALK_HACK_DETECTION         = 0x080,
    ANTICHEAT_SHOW_DEBUG_INFO       = 0x100,
    ANTICHEAT_AUTO_BAN              = 0x200,
    PQR_HACK_DETECTION              = 0x400,
};

#define MAX_HISTORY_STEPS 4

class TC_GAME_API AnticheatData
{
public:
    struct TC_GAME_API PQR
    {
        ~PQR();

        struct TC_GAME_API Spell
        {
            Spell(int32 start, int32 end, int32 cast, int32 time, uint32 spellId);
            int32 start;
            int32 end;
            int32 cast;
            int32 time;
            uint32 spellId;
        };

        struct TC_GAME_API Aura
        {
            Aura(int32 duration, int32 delay, int32 time, uint32 spellId);
            int32 duration;
            int32 delay;
            int32 time;
            uint32 spellId;
        };
        std::list<PQR::Spell*> interrupts;
        std::list<PQR::Aura*> dispells;
    };

    AnticheatData();
    ~AnticheatData();

    void SetLastOpcode(uint32 opcode);
    uint32 GetLastOpcode() const;

    const MovementInfo& GetLastMovementInfo() const;
    void SetLastMovementInfo(MovementInfo& moveInfo);

    void SetPosition(float x, float y, float z, float o);

    /*
    bool GetDisableACCheck() const;
    void SetDisableACCheck(bool check);

    uint32 GetDisableACTimer() const;
    void SetDisableACTimer(uint32 timer);*/

    void UpdateSpeedHistory(float speed);
    float GetMaxSpeed();

    uint32 GetTotalReports() const;
    void SetTotalReports(uint32 _totalReports);

    uint32 GetTypeReports(uint32 type) const;
    void SetTypeReports(uint32 type, uint32 amount);

    float GetAverage() const;
    void SetAverage(float _average);

    uint32 GetCreationTime() const;
    void SetCreationTime(uint32 creationTime);

    void SetTempReports(uint32 amount, uint8 type);
    uint32 GetTempReports(uint8 type);

    void SetTempReportsTimer(uint32 time, uint8 type);
    uint32 GetTempReportsTimer(uint8 type);

    void SetDailyReportState(bool b);
    bool GetDailyReportState();

    uint16 UpdateAirWalkCounter();
    void ResetAirWalkCounter();

    void UpdatePQRInterruptLogs(int32 cast, int32 start, int32 end, int32 time, uint32 spellId);
    void UpdatePQRDispellLogs(int32 delay, int32 duration, int32 time, uint32 spellId);
    PQR* GetPQRData();
private:

    PQR* PQRCheck;

    uint16 airWalkCounter;
    uint32 lastOpcode;
    MovementInfo lastMovementInfo;
    float speedHistory[MAX_HISTORY_STEPS];
    //bool disableACCheck;
    //uint32 disableACCheckTimer;
    uint32 totalReports;
    uint32 typeReports[MAX_REPORT_TYPES];
    float average;
    uint32 creationTime;
    uint32 tempReports[MAX_REPORT_TYPES];
    uint32 tempReportsTimer[MAX_REPORT_TYPES];
    bool hasDailyReport;
};

#endif
