#include "LogMgr.h"
#include "DatabaseEnv.h"

LogMgr* sLogMgr;

LogMgr::LogMgr()
{

}

void LogMgr::Initialize()
{
    if (!sLogMgr)
        sLogMgr = new LogMgr();
    sLogMgr->_Initialize();
}

void LogMgr::_Initialize()
{
    PreparedStatement* stmt = LogsDatabase.GetPreparedStatement(LOGS_SEL_ARENA_ID);
    PreparedQueryResult result = LogsDatabase.Query(stmt);
    logIds[LOG_TYPE_ARENA] = result ? result->Fetch()[0].GetUInt32() : 0;

    stmt = LogsDatabase.GetPreparedStatement(LOGS_SEL_BOSS_KILL_ID);
    result = LogsDatabase.Query(stmt);
    logIds[LOG_TYPE_BOSS] = result ? result->Fetch()[0].GetUInt32() : 0;
}

uint32 LogMgr::GetNextLogId(LogType type)
{
    return ++logIds[type];
}
