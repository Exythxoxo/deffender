/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "AreaBoundary.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "eye_of_eternity.h"
#include "GameObject.h"
#include "InstanceScript.h"
#include "Map.h"
#include "Player.h"

BossBoundaryData const boundaries =
{
    { DATA_MALYGOS_EVENT, new CircleBoundary(Position(754.362f, 1301.609985f), 280.0) } // sanity check boundary
};

ObjectData const creatureData[] =
{
    { NPC_MALYGOS,            DATA_MALYGOS           },
    { NPC_ALEXSTRASZA_BUNNY,  DATA_ALEXSTRASZA_BUNNY },
    { NPC_ALEXSTRASZAS_GIFT,  DATA_GIFT_BOX_BUNNY    },
    { 0,                      0                      } // END
};

ObjectData const goData[] =
{
    { GO_NEXUS_RAID_PLATFORM, DATA_PLATFORM          },
    { GO_EXIT_PORTAL,         DATA_EXIT_PORTAL       },
    { 0,                      0                      }
};

class instance_eye_of_eternity : public InstanceMapScript
{
public:
    instance_eye_of_eternity() : InstanceMapScript(EoEScriptName, 616) { }

    InstanceScript* GetInstanceScript(InstanceMap* map) const override
    {
        return new instance_eye_of_eternity_InstanceMapScript(map);
    }

    struct instance_eye_of_eternity_InstanceMapScript : public InstanceScript
    {
        instance_eye_of_eternity_InstanceMapScript(Map* map) : InstanceScript(map)
        {
            SetHeaders(DataHeader);
            SetBossNumber(MAX_ENCOUNTER);
            LoadBossBoundaries(boundaries);
            LoadObjectData(creatureData, goData);
        }

        void OnPlayerEnter(Player* player) override
        {
            if (GetBossState(DATA_MALYGOS_EVENT) == DONE)
                player->CastSpell(player, SPELL_SUMMOM_RED_DRAGON_BUDDY, true);
        }

        bool SetBossState(uint32 type, EncounterState state) override
        {
            if (!InstanceScript::SetBossState(type, state))
                return false;

            if (type == DATA_MALYGOS_EVENT)
            {
                if (state == FAIL)
                {
                    for (GuidList::const_iterator itr_trigger = portalTriggers.begin(); itr_trigger != portalTriggers.end(); ++itr_trigger)
                    {
                        if (Creature* trigger = instance->GetCreature(*itr_trigger))
                        {
                            // just in case
                            trigger->RemoveAllAuras();
                            trigger->AI()->Reset();
                        }
                    }

                    if (GameObject* exitPortal = GetGameObject(DATA_EXIT_PORTAL))
                        exitPortal->SetRespawnTime(0);

                    if (GameObject* platform = GetGameObject(DATA_PLATFORM))
                        platform->RemoveFlag(GAMEOBJECT_FLAGS, GO_FLAG_DESTROYED);
                }
                else if (state == NOT_STARTED)
                {
                    // Teleport to spawn position, we can't use normal relocate
                    // float x, y, z, o;
                    // me->GetRespawnPosition(x, y, z, &o);
                    // me->NearTeleportTo(x, y, z, o);
                    // std::cout << "lala...";
                    // Respawn Iris
                    if (GameObject* iris = instance->GetGameObject(GetGuidData(DATA_FOCUSING_IRIS_GUID)))
                        iris->SetRespawnTime(0);
                }
                else if (state == DONE)
                    if (GameObject* exitPortal = GetGameObject(DATA_EXIT_PORTAL))
                        exitPortal->SetRespawnTime(0);
            }
            return true;
        }

        void OnGameObjectCreate(GameObject* go) override
        {
            InstanceScript::OnGameObjectCreate(go);
            switch (go->GetEntry())
            {
                case GO_FOCUSING_IRIS_10:
                case GO_FOCUSING_IRIS_25:
                    irisGUID = go->GetGUID();
                    focusingIrisPosition = go->GetPosition();
                    break;
                case GO_HEART_OF_MAGIC_10:
                case GO_HEART_OF_MAGIC_25:
                    heartOfMagicGUID = go->GetGUID();
                    break;
                default:
                    break;
            }
        }

        void OnCreatureCreate(Creature* creature) override
        {
			InstanceScript::OnCreatureCreate(creature);
            switch (creature->GetEntry())
            {
                case NPC_VORTEX_TRIGGER:
                    vortexTriggers.push_back(creature->GetGUID());
                    break;
                case NPC_PORTAL_TRIGGER:
                    portalTriggers.push_back(creature->GetGUID());
                    break;
            }
        }

        void OnUnitDeath(Unit* unit) override
        {
            if (unit->GetTypeId() != TYPEID_PLAYER)
                return;

            // Player continues to be moving after death no matter if spline will be cleared along with all movements,
            // so on next world tick was all about delay if box will pop or not (when new movement will be registered)
            // since in EoE you never stop falling. However root at this precise* moment works,
            // it will get cleared on release. If by any chance some lag happen "Reload()" and "RepopMe()" works,
            // last test I made now gave me 50/0 of this bug so I can't do more about it.
            unit->SetControlled(true, UNIT_STATE_ROOT);
        }

        void ProcessEvent(WorldObject* /*obj*/, uint32 eventId) override
        {
            if (eventId == EVENT_FOCUSING_IRIS)
            {
                if (Creature* alexstraszaBunny = GetCreature(DATA_GIFT_BOX_BUNNY))
                    alexstraszaBunny->CastSpell(alexstraszaBunny, SPELL_IRIS_OPENED);

                if (GameObject* iris = instance->GetGameObject(irisGUID))
                    iris->SetFlag(GAMEOBJECT_FLAGS, GO_FLAG_IN_USE);

                if (Creature* malygos = GetCreature(DATA_MALYGOS))
                    malygos->AI()->DoAction(0); // ACTION_LAND_ENCOUNTER_START

                if (GameObject* exitPortal = GetGameObject(DATA_EXIT_PORTAL))
                    exitPortal->SetRespawnTime(15 * MINUTE);
            }
        }

        void VortexHandling()
        {
            if (Creature* malygos = GetCreature(DATA_MALYGOS))
            {
                for (auto itr_vortex : vortexTriggers)
                {
                    uint8 counter = 0;
                    if (Creature* trigger = instance->GetCreature(itr_vortex))
                    {
                        // each trigger have to cast the spell to 5 players.
                        for (auto* ref : malygos->GetThreatManager().GetUnsortedThreatList())
                        {
                            if (counter >= 5)
                                break;

                            if (Player* player = ref->GetVictim()->ToPlayer())
                            {
                                if (player->IsGameMaster() || player->HasAura(SPELL_VORTEX_4))
                                    continue;

                                player->CastSpell(trigger, SPELL_VORTEX_4, true);
                                counter++;
                            }
                        }
                    }
                }
            }
        }

        void PowerSparksHandling()
        {
            bool next = (!lastPortalGUID || lastPortalGUID == portalTriggers.back());

            for (auto itr_trigger : portalTriggers)
            {
                if (next)
                {
                    if (Creature* trigger = instance->GetCreature(itr_trigger))
                    {
                        lastPortalGUID = trigger->GetGUID();
                        trigger->CastSpell(trigger, SPELL_PORTAL_OPENED, true);
                        return;
                    }
                }

                if (itr_trigger == lastPortalGUID)
                    next = true;
            }
        }

        void SetData(uint32 data, uint32 /*value*/) override
        {
            switch (data)
            {
                case DATA_VORTEX_HANDLING:
                    VortexHandling();
                    break;
                case DATA_POWER_SPARKS_HANDLING:
                    PowerSparksHandling();
                    break;
            }
        }

        ObjectGuid GetGuidData(uint32 data) const override
        {
            switch (data)
            {
                case DATA_TRIGGER:
                    return vortexTriggers.front();
                case DATA_HEART_OF_MAGIC_GUID:
                    return heartOfMagicGUID;
                case DATA_FOCUSING_IRIS_GUID:
                    return irisGUID;
            }

            return ObjectGuid::Empty;
        }

    private:
        GuidList vortexTriggers;
        GuidList portalTriggers;
        ObjectGuid irisGUID;
        ObjectGuid lastPortalGUID;
        ObjectGuid heartOfMagicGUID;
        Position focusingIrisPosition;
    };
};

void AddSC_instance_eye_of_eternity()
{
    new instance_eye_of_eternity();
}
