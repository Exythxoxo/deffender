/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "AreaBoundary.h"
#include "Creature.h"
#include "InstanceScript.h"
#include "Map.h"
#include "obsidian_sanctum.h"
#include "Pet.h"

/* Obsidian Sanctum encounters:
0 - Sartharion
*/

BossBoundaryData const boundaries =
{
    { BOSS_SARTHARION, new RectangleBoundary(3218.86f, 3275.69f, 484.68f, 572.4f) }
};

class instance_obsidian_sanctum : public InstanceMapScript
{
public:
    instance_obsidian_sanctum() : InstanceMapScript(OSScriptName, 615) { }

    struct instance_obsidian_sanctum_InstanceMapScript : public InstanceScript
    {
        instance_obsidian_sanctum_InstanceMapScript(Map* map) : InstanceScript(map)
        {
            SetHeaders(DataHeader);
            SetBossNumber(EncounterCount);
            LoadBossBoundaries(boundaries);
            shadAcolyte = false;
            vespAcolyte = false;
            tenebEgg = false;
            portalOpen = false;
            instanceSummons = GuidList();
            shadLoot = true;
            vespLoot = true;
            teneLoot = true;
            achievEligible = false;
        }

        void TwilightShiftOutPlayers()
        {
            Map::PlayerList const& PlayerList = instance->GetPlayers();
            if (!PlayerList.isEmpty())
            {
                for (Map::PlayerList::const_iterator itr = PlayerList.begin(); itr != PlayerList.end(); ++itr)
                {
                    if (Player* player = itr->GetSource())
                    {
                        if (player->HasAura(SPELL_TWILIGHT_SHIFT_AURA))
                        {
                            player->RemoveAura(SPELL_TWILIGHT_SHIFT_AURA);
                            player->RemoveAura(SPELL_TWILIGHT_SHIFT_DMG);
                            player->AddAura(SPELL_TWILIGHT_RESIDUE,player);
                            if (Pet* pet = player->GetPet())
                            {
                                pet->RemoveAura(SPELL_TWILIGHT_SHIFT_AURA);
                                pet->RemoveAura(SPELL_TWILIGHT_SHIFT_DMG);
                                pet->AddAura(SPELL_TWILIGHT_RESIDUE,pet);
                            }
                        }
                    }
                }
            }
        }

        void ClearSummons()
        {
            for (auto summ : instanceSummons)
            {
                if (Creature *cre = instance->GetCreature(summ))
                    cre->KillSelf();
            }
            instanceSummons.clear();
        }

        void OnCreatureCreate(Creature* creature) override
        {
            switch (creature->GetEntry())
            {
                case NPC_SARTHARION:
                    sartharionGUID = creature->GetGUID();
                    break;
                case NPC_TENEBRON:
                    if (Creature *tene = instance->GetCreature(tenebronGUID))
                        if (tenebronGUID != creature->GetGUID())
                            tene->RemoveFromGrid();
                    if (!teneLoot)
                        creature->SetLootDisabled(true);

                    tenebronGUID = creature->GetGUID();
                    creature->setActive(true);
                    break;
                case NPC_SHADRON:
                    if (Creature *shad = instance->GetCreature(shadronGUID))
                        if (shadronGUID != creature->GetGUID())
                            shad->RemoveFromGrid();
                    if (!shadLoot)
                        creature->SetLootDisabled(true);

                    shadronGUID = creature->GetGUID();
                    creature->setActive(true);
                    break;
                case NPC_VESPERON:
                    if (Creature *vesp = instance->GetCreature(vesperonGUID))
                        if (vesperonGUID != creature->GetGUID())
                            vesp->RemoveFromGrid();
                    if (!vespLoot)
                        creature->SetLootDisabled(true);

                    vesperonGUID = creature->GetGUID();
                    creature->setActive(true);
                    break;
                case NPC_LAVA_BLAZE:
                case NPC_TWILIGHT_WHELP:
                case NPC_SARTHARION_TWILIGHT_WHELP:
                case NPC_ACOLYTE_OF_VESPERON:
                case NPC_ACOLYTE_OF_SHADRON:
                    instanceSummons.push_back(creature->GetGUID());
                    break;
            }
        }

        bool SetBossState(uint32 type, EncounterState state) override
        {
            if (!InstanceScript::SetBossState(type, state))
                 return false;

            switch (type)
            {
                case BOSS_SARTHARION:
                case BOSS_TENEBRON:
                case BOSS_SHADRON:
                case BOSS_VESPERON:
                    break;
                default:
                    break;
            }
            return true;
        }

        ObjectGuid GetGuidData(uint32 Data) const override
        {
            switch (Data)
            {
                case BOSS_SARTHARION:
                    return sartharionGUID;
                case BOSS_TENEBRON:
                    return tenebronGUID;
                case BOSS_SHADRON:
                    return shadronGUID;
                case BOSS_VESPERON:
                    return vesperonGUID;
            }
            return ObjectGuid::Empty;
        }

        uint32 GetData(uint32 id) const override
        {
            switch (id)
            {
            case DATA_VESP_LOOT:
                if (vespLoot)
                    return HAS_LOOT;
                return NO_LOOT;
            case DATA_SHAD_LOOT:
                if (shadLoot)
                    return HAS_LOOT;
                return NO_LOOT;
            case DATA_TENE_LOOT:
                if (teneLoot)
                    return HAS_LOOT;
                return NO_LOOT;
            case DATA_VESP_ACO:
                if (vespAcolyte)
                    return SPAWNED;
                return NOT_SPAWNED;
            case DATA_SHAD_ACO:
                if (shadAcolyte)
                    return SPAWNED;
                return NOT_SPAWNED;
            case DATA_TENE_EGG:
                if (tenebEgg)
                    return SPAWNED;
                return NOT_SPAWNED;
            case DATA_PORTAL_OPEN:
                if (portalOpen)
                    return SPAWNED;
                return NOT_SPAWNED;
            default:
                break;
            }
            return 0xDEADBEEF;
        }

        void SetData(uint32 id, uint32 data) override
        {
            bool portalWasOpen = portalOpen;
            switch (id)
            {
            case DATA_VESP_LOOT:
                vespLoot = data ? true : false;
                break;
            case DATA_SHAD_LOOT:
                shadLoot = data ? true : false;
                break;
            case DATA_TENE_LOOT:
                teneLoot = data ? true : false;
                break;
            case DATA_VESP_ACO:
                vespAcolyte = data ? true : false;
                break;
            case DATA_SHAD_ACO:
                shadAcolyte = data ? true : false;
                break;
            case DATA_TENE_EGG:
                tenebEgg = data ? true : false;
                break;
            case DATA_PORTAL_OPEN:
                portalOpen = data ? true : false;
                break;
            case DATA_ALL:
                vespAcolyte = shadAcolyte = tenebEgg = portalOpen = data ? true : false;
                break;
            case DATA_ACHIEV_ELIGIBLE:
                achievEligible = data ? true : false;
                break;
            case FNC_DEL_SUMMONS:
                ClearSummons();
                break;
            default:
                break;
            }

            // Check if we should close portal
            if (!shadAcolyte && !vespAcolyte && !tenebEgg)
                portalOpen = NOT_SPAWNED;

            if (portalWasOpen && !portalOpen)
                TwilightShiftOutPlayers();
        }

        bool CheckAchievementCriteriaMeet(uint32 criteriaId, Player const* /*player*/, Unit const* /* = nullptr */, uint32 /* = 0 */) override
        {
            if (criteriaId == CRITERIA_OBSIDIAN_SLAYER)
                return instance->Is25ManRaid() && achievEligible;

            return false;
        }

    protected:
        bool shadLoot;
        bool vespLoot;
        bool teneLoot;
        bool shadAcolyte;
        bool vespAcolyte;
        bool tenebEgg;
        bool portalOpen;
        bool achievEligible;
        GuidList instanceSummons;
        ObjectGuid sartharionGUID;
        ObjectGuid tenebronGUID;
        ObjectGuid shadronGUID;
        ObjectGuid vesperonGUID;
    };

    InstanceScript* GetInstanceScript(InstanceMap* map) const override
    {
        return new instance_obsidian_sanctum_InstanceMapScript(map);
    }
};

void AddSC_instance_obsidian_sanctum()
{
    new instance_obsidian_sanctum();
}
