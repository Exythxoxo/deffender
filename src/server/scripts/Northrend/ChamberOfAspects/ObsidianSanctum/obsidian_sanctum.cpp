/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "CellImpl.h"
#include "GridNotifiersImpl.h"
#include "InstanceScript.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "obsidian_sanctum.h"
#include "ScriptedCreature.h"
#include "TemporarySummon.h"
#include "GameObject.h"
#include "GameObjectAI.h"

enum Misc
{
    DATA_CAN_LOOT               = 0,
};

#define MAX_WAYPOINT 6
//points around raid "isle", counter clockwise. should probably be adjusted to be more alike
Position const dragonCommon[MAX_WAYPOINT]=
{
    { 3214.012f, 468.932f, 98.652f, 0.0f },
    { 3244.950f, 468.427f, 98.652f, 0.0f },
    { 3283.520f, 496.869f, 98.652f, 0.0f },
    { 3287.316f, 555.875f, 98.652f, 0.0f },
    { 3250.479f, 585.827f, 98.652f, 0.0f },
    { 3209.969f, 566.523f, 98.652f, 0.0f }
};

Position const AcolyteofShadron   = { 3363.92f, 534.703f, 97.2683f, 0.0f };
Position const AcolyteofShadron2  = { 3246.57f, 551.263f, 58.6164f, 0.0f };
Position const AcolyteofVesperon  = { 3145.68f, 520.71f,  89.7f,    0.0f };
Position const AcolyteofVesperon2 = { 3246.57f, 551.263f, 58.6164f, 0.0f };

Position const TwilightEggs[] =
{
    { 3219.28f, 669.121f, 88.5549f, 0.0f },
    { 3221.55f, 682.852f, 90.5361f, 0.0f },
    { 3239.77f, 685.94f,  90.3168f, 0.0f },
    { 3250.33f, 669.749f, 88.7637f, 0.0f },
    { 3246.6f,  642.365f, 84.8752f, 0.0f },
    { 3233.68f, 653.117f, 85.7051f, 0.0f }
};

Position const TwilightEggsSarth[] =
{
    { 3252.73f, 515.762f, 58.5501f, 0.0f },
    { 3256.56f, 521.119f, 58.6061f, 0.0f },
    { 3255.63f, 527.513f, 58.7568f, 0.0f },
    { 3264.90f, 525.865f, 58.6436f, 0.0f },
    { 3264.26f, 516.364f, 58.8011f, 0.0f },
    { 3257.54f, 502.285f, 58.2077f, 0.0f }
};

enum SharedTextIds
{
    SAY_AGGRO                      = 0,
    SAY_SLAY                       = 1,
    SAY_DEATH                      = 2,
    SAY_BREATH                     = 3,
    SAY_RESPOND                    = 4,
    SAY_SPECIAL                    = 5,
    WHISPER_OPEN_PORTAL            = 6,
    WHISPER_OPENED_PORTAL          = 7
};

enum DragonEvents
{
    // Shared Events
    EVENT_FREE_MOVEMENT            = 1,
    EVENT_SHADOW_FISSURE           = 2,
    EVENT_SHADOW_BREATH            = 3,

    // Tenebron
    EVENT_HATCH_EGGS               = 4,

    // Shadron
    EVENT_ACOLYTE_SHADRON          = 5,

    // Vesperon
    EVENT_ACOLYTE_VESPERON         = 6
};

// to control each dragons common abilities
struct dummy_dragonAI : public ScriptedAI
{
    dummy_dragonAI(Creature* creature) : ScriptedAI(creature)
    {
        Initialize();
        instance = creature->GetInstanceScript();
    }

    void Initialize()
    {
        me->RemoveAllAuras();
        waypointId = 0;
        _canMoveFree = false;
    }

    void Reset() override
    {
        if (me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE))
            me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE);

        instance->SetData(FNC_DEL_SUMMONS, PROCEED);

        switch (me->GetEntry())
        {
            case NPC_TENEBRON:
                if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                {
                    instance->SetData(DATA_TENE_EGG, NOT_SPAWNED);
                }
                break;
            case NPC_SHADRON:
                if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                {
                    instance->SetData(DATA_SHAD_ACO, NOT_SPAWNED);
                    if (Creature* acolyte = me->FindNearestCreature(NPC_ACOLYTE_OF_SHADRON, 100.0f))
                        acolyte->KillSelf();
                }

                break;
            case NPC_VESPERON:
                if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                {
                    instance->SetData(DATA_VESP_ACO, NOT_SPAWNED);
                    if (Creature* acolyte = me->FindNearestCreature(NPC_ACOLYTE_OF_VESPERON, 100.0f))
                        acolyte->KillSelf();
                }
                break;
        }

        events.Reset();
        Initialize();
    }

    void EnterCombat(Unit* /*who*/) override
    {
        Talk(SAY_AGGRO);
        DoZoneInCombat();
        if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
            me->RemoveAllAuras();

        events.ScheduleEvent(EVENT_SHADOW_FISSURE, 5000);
        events.ScheduleEvent(EVENT_SHADOW_BREATH, 12000);
    }

    void MovementInform(uint32 type, uint32 pointId) override
    {
        if (!instance || type != POINT_MOTION_TYPE)
            return;

        // debug_log("dummy_dragonAI: %s reached point %u", me->GetName(), uiPointId);

        // if healers messed up the raid and we was already initialized
        if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
        {
            EnterEvadeMode();
            return;
        }

        // this is end, if we reach this, don't do much
        if (pointId == POINT_ID_LAND)
        {
            me->GetMotionMaster()->Clear();
            me->SetInCombatWithZone();
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0, true))
            {
                AddThreat(target, 1.0f);
                me->Attack(target, true);
                me->GetMotionMaster()->MoveChase(target);
            }

            _canMoveFree = false;
            return;
        }

        // increase
        waypointId = pointId + 1;

        // if we have reached a point bigger or equal to count, it mean we must reset to point 0
        if (waypointId >= MAX_WAYPOINT)
        {
            if (!_canMoveFree)
                _canMoveFree = true;

            waypointId = 0;
        }

        events.ScheduleEvent(EVENT_FREE_MOVEMENT, 500);
    }

    // "opens" the portal and does the "opening" whisper
    void OpenPortal()
    {
        Talk(WHISPER_OPEN_PORTAL);
        Talk(WHISPER_OPENED_PORTAL);

        instance->SetData(DATA_PORTAL_OPEN, SPAWNED);
    }

    void KilledUnit(Unit* who) override
    {
        if (who->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_SLAY);
    }

    void JustDied(Unit* /*killer*/) override
    {
        uint32 spellId = 0;

        switch (me->GetEntry())
        {
            case NPC_TENEBRON:
                if (instance->GetData(DATA_TENE_LOOT) == NO_LOOT)
                    me->SetLootRecipient(nullptr);

                spellId = SPELL_POWER_OF_TENEBRON;
                if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                {
                    instance->SetBossState(BOSS_TENEBRON, DONE);
                }
                break;
            case NPC_SHADRON:
                if (instance->GetData(DATA_SHAD_LOOT) == NO_LOOT)
                    me->SetLootRecipient(nullptr);

                spellId = SPELL_POWER_OF_SHADRON;
                if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                {
                    instance->SetBossState(BOSS_SHADRON, DONE);
                }
                break;
            case NPC_VESPERON:
                if (instance->GetData(DATA_VESP_LOOT) == NO_LOOT)
                    me->SetLootRecipient(nullptr);

                spellId = SPELL_POWER_OF_VESPERON;
                if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                {
                    instance->SetBossState(BOSS_VESPERON, DONE);
                }
                break;
        }

        Talk(SAY_DEATH);

        // not if solo mini-boss fight
        if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
            return;
        if (Unit* sartharion = ObjectAccessor::GetUnit(*me, instance->GetGuidData(BOSS_SARTHARION)))
        {
            sartharion->RemoveOwnedAura(spellId);
            // Twilight Revenge to main boss
            if (sartharion->IsAlive())
            {
                sartharion->AddAura(SPELL_TWILIGHT_REVENGE, sartharion);
            }
        }
    }

    void UpdateAI(uint32 diff) override
    {
        events.Update(diff);

        if (events.ExecuteEvent() == EVENT_FREE_MOVEMENT)
        {
            if (_canMoveFree && waypointId < MAX_WAYPOINT)
                me->GetMotionMaster()->MovePoint(waypointId, dragonCommon[waypointId]);
        }
    }

    void ExecuteEvent(uint32 eventId)
    {
        switch (eventId)
        {
            case EVENT_SHADOW_FISSURE:
                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0.0f, true))
                    DoCast(target, SPELL_SHADOW_FISSURE);
                events.ScheduleEvent(eventId, 10000);
                break;
            case EVENT_SHADOW_BREATH:
                Talk(SAY_BREATH);
                DoCastVictim(SPELL_SHADOW_BREATH);
                events.ScheduleEvent(eventId, urand(15000, 20000));
                break;
            default:
                break;
        }
    }

    protected:
        InstanceScript* instance;
        EventMap events;
        uint32   waypointId;
        bool     _canMoveFree;
};

/*######
## Tenebron
######*/

class npc_tenebron : public CreatureScript
{
public:
    npc_tenebron() : CreatureScript("npc_tenebron") { }

    struct npc_tenebronAI : public dummy_dragonAI
    {
        npc_tenebronAI(Creature* creature) : dummy_dragonAI(creature) { }

        void Reset() override
        {
            dummy_dragonAI::Reset();
        }

        void EnterCombat(Unit* who) override
        {
            dummy_dragonAI::EnterCombat(who);

            events.ScheduleEvent(EVENT_HATCH_EGGS, 30000);
        }

        void UpdateAI(uint32 diff) override
        {
            // if no target, update dummy and return
            if (!UpdateVictim())
            {
                dummy_dragonAI::UpdateAI(diff);
                return;
            }

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_HATCH_EGGS:
                        instance->SetData(DATA_TENE_EGG, SPAWNED);
                        if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                            for (uint32 i = 0; i < 6; ++i)
                                me->SummonCreature(NPC_TWILIGHT_EGG, TwilightEggs[i], TEMPSUMMON_CORPSE_TIMED_DESPAWN, 20000);
                        else
                            for (uint32 i = 0; i < 6; ++i)
                                me->SummonCreature(NPC_SARTHARION_TWILIGHT_EGG, TwilightEggsSarth[i], TEMPSUMMON_CORPSE_TIMED_DESPAWN, 20000);

                        events.ScheduleEvent(EVENT_HATCH_EGGS, 30000);
                        OpenPortal();
                        break;
                    default:
                        dummy_dragonAI::ExecuteEvent(eventId);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_tenebronAI>(creature);
    }
};

/*######
## Shadron
######*/

class npc_shadron : public CreatureScript
{
public:
    npc_shadron() : CreatureScript("npc_shadron") { }

    struct npc_shadronAI : public dummy_dragonAI
    {
        npc_shadronAI(Creature* creature) : dummy_dragonAI(creature) { }

        void Reset() override
        {
            dummy_dragonAI::Reset();
        }

        void EnterCombat(Unit* who) override
        {
            dummy_dragonAI::EnterCombat(who);
            if (Creature* acolyte = me->FindNearestCreature(NPC_ACOLYTE_OF_SHADRON, 100.0f))
                acolyte->KillSelf();
            events.ScheduleEvent(EVENT_ACOLYTE_SHADRON, 30000);
        }

        void UpdateAI(uint32 diff) override
        {
            // if no target, update dummy and return
            if (!UpdateVictim())
            {
                dummy_dragonAI::UpdateAI(diff);
                return;
            }

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ACOLYTE_SHADRON:
                        if (instance->GetData(DATA_SHAD_ACO))
                            events.ScheduleEvent(EVENT_ACOLYTE_SHADRON, 10000);
                        else
                        {
                            instance->SetData(DATA_SHAD_ACO, SPAWNED);
                            if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                                me->SummonCreature(NPC_ACOLYTE_OF_SHADRON, AcolyteofShadron, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 600000);
                            else
                                me->SummonCreature(NPC_ACOLYTE_OF_SHADRON, AcolyteofShadron2, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 600000);

                            events.ScheduleEvent(EVENT_ACOLYTE_SHADRON, 60000);
                            OpenPortal();
                        }
                        break;
                    default:
                        dummy_dragonAI::ExecuteEvent(eventId);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_shadronAI>(creature);
    }
};

/*######
## Vesperon
######*/

class npc_vesperon : public CreatureScript
{
public:
    npc_vesperon() : CreatureScript("npc_vesperon") { }

    struct npc_vesperonAI : public dummy_dragonAI
    {
        npc_vesperonAI(Creature* creature) : dummy_dragonAI(creature) { }

        void Reset() override
        {
            dummy_dragonAI::Reset();
        }

        void EnterCombat(Unit* who) override
        {
            dummy_dragonAI::EnterCombat(who);
            if (Creature* acolyte = me->FindNearestCreature(NPC_ACOLYTE_OF_VESPERON, 100.0f))
                acolyte->KillSelf();
            events.ScheduleEvent(EVENT_ACOLYTE_VESPERON, 30000);
        }

        void UpdateAI(uint32 diff) override
        {
            // if no target, update dummy and return
            if (!UpdateVictim())
            {
                dummy_dragonAI::UpdateAI(diff);
                return;
            }

            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_ACOLYTE_VESPERON:
                        if (instance->GetData(DATA_VESP_ACO))
                            events.ScheduleEvent(EVENT_ACOLYTE_VESPERON, 10000);
                        else
                        {
                            instance->SetData(DATA_VESP_ACO, SPAWNED);
                            if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                                me->SummonCreature(NPC_ACOLYTE_OF_VESPERON, AcolyteofVesperon, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 600000);
                            else
                                me->SummonCreature(NPC_ACOLYTE_OF_VESPERON, AcolyteofVesperon2, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 600000);

                            instance->DoApplyAuraOnPlayers(SPELL_TWILIGHT_TORMENT);
                            events.ScheduleEvent(EVENT_ACOLYTE_VESPERON, 60000);
                            OpenPortal();
                        }
                        break;
                    default:
                        dummy_dragonAI::ExecuteEvent(eventId);
                        break;
                }
            }

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_vesperonAI>(creature);
    }
};

/*######
## Acolyte of Shadron
######*/

class npc_acolyte_of_shadron : public CreatureScript
{
    public:
        npc_acolyte_of_shadron() : CreatureScript("npc_acolyte_of_shadron") { }

        struct npc_acolyte_of_shadronAI : public ScriptedAI
        {
            npc_acolyte_of_shadronAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = creature->GetInstanceScript();
            }

            void Reset() override
            {
                //if not solo fight, buff main boss, else place debuff on mini-boss
                if (instance->GetBossState(BOSS_SARTHARION) == IN_PROGRESS)
                {
                    if (Creature* sartharion = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_SARTHARION)))
                        sartharion->AddAura(SPELL_GIFT_OF_TWILIGTH_SAR, sartharion);
                }
                else
                {
                    if (Creature* shadron = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_SHADRON)))
                        shadron->AddAura(SPELL_GIFT_OF_TWILIGTH_SHA, shadron);
                }

                me->AddAura(SPELL_TWILIGHT_SHIFT_AURA, me);
            }

            void JustDied(Unit* /*killer*/) override
            {
                instance->SetData(DATA_SHAD_ACO, NOT_SPAWNED);

                // debuff Satharion
                if (Creature* debuffTarget = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_SARTHARION)))
                    if (debuffTarget->IsAlive() && debuffTarget->HasAura(SPELL_GIFT_OF_TWILIGTH_SAR))
                        debuffTarget->RemoveAurasDueToSpell(SPELL_GIFT_OF_TWILIGTH_SAR);

                // debuff Shadron
                if (Creature* debuffTarget = ObjectAccessor::GetCreature(*me, instance->GetGuidData(BOSS_SHADRON)))
                    if (debuffTarget->IsAlive() && debuffTarget->HasAura(SPELL_GIFT_OF_TWILIGTH_SHA))
                        debuffTarget->RemoveAurasDueToSpell(SPELL_GIFT_OF_TWILIGTH_SHA);
            }

            void UpdateAI(uint32 /*diff*/) override
            {
                if (!UpdateVictim())
                    return;

                DoMeleeAttackIfReady();
            }

        private:
            InstanceScript* instance;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetObsidianSanctumAI<npc_acolyte_of_shadronAI>(creature);
        }
};

/*######
## Acolyte of Vesperon
######*/

class npc_acolyte_of_vesperon : public CreatureScript
{
    public:
        npc_acolyte_of_vesperon() : CreatureScript("npc_acolyte_of_vesperon") { }

        struct npc_acolyte_of_vesperonAI : public ScriptedAI
        {
            npc_acolyte_of_vesperonAI(Creature* creature) : ScriptedAI(creature)
            {
                instance = creature->GetInstanceScript();
            }

            void Reset() override
            {
                me->AddAura(SPELL_TWILIGHT_SHIFT_AURA, me);
            }

            void JustDied(Unit* /*killer*/) override
            {
                instance->SetData(DATA_VESP_ACO, NOT_SPAWNED);
                instance->DoRemoveAurasDueToSpellOnPlayers(SPELL_TWILIGHT_TORMENT);
            }

            void UpdateAI(uint32 /*diff*/) override
            {
                if (!UpdateVictim())
                    return;

                DoMeleeAttackIfReady();
            }

        private:
            InstanceScript* instance;
        };

        CreatureAI* GetAI(Creature* creature) const override
        {
            return GetObsidianSanctumAI<npc_acolyte_of_vesperonAI>(creature);
        }
};

/*######
## Twilight Eggs
######*/

enum TwilightEggs
{
    EVENT_TWILIGHT_EGGS           = 11
};

class npc_twilight_eggs : public CreatureScript
{
public:
    npc_twilight_eggs() : CreatureScript("npc_twilight_eggs") { }

    struct npc_twilight_eggsAI : public ScriptedAI
    {
        npc_twilight_eggsAI(Creature* creature) : ScriptedAI(creature)
        {
            SetCombatMovement(false);
            instance = creature->GetInstanceScript();
        }

        void Reset() override
        {
            me->AddAura(SPELL_TWILIGHT_SHIFT_AURA, me);

            events.ScheduleEvent(EVENT_TWILIGHT_EGGS, 20000);
        }

        void SpawnWhelps()
        {
            me->RemoveAllAuras();

            if (instance->GetBossState(BOSS_SARTHARION) != IN_PROGRESS)
                me->SummonCreature(NPC_TWILIGHT_WHELP, 0.0f, 0.0f, 0.0f, 0.0f, TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 60000);
            else
                me->SummonCreature(NPC_SARTHARION_TWILIGHT_WHELP, 0.0f, 0.0f, 0.0f, 0.0f, TEMPSUMMON_TIMED_OR_CORPSE_DESPAWN, 60000);
            me->DealDamage(me, me->GetHealth());
        }

        void JustSummoned(Creature* who) override
        {
            who->SetInCombatWithZone();
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            if (events.ExecuteEvent() == EVENT_TWILIGHT_EGGS)
            {
                instance->SetData(DATA_TENE_EGG, NOT_SPAWNED);

                SpawnWhelps();
            }
        }

    private:
        InstanceScript* instance;
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_twilight_eggsAI>(creature);
    }
};

/*######
## Flame Tsunami
######*/

enum FlameTsunami
{
    EVENT_TSUNAMI_TIMER           = 12,
    EVENT_TSUNAMI_BUFF            = 13
};

class npc_flame_tsunami : public CreatureScript
{
public:
    npc_flame_tsunami() : CreatureScript("npc_flame_tsunami") { }

    struct npc_flame_tsunamiAI : public ScriptedAI
    {
        npc_flame_tsunamiAI(Creature* creature) : ScriptedAI(creature)
        {
            me->SetDisplayId(11686);
            me->AddAura(SPELL_FLAME_TSUNAMI, me);
        }

        void SpellHitTarget(Unit* target, SpellInfo const* spell)
        {
            if (spell->Id == SPELL_FLAME_TSUNAMI_DMG_AURA && target)
                DoCast(target, SPELL_FLAME_TSUNAMI_LEAP, true);
        }

        void Reset() override
        {
            me->SetReactState(REACT_PASSIVE);
            events.ScheduleEvent(EVENT_TSUNAMI_TIMER, 100);
            events.ScheduleEvent(EVENT_TSUNAMI_BUFF, 1000);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            while (uint32 eventId = events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_TSUNAMI_TIMER:
                        DoCast(me, SPELL_FLAME_TSUNAMI_DMG_AURA);
                        events.ScheduleEvent(EVENT_TSUNAMI_TIMER, 500);
                        break;
                    case EVENT_TSUNAMI_BUFF:
                        if (Unit* lavaBlaze = GetClosestCreatureWithEntry(me, NPC_LAVA_BLAZE, 10.0f, true))
                            lavaBlaze->CastSpell(lavaBlaze, SPELL_FLAME_TSUNAMI_BUFF, true);
                        events.ScheduleEvent(EVENT_TSUNAMI_BUFF, 1000);
                        break;
                }
            }
        }

    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_flame_tsunamiAI>(creature);
    }
};

/*######
## Twilight Fissure
######*/

enum TwilightFissure
{
    EVENT_VOID_BLAST              = 14
};

class npc_twilight_fissure : public CreatureScript
{
public:
    npc_twilight_fissure() : CreatureScript("npc_twilight_fissure") { }

    struct npc_twilight_fissureAI : public ScriptedAI
    {
        npc_twilight_fissureAI(Creature* creature) : ScriptedAI(creature)
        {
            SetCombatMovement(false);
        }

        void Reset() override
        {
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
            me->AddAura(VOID_ZONE_PREVISUAL, me);
            me->AddAura(VOID_ZONE_VISUAL, me);
            events.ScheduleEvent(EVENT_VOID_BLAST, 5000);
        }

        void UpdateAI(uint32 diff) override
        {
            events.Update(diff);

            if (events.ExecuteEvent() == EVENT_VOID_BLAST)
            {
                DoCastAOE(SPELL_VOID_BLAST);
                ////twilight realm
                //DoCastVictim(57620, true);
                //DoCastVictim(57874, true);
                me->RemoveAllAuras();
                me->KillSelf();
            }
        }

    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_twilight_fissureAI>(creature);
    }
};

/*######
## Twilight Whelps
######*/

enum TwilightWhelps
{
    EVENT_FADE_ARMOR              = 15
};

class npc_twilight_whelp : public CreatureScript
{
public:
    npc_twilight_whelp() : CreatureScript("npc_twilight_whelp") { }

    struct npc_twilight_whelpAI : public ScriptedAI
    {
        npc_twilight_whelpAI(Creature* creature) : ScriptedAI(creature)
        {
            Reset();
        }

        void Reset() override
        {
            me->RemoveAllAuras();
            me->SetInCombatWithZone();
            events.ScheduleEvent(EVENT_FADE_ARMOR, 1000);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            // twilight torment
            events.Update(diff);

            if (events.ExecuteEvent() == EVENT_FADE_ARMOR)
            {
                DoCastVictim(SPELL_FADE_ARMOR);
                events.ScheduleEvent(EVENT_FADE_ARMOR, urand(5000, 10000));
            }

            DoMeleeAttackIfReady();
        }

    private:
        EventMap events;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetObsidianSanctumAI<npc_twilight_whelpAI>(creature);
    }
};

struct npc_os_flame_orb : public ScriptedAI
{
    npc_os_flame_orb(Creature* creature) : ScriptedAI(creature), _owner(nullptr) { }

    void IsSummonedBy(Unit* owner) override
    {
        _owner = owner;
    }

    void Reset() override
    {
        me->SetCanFly(true);
        me->SetDisableGravity(true);
        me->SetReactState(REACT_PASSIVE);
        me->SetInCombatWithZone();
        DoCastSelf(SPELL_FLAME_ORB_PERIODIC);
        DoCastSelf(SPELL_BALL_OF_FLAMES_VISUAL);
        me->GetMotionMaster()->MovePoint(0, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ() + 5.0f);
    }

    void EnterEvadeMode(EvadeReason why) override
    {
        ScriptedAI::EnterEvadeMode(why);
        me->DespawnOrUnsummon();
    }

    void UpdateAI(uint32 diff) override
    {
        if (!_owner)
            return;

        if (!_owner->IsEngaged() || !UpdateVictim())
            me->DespawnOrUnsummon();
    }

private:
    Unit* _owner;
};

class spell_os_conjure_flame_orb : public SpellScript
{
    PrepareSpellScript(spell_os_conjure_flame_orb);

    void HandleScript(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);
        GetCaster()->CastSpell(GetCaster(), SPELL_SUMMON_FLAME_ORB, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_os_conjure_flame_orb::HandleScript, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

// 60241 - Flame Tsunami
class spell_flame_tsunami_jump : public SpellScript
{
    PrepareSpellScript(spell_flame_tsunami_jump);

    void DoJump(SpellEffIndex effIndex)
    {
        Unit* target = GetExplTargetUnit();
        if (!target || target->GetTypeId() != TYPEID_PLAYER || !GetCaster())
            return;

        PreventHitDefaultEffect(effIndex);

        // From Unit::JumpTo
        float vcos = std::cos(GetCaster()->GetOrientation());
        float vsin = std::sin(GetCaster()->GetOrientation());

        WorldPacket data(SMSG_MOVE_KNOCK_BACK, (8+4+4+4+4+4));
        data << target->GetPackGUID();
        data << uint32(0);
        data << TaggedPosition<Position::XY>(vcos, vsin);
        data << float(GetSpellInfo()->Effects[effIndex].MiscValue / 10.f);
        data << float(-(GetSpellInfo()->Effects[effIndex].BasePoints / 10.f));

        target->ToPlayer()->SendDirectMessage(&data);

        // changes fall time
        if (target->GetTypeId() == TYPEID_PLAYER)
            target->ToPlayer()->SetFallInformation(0, GetCaster()->GetPositionZ());
    }

    void Register() override
    {
        OnEffectLaunch += SpellEffectFn(spell_flame_tsunami_jump::DoJump, EFFECT_0, SPELL_EFFECT_LEAP_BACK);
    }
};

class achievement_twilight_assist : public AchievementCriteriaScript
{
    public:
        achievement_twilight_assist() : AchievementCriteriaScript("achievement_twilight_assist") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(TWILIGHT_ACHIEVEMENTS) >= 1;
        }
};

class achievement_twilight_duo : public AchievementCriteriaScript
{
    public:
        achievement_twilight_duo() : AchievementCriteriaScript("achievement_twilight_duo") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(TWILIGHT_ACHIEVEMENTS) >= 2;
        }
};

class achievement_twilight_zone : public AchievementCriteriaScript
{
    public:
        achievement_twilight_zone() : AchievementCriteriaScript("achievement_twilight_zone") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(TWILIGHT_ACHIEVEMENTS) == 3;
        }
};

enum PortalPhaseMasks
{
    NORMAL_PHASE        = 1,
    HIDDEN_PHASE        = 8,
    TWILIGHT_PHASE      = 16
};

class go_twilight_OBS_portal : public GameObjectScript
{
public:
    go_twilight_OBS_portal() : GameObjectScript("go_twilight_OBS_portal") { }

    struct go_twilight_OBS_portalAI : public GameObjectAI
    {
        go_twilight_OBS_portalAI(GameObject* gameobject) : GameObjectAI(gameobject) {}

        void InitializeAI() override
        {
            me->SetPhaseMask(HIDDEN_PHASE, true);
            opened = NOT_SPAWNED;
        }

        bool GossipHello(Player* player) override
        {
            switch (me->GetEntry())
            {
            case PORTAL_IN:
                if (me->GetInstanceScript()->GetData(DATA_PORTAL_OPEN))
                    player->CastSpell(player, SPELL_TWILIGHT_SHIFT_AURA, true);
                player->RemoveAura(SPELL_TWILIGHT_TORMENT);
                break;
            case PORTAL_OUT:
                player->RemoveAura(SPELL_TWILIGHT_SHIFT_AURA);
                player->RemoveAura(SPELL_TWILIGHT_SHIFT_DMG);
                player->AddAura(SPELL_TWILIGHT_RESIDUE, player);
                if (me->GetInstanceScript()->GetData(DATA_VESP_ACO))
                    player->AddAura(SPELL_TWILIGHT_TORMENT, player);
                break;
            default:
                break;
            }
            return true;
        }

        void UpdateAI(uint32 diff) override
        {
            if (me->GetInstanceScript()->GetData(DATA_PORTAL_OPEN) != opened)
            {
                opened = (OSDataTypes) me->GetInstanceScript()->GetData(DATA_PORTAL_OPEN);
                if (opened)
                {
                    if (me->GetEntry() == PORTAL_IN)
                    {
                        me->SetPhaseMask(NORMAL_PHASE, true);
                        me->SetRespawnTime(900 * IN_MILLISECONDS);
                    }
                    else
                    {
                        me->SetPhaseMask(TWILIGHT_PHASE, true);
                        me->SetRespawnTime(900 * IN_MILLISECONDS);
                    }
                }
                else
                    me->SetPhaseMask(HIDDEN_PHASE, true);
            }
        }

    protected:
        OSDataTypes opened;
    };

    GameObjectAI* GetAI(GameObject* gameobject) const override
    {
        return GetObsidianSanctumAI<go_twilight_OBS_portalAI>(gameobject);
    }
};

void AddSC_obsidian_sanctum()
{
    new npc_vesperon();
    new npc_shadron();
    new npc_tenebron();
    new npc_acolyte_of_shadron();
    new npc_acolyte_of_vesperon();
    new npc_twilight_eggs();
    new npc_flame_tsunami();
    new npc_twilight_fissure();
    new npc_twilight_whelp();
    RegisterCreatureAI(npc_os_flame_orb);
    RegisterSpellScript(spell_os_conjure_flame_orb);
    RegisterSpellScript(spell_flame_tsunami_jump);
    new achievement_twilight_assist();
    new achievement_twilight_duo();
    new achievement_twilight_zone();
    new go_twilight_OBS_portal();
}

