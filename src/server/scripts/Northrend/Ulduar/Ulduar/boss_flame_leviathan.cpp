/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Comment: there is missing code on triggers,
 *          brann bronzebeard needs correct gossip info.
 *          requires more work involving area triggers.
 *          if reached brann speaks through his radio..
 */

#include "ScriptMgr.h"
#include "CellImpl.h"
#include "CombatAI.h"
#include "GameObjectAI.h"
#include "GridNotifiersImpl.h"
#include "InstanceScript.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "PassiveAI.h"
#include "ScriptedEscortAI.h"
#include "ScriptedGossip.h"
#include "Spell.h"
#include "SpellInfo.h"
#include "SpellScript.h"
#include "ulduar.h"
#include "Vehicle.h"

enum Spells
{
    SPELL_PURSUED                  = 62374,
    SPELL_GATHERING_SPEED          = 62375,
    SPELL_BATTERING_RAM            = 62376,
    SPELL_FLAME_VENTS              = 62396,
    SPELL_MISSILE_BARRAGE          = 62400,
    SPELL_SYSTEMS_SHUTDOWN         = 62475,
    SPELL_OVERLOAD_CIRCUIT         = 62399,
    SPELL_START_THE_ENGINE         = 62472,
    SPELL_SEARING_FLAME            = 62402,
    SPELL_BLAZE                    = 62292,
    SPELL_TAR_PASSIVE              = 62288,
    SPELL_SMOKE_TRAIL              = 63575,
    SPELL_ELECTROSHOCK             = 62522,
    SPELL_NAPALM                   = 63666,
    SPELL_INVIS_AND_STEALTH_DETECT = 18950, // Passive
    //TOWER Additional SPELLS
    SPELL_THORIM_S_HAMMER          = 62911, // Tower of Storms
    SPELL_THORIM_BEACON_VISUAL     = 63773,
    SPELL_MIMIRON_S_INFERNO        = 62909, // Tower of Flames
    SPELL_MIMIRON_BEACON_VISUAL    = 63772,
    SPELL_HODIR_S_FURY             = 62533, // Tower of Frost
    SPELL_HODIR_BEACON_VISUAL      = 63769,
    SPELL_FREYA_S_WARD             = 62906, // Tower of Nature
    SPELL_FREYA_BEACON_VISUAL      = 64117,
    //TOWER ap & health spells
    SPELL_BUFF_TOWER_OF_STORMS     = 65076,
    SPELL_BUFF_TOWER_OF_FLAMES     = 65075,
    SPELL_BUFF_TOWER_OF_FROST      = 65077,
    SPELL_BUFF_TOWER_OF_LIFE       = 64482,
    //Additional Spells
    SPELL_LASH                     = 65062,
    SPELL_AUTO_REPAIR              = 62705,
    SPELL_LIQUID_PYRITE            = 62494,
    SPELL_DUSTY_EXPLOSION          = 63360,
    SPELL_DUST_CLOUD_IMPACT        = 54740,
    AURA_STEALTH_DETECTION         = 18950,
    SPELL_RIDE_VEHICLE             = 46598,
    SPELL_ADD_PYRITE               = 62496,
};

enum Creatures
{
    NPC_SEAT                       = 33114,
    NPC_MECHANOLIFT                = 33214,
    NPC_LIQUID                     = 33189,
    NPC_CONTAINER                  = 33218,
    NPC_THORIM_BEACON              = 33365,
    NPC_MIMIRON_BEACON             = 33370,
    NPC_HODIR_BEACON               = 33212,
    NPC_FREYA_BEACON               = 33367,
    NPC_THORIM_TARGET_BEACON       = 33364,
    NPC_MIMIRON_TARGET_BEACON      = 33369,
    NPC_HODIR_TARGET_BEACON        = 33108,
    NPC_FREYA_TARGET_BEACON        = 33366,
    NPC_ULDUAR_GAUNTLET_GENERATOR  = 33571, // Trigger tied to towers
};

enum Towers
{
    GO_TOWER_OF_STORMS    = 194377,
    GO_TOWER_OF_FLAMES    = 194371,
    GO_TOWER_OF_FROST     = 194370,
    GO_TOWER_OF_LIFE      = 194375,
};

enum Seats
{
    SEAT_PLAYER    = 0,
    SEAT_TURRET    = 1,
    SEAT_DEVICE    = 2,
    SEAT_CANNON    = 7,
};

enum Vehicles
{
    VEHICLE_SIEGE         = 33060,
    VEHICLE_CHOPPER       = 33062,
    VEHICLE_DEMOLISHER    = 33109,
};

enum Misc
{
    DATA_SHUTOUT               = 29112912, // 2911, 2912 are achievement IDs
    DATA_ORBIT_ACHIEVEMENTS    = 1,
    VEHICLE_SPAWNS             = 5,
    FREYA_SPAWNS               = 4,
};

enum Yells
{
    SAY_AGGRO            = 0,
    SAY_SLAY             = 1,
    SAY_DEATH            = 2,
    SAY_TARGET           = 3,
    SAY_HARDMODE         = 4,
    SAY_TOWER_NONE       = 5,
    SAY_TOWER_FROST      = 6,
    SAY_TOWER_FLAME      = 7,
    SAY_TOWER_NATURE     = 8,
    SAY_TOWER_STORM      = 9,
    SAY_PLAYER_RIDING    = 10,
    SAY_OVERLOAD         = 11,
    EMOTE_PURSUE         = 12,
    EMOTE_OVERLOAD       = 13,
    EMOTE_REPAIR         = 14
};

#define TARGET_BEACON_HEIGHT  20.0f

Position const Center = { 354.8771f, -12.90240f, 409.803650f, M_PI };
Position const InfernoStart = { 390.93f, -13.91f, 409.81f, 0.0f };

Position const PosSiege[VEHICLE_SPAWNS] =
{
    {-814.59f, -64.54f, 429.92f, 5.969f},
    {-784.37f, -33.31f, 429.92f, 5.096f},
    {-808.99f, -52.10f, 429.92f, 5.668f},
    {-798.59f, -44.00f, 429.92f, 5.663f},
    {-812.83f, -77.71f, 429.92f, 0.046f},
};

Position const PosChopper[VEHICLE_SPAWNS] =
{
    {-717.83f, -106.56f, 430.02f, 0.122f},
    {-717.83f, -114.23f, 430.44f, 0.122f},
    {-717.83f, -109.70f, 430.22f, 0.122f},
    {-718.45f, -118.24f, 430.26f, 0.052f},
    {-718.45f, -123.58f, 430.41f, 0.085f},
};

Position const PosDemolisher[VEHICLE_SPAWNS] =
{
    {-724.12f, -176.64f, 430.03f, 2.543f},
    {-766.70f, -225.03f, 430.50f, 1.710f},
    {-729.54f, -186.26f, 430.12f, 1.902f},
    {-756.01f, -219.23f, 430.50f, 2.369f},
    {-798.01f, -227.24f, 429.84f, 1.446f},
};

Position const FreyaBeacons[FREYA_SPAWNS] =
{
    {377.02f, -119.10f, 409.81f, 0.0f},
    {185.62f, -119.10f, 409.81f, 0.0f},
    {377.02f, 54.78f, 409.81f, 0.0f},
    {185.62f, 54.78f, 409.81f, 0.0f},
};

struct boss_flame_leviathan : public BossAI
{
    boss_flame_leviathan(Creature* creature) : BossAI(creature, BOSS_LEVIATHAN) { }

    void InitializeAI() override
    {
        if (!me->isDead())
            Reset();

        scheduler.SetValidator([this]
        {
            return !me->HasUnitState(UNIT_STATE_CASTING) && !me->HasUnitState(UNIT_STATE_STUNNED);
        });

        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_STUNNED);
        me->SetReactState(REACT_PASSIVE);
    }

    void Reset() override
    {
        _Reset();
        _towerCount = 0;
        _shutdown = 0;
        _shutout = true;
        _pursueTarget.Clear();

        me->SetReactState(REACT_DEFENSIVE);
        DoCastSelf(SPELL_INVIS_AND_STEALTH_DETECT);
    }

    void ScheduleTasks() override
    {
        using namespace std::placeholders;

        scheduler.Schedule(1ms, [this](TaskContext pursue)
        {
            Talk(SAY_TARGET);
            DoCast(SPELL_PURSUED);
            pursue.Repeat(35s);
        })
        .Schedule(1500ms, 4s, [this](TaskContext missile)
        {
            DoCast(me, SPELL_MISSILE_BARRAGE, true);
            missile.Repeat(2s);
        })
        .Schedule(20s, [this](TaskContext flame_vents)
        {
            DoCastAOE(SPELL_FLAME_VENTS);
            flame_vents.Repeat();
        })
        .Schedule(15s, [this](TaskContext speed)
        {
            DoCastAOE(SPELL_GATHERING_SPEED);
            speed.Repeat();
        })
        .Schedule(1s, [this](TaskContext summon)
        {
            if (summons.size() < 15)
                DoSummonFlyer(NPC_MECHANOLIFT, me, 30.0f, 50.0f, 0);

            summon.Repeat(2s);
        });
    }

    void EnterCombat(Unit* /*who*/) override
    {
        using namespace std::chrono_literals;
        using namespace std::placeholders;

        _EnterCombat();
        me->SetReactState(REACT_PASSIVE);

        me->ResetLootMode();
        if (instance->GetData(DATA_LEVIATHAN_HARD))
        {
            if (instance->GetData(DATA_TOWER_OF_STORMS))
            {
                ++_towerCount;
                DoCastSelf(SPELL_BUFF_TOWER_OF_STORMS);
                scheduler.Schedule(60s, 70s, std::bind(&boss_flame_leviathan::ThorimsHammer, this, _1));
            }

            if (instance->GetData(DATA_TOWER_OF_FLAMES))
            {
                ++_towerCount;
                DoCastSelf(SPELL_BUFF_TOWER_OF_FLAMES);
                scheduler.Schedule(50s, 60s, std::bind(&boss_flame_leviathan::MimironsInferno, this, _1));
            }

            if (instance->GetData(DATA_TOWER_OF_FROST))
            {
                ++_towerCount;
                DoCastSelf(SPELL_BUFF_TOWER_OF_FROST);
                scheduler.Schedule(25s, 35s, std::bind(&boss_flame_leviathan::HodirsFury, this, _1));
            }

            if (instance->GetData(DATA_TOWER_OF_LIFE))
            {
                ++_towerCount;
                DoCastSelf(SPELL_BUFF_TOWER_OF_LIFE);
                scheduler.Schedule(50s, 60s, std::bind(&boss_flame_leviathan::FreyasWard, this, _1));
            }

            if (_towerCount == 4)
                me->AddLootMode(LOOT_MODE_HARD_MODE_4);
            if (_towerCount >= 3)
                me->AddLootMode(LOOT_MODE_HARD_MODE_3);
            if (_towerCount >= 2)
                me->AddLootMode(LOOT_MODE_HARD_MODE_2);
            if (_towerCount >= 1)
                me->AddLootMode(LOOT_MODE_HARD_MODE_1);

            if (_towerCount)
                Talk(SAY_HARDMODE);
            else
                Talk(SAY_TOWER_NONE);
        }
        else
            Talk(SAY_AGGRO);
    }

    void JustDied(Unit* /*killer*/) override
    {
        _JustDied();
        // Set Field Flags 67108928 = 64 | 67108864 = UNIT_FLAG_UNK_6 | UNIT_FLAG_SKINNABLE
        // Set DynFlags 12
        // Set NPCFlags 0
        Talk(SAY_DEATH);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            //! Copypasta from DoSpellAttackIfReady, only difference is the target - it cannot be selected trough getVictim this way -
            //! I also removed the spellInfo check

            if (me->isAttackReady())
            {
                Unit* target = ObjectAccessor::GetUnit(*me, _pursueTarget);

                // Pursue was unable to acquire a valid target, so get the current victim as target.
                if (!target && me->GetVictim())
                    target = me->GetVictim();

                if (me->IsWithinCombatRange(target, 10.0f))
                {
                    DoCast(target, SPELL_BATTERING_RAM);
                    me->resetAttackTimer();
                }
            }
        });
    }

    void SpellHit(Unit* /*caster*/, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_START_THE_ENGINE)
            if (Vehicle* vehicleKit = me->GetVehicleKit())
                vehicleKit->InstallAllAccessories(false);

        if (spell->Id == SPELL_ELECTROSHOCK)
            me->InterruptSpell(CURRENT_CHANNELED_SPELL);

        if (spell->Id == SPELL_OVERLOAD_CIRCUIT && ++_shutdown == RAID_MODE(2, 4))
        {
            _shutdown = 0;
            SystemsShutdown();
            // me->InterruptNonMeleeSpells(true);
        }
    }

    void JustSummoned(Creature* summon) override
    {
        summons.Summon(summon);
        if (!summon->IsTrigger() && me->IsEngaged())
            DoZoneInCombat(summon);
    }

    void SpellHitTarget(Unit* target, SpellInfo const* spell) override
    {
        if (spell->Id == SPELL_PURSUED)
            _pursueTarget = target->GetGUID();
    }

    uint32 GetData(uint32 type) const override
    {
        switch (type)
        {
            case DATA_SHUTOUT:
                return _shutout ? 1 : 0;
            case DATA_ORBIT_ACHIEVEMENTS:
                return instance->GetData(DATA_LEVIATHAN_HARD) ? _towerCount : 0;
            default:
                break;
        }

        return 0;
    }

    void DoAction(int32 action) override
    {
        switch (action)
        {
            case ACTION_MOVE_TO_CENTER_POSITION: // Triggered by 2 Collossus near door
                if (!me->isDead())
                {
                    me->SetHomePosition(Center);
                    me->GetMotionMaster()->MoveCharge(Center.GetPositionX(), Center.GetPositionY(), Center.GetPositionZ()); // position center
                    me->SetReactState(REACT_AGGRESSIVE);
                    me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_STUNNED);
                    JustReachedHome();
                    return;
                }
                break;
            default:
                break;
        }
    }

    void EnterEvadeMode(EvadeReason why) override
    {
        BossAI::EnterEvadeMode(why);
        instance->SetData(DATA_SPAWN_VEHICLES, 1);
    }

private:
    void SystemsShutdown()
    {
        Talk(SAY_OVERLOAD);
        Talk(EMOTE_OVERLOAD);
        DoCastSelf(SPELL_SYSTEMS_SHUTDOWN, true);
        _shutout = false;
        // scheduler.DelayAll(20s);

        scheduler.Schedule(20s, [this](TaskContext /*shutdown_end*/)
        {
            me->RemoveAurasDueToSpell(SPELL_OVERLOAD_CIRCUIT);
        });
    }

    void ThorimsHammer(TaskContext /*context*/)
    {
        Talk(SAY_TOWER_STORM);

        for (uint8 i = 0; i < 7; ++i)
            if (Creature* thorim = DoSummon(NPC_THORIM_BEACON, me, 60.f, 20000, TEMPSUMMON_TIMED_DESPAWN))
                thorim->GetMotionMaster()->MoveRandom(100);
    }

    void MimironsInferno(TaskContext /*context*/)
    {
        Talk(SAY_TOWER_FLAME);
        me->SummonCreature(NPC_MIMIRON_BEACON, InfernoStart);
    }

    void HodirsFury(TaskContext /*context*/)
    {
        Talk(SAY_TOWER_FROST);

        for (uint8 i = 0; i < 7; ++i)
        {
            if (Creature* hodir = DoSummon(NPC_HODIR_BEACON, me, 50, 0, TEMPSUMMON_MANUAL_DESPAWN))
                hodir->GetMotionMaster()->MoveRandom(100);
        }
    }

    void FreyasWard(TaskContext /*context*/)
    {
        Talk(SAY_TOWER_NATURE);

        for (int32 i = 0; i < 4; ++i)
            me->SummonCreature(NPC_FREYA_BEACON, FreyaBeacons[i]);
    }

    ObjectGuid _pursueTarget;
    uint8 _towerCount;
    uint8 _shutdown;
    bool _shutout;
};

struct boss_flame_leviathan_seat : public ScriptedAI
{
    boss_flame_leviathan_seat(Creature* creature) : ScriptedAI(creature)
    {
        me->SetReactState(REACT_PASSIVE);
        instance = creature->GetInstanceScript();
    }

    struct InstallEvent : public BasicEvent
    {
        InstallEvent(Unit* owner) : _owner(owner) {}

        bool Execute(uint64 /*e_time*/, uint32 /*p_time*/)
        {
            if (_owner->GetVehicleKit())
                _owner->GetVehicleKit()->Reset(false);

            return true;
        }

    private:
        Unit* _owner;
    };

    InstanceScript* instance;

    void JustAppeared() override
    {
        me->m_Events.AddEvent(new InstallEvent(me), me->m_Events.CalculateTime(500));
    }

    void PassengerBoarded(Unit* who, int8 seatId, bool apply) override
    {
        if (!me->GetVehicle())
            return;

        if (seatId == SEAT_PLAYER)
        {
            if (!apply)
                return;
            else if (Creature* leviathan = me->GetVehicleCreatureBase())
                leviathan->AI()->Talk(SAY_PLAYER_RIDING);

            if (Unit* turret = me->GetVehicleKit()->GetPassenger(SEAT_TURRET))
                if (turret->IsAIEnabled)
                    turret->GetAI()->AttackStart(who);

            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        }
        else if (seatId == SEAT_TURRET)
        {
            if (!apply && me->GetVehicleKit())
            {
                if (Unit* device = me->GetVehicleKit()->GetPassenger(SEAT_DEVICE))
                {
                    device->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
                    device->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                }
            }
        }
    }
};

struct boss_flame_leviathan_defense_cannon : public ScriptedAI
{
    boss_flame_leviathan_defense_cannon(Creature* creature) : ScriptedAI(creature), _napalmTimer(5 * IN_MILLISECONDS) { }

    // void JustAppeared() override
    // {
    //     me->ChangeSeat(SEAT_CANNON, true);
    // }

    void Reset() override
    {
        DoCastSelf(AURA_STEALTH_DETECTION);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        if (_napalmTimer <= diff)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
                if (CanAIAttack(target))
                    DoCast(target, SPELL_NAPALM, true);

            _napalmTimer = 5000;
        }
        else
            _napalmTimer -= diff;
    }

    bool CanAIAttack(Unit const* who) const override
    {
        return who->GetTypeId() == TYPEID_PLAYER && who->GetVehicle() && who->GetVehicleBase()->GetEntry() != NPC_SEAT;
    }

private:
    uint32 _napalmTimer;
};

struct boss_flame_leviathan_defense_turret : public TurretAI
{
    boss_flame_leviathan_defense_turret(Creature* creature) : TurretAI(creature) { }

    void DamageTaken(Unit* who, uint32 &damage) override
    {
        if (!CanAIAttack(who))
            damage = 0;
    }

    bool CanAIAttack(Unit const* who) const override
    {
        return who->GetTypeId() == TYPEID_PLAYER && who->GetVehicle() && who->GetVehicleBase()->GetEntry() == NPC_SEAT;
    }
};

struct boss_flame_leviathan_overload_device : public PassiveAI
{
    boss_flame_leviathan_overload_device(Creature* creature) : PassiveAI(creature) { }

    void OnSpellClick(Unit* /*clicker*/, bool& result) override
    {
        if (!result)
            return;

        if (me->GetVehicle())
        {
            me->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);

            if (Unit* player = me->GetVehicle()->GetPassenger(SEAT_PLAYER))
            {
                me->GetVehicleBase()->CastSpell(player, SPELL_SMOKE_TRAIL, true);
                player->ExitVehicle();
                player->GetMotionMaster()->MoveKnockbackFrom(me->GetVehicleBase()->GetPositionX(), me->GetVehicleBase()->GetPositionY(), 30, 30);
            }
        }
    }
};

struct boss_flame_leviathan_safety_container : public PassiveAI
{
    boss_flame_leviathan_safety_container(Creature* creature) : PassiveAI(creature) { }

    void JustDied(Unit* /*killer*/) override
    {
        float x, y, z;
        me->GetPosition(x, y, z);
        z = me->GetMap()->GetHeight(me->GetPhaseMask(), x, y, z);
        me->GetMotionMaster()->MovePoint(0, x, y, z);
        me->UpdatePosition(x, y, z, 0);
    }

    void UpdateAI(uint32 /*diff*/) override
    {
    }
};

struct npc_mechanolift : public PassiveAI
{
    npc_mechanolift(Creature* creature) : PassiveAI(creature), _moveTimer(0) { }

    void JustAppeared() override
    {
        me->GetMotionMaster()->MoveRandom(50);
    }

    void JustDied(Unit* /*killer*/) override
    {
        me->GetMotionMaster()->MoveTargetedHome();
        DoCast(SPELL_DUSTY_EXPLOSION);
        Creature* liquid = DoSummon(NPC_LIQUID, me, 0);

        if (liquid)
        {
            liquid->CastSpell(liquid, SPELL_LIQUID_PYRITE, true);
            liquid->CastSpell(liquid, SPELL_DUST_CLOUD_IMPACT, true);
        }
    }

    void MovementInform(uint32 type, uint32 id) override
    {
        if (type == POINT_MOTION_TYPE && id == 1)
            if (Creature* container = me->FindNearestCreature(NPC_CONTAINER, 5, true))
                container->EnterVehicle(me);
    }

    void UpdateAI(uint32 diff) override
    {
        if (_moveTimer <= diff)
        {
            if (me->GetVehicleKit() && me->GetVehicleKit()->HasEmptySeat(-1))
            {
                Creature* container = me->FindNearestCreature(NPC_CONTAINER, 50, true);
                if (container && !container->GetVehicle())
                    me->GetMotionMaster()->MovePoint(1, container->GetPositionX(), container->GetPositionY(), container->GetPositionZ());
            }

            _moveTimer = 30000; //check next 30 seconds
        }
        else
            _moveTimer -= diff;
    }

private:
    uint32 _moveTimer;
};

struct npc_pool_of_tar : public ScriptedAI
{
    npc_pool_of_tar(Creature* creature) : ScriptedAI(creature)
    {
        // me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
        me->SetReactState(REACT_PASSIVE);
        me->CastSpell(me, SPELL_TAR_PASSIVE, true);
    }

    void DamageTaken(Unit* /*who*/, uint32& damage) override
    {
        damage = 0;
    }

    void SpellHit(Unit* /*caster*/, SpellInfo const* spell) override
    {
        if (spell->SchoolMask & SPELL_SCHOOL_MASK_FIRE && !me->HasAura(SPELL_BLAZE))
            me->CastSpell(me, SPELL_BLAZE, true);
    }

    void UpdateAI(uint32 /*diff*/) override { }
};

struct npc_colossus : public ScriptedAI
{
    npc_colossus(Creature* creature) : ScriptedAI(creature),  instance(creature->GetInstanceScript()) { }

    void JustDied(Unit* /*killer*/) override
    {
        if (me->GetHomePosition().IsInDist(&Center, 50.f))
            instance->SetData(DATA_COLOSSUS, instance->GetData(DATA_COLOSSUS)+1);
    }

    void UpdateAI(uint32 /*diff*/) override
    {
        if (!UpdateVictim())
            return;

        DoMeleeAttackIfReady();
    }

private:
    InstanceScript* instance;
};

template<typename ParentAI>
struct leviathan_beacon_summonerAI : public ParentAI
{
    leviathan_beacon_summonerAI(Creature* creature) : ParentAI(creature), instance(creature->GetInstanceScript())
    {
        creature->SetReactState(REACT_PASSIVE);
    }

    void JustSummoned(Creature* summon) override
    {
        if (instance)
            if (Creature* levi = instance->GetCreature(BOSS_LEVIATHAN))
                levi->AI()->JustSummoned(summon);
    }

    void SummonedCreatureDespawn(Creature* summon) override
    {
        if (instance)
            if (Creature* levi = instance->GetCreature(BOSS_LEVIATHAN))
                levi->AI()->SummonedCreatureDespawn(summon);
    }

private:
    InstanceScript* instance;
};

template<uint32 SpellId, uint32 BeaconId>
struct leviathan_beacon_dropperAI : public leviathan_beacon_summonerAI<ScriptedAI>
{
    leviathan_beacon_dropperAI(Creature* creature) : leviathan_beacon_summonerAI(creature) { }

    void MoveInLineOfSight(Unit* who) override
    {
        if (_delayTimer.Passed() && who->IsVehicle() && who->IsControlledByPlayer() && me->IsInRange(who, 0, 10, false))
        {
            _delayTimer.Reset(5000);
            if (Creature* trigger = DoSummonFlyer(BeaconId, me, TARGET_BEACON_HEIGHT, 0, 30000, TEMPSUMMON_TIMED_DESPAWN))
                trigger->CastSpell(nullptr, SpellId, true);
        }
    }

    void UpdateAI(uint32 diff) override
    {
        UpdateVictim();
        _delayTimer.Update(diff);
    }

private:
    TimeTrackerSmall _delayTimer;
};

struct npc_thorims_hammer : public leviathan_beacon_dropperAI<SPELL_THORIM_S_HAMMER, NPC_THORIM_TARGET_BEACON>
{
    npc_thorims_hammer(Creature* creature) : leviathan_beacon_dropperAI(creature) { }

    void JustAppeared() override
    {
        DoCastSelf(SPELL_THORIM_BEACON_VISUAL, true);
    }
};

struct npc_hodirs_fury : public leviathan_beacon_dropperAI<SPELL_HODIR_S_FURY, NPC_HODIR_TARGET_BEACON>
{
    npc_hodirs_fury(Creature* creature) : leviathan_beacon_dropperAI(creature) { }

    void JustAppeared() override
    {
        DoCastSelf(SPELL_HODIR_BEACON_VISUAL, true);
    }
};

struct npc_freyas_ward : public leviathan_beacon_summonerAI<ScriptedAI>
{
    npc_freyas_ward(Creature* creature) : leviathan_beacon_summonerAI(creature), _summonTimer(5000) { }

    void JustAppeared() override
    {
        DoCastSelf(SPELL_FREYA_BEACON_VISUAL, true);
    }

    void UpdateAI(uint32 diff) override
    {
        if (_summonTimer <= diff)
        {
            if (Creature* trigger = DoSummonFlyer(NPC_FREYA_TARGET_BEACON, me, TARGET_BEACON_HEIGHT, 0, 1000, TEMPSUMMON_TIMED_DESPAWN))
                trigger->CastSpell(nullptr, SPELL_FREYA_S_WARD, true);

            _summonTimer = 20000;
        }
        else
            _summonTimer -= diff;

        UpdateVictim();
    }

private:
    uint32 _summonTimer;
};

struct npc_mimirons_inferno : public leviathan_beacon_summonerAI<EscortAI>
{
    enum Timers
    {
        INFERNO_TIMER = 4000
    };

    npc_mimirons_inferno(Creature* creature) : leviathan_beacon_summonerAI(creature), infernoTimer(INFERNO_TIMER) { }

    void JustAppeared() override
    {
        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_SELECTABLE);
        DoCastSelf(SPELL_MIMIRON_BEACON_VISUAL, true);
    }

    void UpdateEscortAI(uint32 diff) override
    {
        if (!HasEscortState(STATE_ESCORT_ESCORTING))
            Start(false, true, ObjectGuid::Empty, nullptr, false, true);
        else
        {
            if (infernoTimer <= diff)
            {
                if (Creature* trigger = DoSummonFlyer(NPC_MIMIRON_TARGET_BEACON, me, TARGET_BEACON_HEIGHT, 0, 30000, TEMPSUMMON_TIMED_DESPAWN))
                    trigger->CastSpell(nullptr, SPELL_MIMIRON_S_INFERNO, true);

                infernoTimer = INFERNO_TIMER;
            }
            else
                infernoTimer -= diff;
        }
    }

private:
    uint32 infernoTimer;
};

struct npc_freya_ward_summon : public ScriptedAI
{
    npc_freya_ward_summon(Creature* creature) : ScriptedAI(creature), _lashTimer(5000) { }

    void JustAppeared() override
    {
        if (!me->IsEngaged())
            me->GetMotionMaster()->MoveRandom(100);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        if (_lashTimer <= diff)
        {
            DoCast(SPELL_LASH);
            _lashTimer = 20000;
        }
        else
            _lashTimer -= diff;

        DoMeleeAttackIfReady();
    }

private:
    uint32 _lashTimer;
};

// TODO: better intro, video: https://www.youtube.com/watch?v=kLx31SXqW4g
struct npc_lorekeeper : public ScriptedAI
{
    enum Gossips
    {
        GOSSIP_MENU_LORE_KEEPER   = 10477,
        GOSSIP_OPTION_LORE_KEEPER = 0
    };

    npc_lorekeeper(Creature* creature) : ScriptedAI(creature), _instance(creature->GetInstanceScript()) { }

    bool GossipSelect(Player* player, uint32 menuId, uint32 gossipListId) override
    {
        if (menuId == GOSSIP_MENU_LORE_KEEPER && gossipListId == GOSSIP_OPTION_LORE_KEEPER)
        {
            me->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
            player->PlayerTalkClass->SendCloseGossip();
            _instance->SetData(DATA_LEVIATHAN_HARD, 1);
            _instance->SetData(DATA_SPAWN_VEHICLES, 0);

            me->SetVisible(false);
            if (Creature* brann = _instance->GetCreature(DATA_BRANN_BRONZEBEARD_INTRO))
            {
                brann->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);

                if (Creature* delorah = _instance->GetCreature(DATA_DELLORAH))
                {
                    delorah->GetMotionMaster()->MovePoint(0, brann->GetPositionX() - 4, brann->GetPositionY(), brann->GetPositionZ());
                    /// @todo delorah->AI()->Talk(xxxx, brann->GetGUID()); when reached brann
                }
            }
        }

        return false;
    }

private:
    InstanceScript* _instance;
};

struct npc_ulduar_gauntlet_generator : public PassiveAI
{
    enum AIData
    {
        MAX_SPAWN_COUNT = 40,
        SPAWN_INTERVAL  = 2000,
    };

    npc_ulduar_gauntlet_generator(Creature* creature) : PassiveAI(creature), _spawning(false), _count(0), _timer(SPAWN_INTERVAL) { }

    void MoveInLineOfSight(Unit* who) override
    {
        if (who->IsControlledByPlayer() && me->IsWithinDist(who, 60.0f))
        {
            _spawning = true;
            _target = who->GetGUID();
        }
    }

    void UpdateAI(uint32 diff) override
    {
        if (_timer <= diff)
        {
            if (_count <= MAX_SPAWN_COUNT)
                if (Creature* cr = me->SummonCreature(NPC_STEELFORGED_DEFFENDER, *me, TEMPSUMMON_CORPSE_TIMED_DESPAWN, 5000))
                    if (Unit* target = FindTarget(cr))
                        cr->EngageWithTarget(target);

            _timer = SPAWN_INTERVAL;
        }
        else
            _timer -= diff;
    }

    void JustSummoned(Creature* /*summon*/) override
    {
        ++_count;
    }

    void SummonedCreatureDies(Creature* /*summon*/, Unit* /*killer*/) override
    {
        --_count;
    }

private:
    Unit* FindTarget(Creature* checker)
    {
        if (Unit* target = ObjectAccessor::GetUnit(*me, _target))
            if (checker->IsValidAttackTarget(target))
                return target;

        Unit* target = nullptr;

        Trinity::NearestHostileUnitInAggroRangeCheck u_check(checker, true);
        Trinity::UnitSearcher<Trinity::NearestHostileUnitInAggroRangeCheck> searcher(checker, target, u_check);

        Cell::VisitGridObjects(checker, searcher, 100.0f);

        return target;
    }

    bool _spawning;
    uint32 _count;
    uint32 _timer;
    ObjectGuid _target;
};

class go_ulduar_tower : public GameObjectScript
{
    public:
        go_ulduar_tower() : GameObjectScript("go_ulduar_tower") { }

        struct go_ulduar_towerAI : public GameObjectAI
        {
            go_ulduar_towerAI(GameObject* go) : GameObjectAI(go), instance(go->GetInstanceScript()) { }

            InstanceScript* instance;

            void Destroyed(Player* /*player*/, uint32 /*eventId*/) override
            {
                switch (me->GetEntry())
                {
                    case GO_TOWER_OF_STORMS:
                        instance->ProcessEvent(me, EVENT_TOWER_OF_STORM_DESTROYED);
                        break;
                    case GO_TOWER_OF_FLAMES:
                        instance->ProcessEvent(me, EVENT_TOWER_OF_FLAMES_DESTROYED);
                        break;
                    case GO_TOWER_OF_FROST:
                        instance->ProcessEvent(me, EVENT_TOWER_OF_FROST_DESTROYED);
                        break;
                    case GO_TOWER_OF_LIFE:
                        instance->ProcessEvent(me, EVENT_TOWER_OF_LIFE_DESTROYED);
                        break;
                }

                if (Creature* trigger = me->FindNearestCreature(NPC_ULDUAR_GAUNTLET_GENERATOR, 15.0f, true))
                    trigger->DisappearAndDie();
            }
        };

        GameObjectAI* GetAI(GameObject* go) const override
        {
            return GetUlduarAI<go_ulduar_towerAI>(go);
        }
};

class achievement_three_car_garage_demolisher : public AchievementCriteriaScript
{
    public:
        achievement_three_car_garage_demolisher() : AchievementCriteriaScript("achievement_three_car_garage_demolisher") { }

        bool OnCheck(Player* source, Unit* /*target*/) override
        {
            if (Creature* vehicle = source->GetVehicleCreatureBase())
            {
                if (vehicle->GetEntry() == VEHICLE_DEMOLISHER)
                    return true;
            }

            return false;
        }
};

class achievement_three_car_garage_chopper : public AchievementCriteriaScript
{
    public:
        achievement_three_car_garage_chopper() : AchievementCriteriaScript("achievement_three_car_garage_chopper") { }

        bool OnCheck(Player* source, Unit* /*target*/) override
        {
            if (Creature* vehicle = source->GetVehicleCreatureBase())
            {
                if (vehicle->GetEntry() == VEHICLE_CHOPPER)
                    return true;
            }

            return false;
        }
};

class achievement_three_car_garage_siege : public AchievementCriteriaScript
{
    public:
        achievement_three_car_garage_siege() : AchievementCriteriaScript("achievement_three_car_garage_siege") { }

        bool OnCheck(Player* source, Unit* /*target*/) override
        {
            if (Creature* vehicle = source->GetVehicleCreatureBase())
            {
                if (vehicle->GetEntry() == VEHICLE_SIEGE)
                    return true;
            }

            return false;
        }
};

class achievement_shutout : public AchievementCriteriaScript
{
    public:
        achievement_shutout() : AchievementCriteriaScript("achievement_shutout") { }

        bool OnCheck(Player* /*source*/, Unit* target) override
        {
            if (target)
                if (Creature* leviathan = target->ToCreature())
                    if (leviathan->AI()->GetData(DATA_SHUTOUT))
                        return true;

            return false;
        }
};

class achievement_orbital_bombardment : public AchievementCriteriaScript
{
    public:
        achievement_orbital_bombardment() : AchievementCriteriaScript("achievement_orbital_bombardment") { }

        bool OnCheck(Player* /*source*/, Unit* target) override
        {
            if (!target)
                return false;

            if (Creature* Leviathan = target->ToCreature())
                if (Leviathan->AI()->GetData(DATA_ORBIT_ACHIEVEMENTS) >= 1)
                    return true;

            return false;
        }
};

class achievement_orbital_devastation : public AchievementCriteriaScript
{
    public:
        achievement_orbital_devastation() : AchievementCriteriaScript("achievement_orbital_devastation") { }

        bool OnCheck(Player* /*source*/, Unit* target) override
        {
            if (!target)
                return false;

            if (Creature* Leviathan = target->ToCreature())
                if (Leviathan->AI()->GetData(DATA_ORBIT_ACHIEVEMENTS) >= 2)
                    return true;

            return false;
        }
};

class achievement_nuked_from_orbit : public AchievementCriteriaScript
{
    public:
        achievement_nuked_from_orbit() : AchievementCriteriaScript("achievement_nuked_from_orbit") { }

        bool OnCheck(Player* /*source*/, Unit* target) override
        {
            if (!target)
                return false;

            if (Creature* Leviathan = target->ToCreature())
                if (Leviathan->AI()->GetData(DATA_ORBIT_ACHIEVEMENTS) >= 3)
                    return true;

            return false;
        }
};

class achievement_orbit_uary : public AchievementCriteriaScript
{
    public:
        achievement_orbit_uary() : AchievementCriteriaScript("achievement_orbit_uary") { }

        bool OnCheck(Player* /*source*/, Unit* target) override
        {
            if (!target)
                return false;

            if (Creature* Leviathan = target->ToCreature())
                if (Leviathan->AI()->GetData(DATA_ORBIT_ACHIEVEMENTS) == 4)
                    return true;

            return false;
        }
};

class spell_load_into_catapult : public AuraScript
{
    PrepareAuraScript(spell_load_into_catapult);

    enum Spells
    {
        SPELL_PASSENGER_LOADED = 62340,
    };

    void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* owner = GetOwner()->ToUnit();
        if (!owner)
            return;

        owner->CastSpell(owner, SPELL_PASSENGER_LOADED, true);
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        Unit* owner = GetOwner()->ToUnit();
        if (!owner)
            return;

        owner->RemoveAurasDueToSpell(SPELL_PASSENGER_LOADED);
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_load_into_catapult::OnApply, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectRemoveFn(spell_load_into_catapult::OnRemove, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL_OR_REAPPLY_MASK);
    }
};

class spell_auto_repair : public SpellScript
{
    PrepareSpellScript(spell_auto_repair);

    enum Spells
    {
        SPELL_AUTO_REPAIR = 62705,
    };

    void CheckCooldownForTarget()
    {
        if (GetHitUnit()->HasAuraEffect(SPELL_AUTO_REPAIR, EFFECT_2))   // Check presence of dummy aura indicating cooldown
        {
            PreventHitEffect(EFFECT_0);
            PreventHitDefaultEffect(EFFECT_1);
            PreventHitDefaultEffect(EFFECT_2);
            //! Currently this doesn't work: if we call PreventHitAura(), the existing aura will be removed
            //! because of recent aura refreshing changes. Since removing the existing aura negates the idea
            //! of a cooldown marker, we just let the dummy aura refresh itself without executing the other spelleffects.
            //! The spelleffects can be executed by letting the dummy aura expire naturally.
            //! This is a temporary solution only.
            //PreventHitAura();
        }
    }

    void HandleScript(SpellEffIndex /*eff*/)
    {
        Vehicle* vehicle = GetHitUnit()->GetVehicleKit();
        if (!vehicle)
            return;

        Unit* driver = vehicle->GetPassenger(0);
        if (!driver)
            return;

        driver->TextEmote(EMOTE_REPAIR, driver, true);

        InstanceScript* instance = driver->GetInstanceScript();
        if (!instance)
            return;

        // Actually should/could use basepoints (100) for this spell effect as percentage of health, but oh well.
        vehicle->GetBase()->SetFullHealth();

        // For achievement
        instance->SetData(DATA_UNBROKEN, 0);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_auto_repair::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        BeforeHit += SpellHitFn(spell_auto_repair::CheckCooldownForTarget);
    }
};

class spell_systems_shutdown : public AuraScript
{
    PrepareAuraScript(spell_systems_shutdown);

    void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Creature* owner = GetOwner()->ToCreature())
        {
            owner->SetControlled(true, UNIT_STATE_STUNNED);
            owner->RemoveAurasDueToSpell(SPELL_GATHERING_SPEED);
        }
    }

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Creature* owner = GetOwner()->ToCreature())
            owner->SetControlled(false, UNIT_STATE_STUNNED);
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_systems_shutdown::OnApply, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectRemoveFn(spell_systems_shutdown::OnRemove, EFFECT_0, SPELL_AURA_MOD_DAMAGE_PERCENT_TAKEN, AURA_EFFECT_HANDLE_REAL);
    }
};

class FlameLeviathanPursuedTargetSelector
{
    enum Area
    {
        AREA_FORMATION_GROUNDS = 4652,
    };

    Unit const* _me;

public:
    explicit FlameLeviathanPursuedTargetSelector(Unit* unit) : _me(unit) { };

    bool operator()(WorldObject* target) const
    {
        //! No players, only vehicles. Pursue is never cast on players.
        Creature* creatureTarget = target->ToCreature();
        if (!creatureTarget)
            return true;

        //! NPC entries must match
        if (creatureTarget->GetEntry() != NPC_SALVAGED_DEMOLISHER && creatureTarget->GetEntry() != NPC_SALVAGED_SIEGE_ENGINE)
            return true;

        //! NPC must be a valid vehicle installation
        Vehicle* vehicle = creatureTarget->GetVehicleKit();
        if (!vehicle)
            return true;

        //! Entity needs to be in appropriate area
        if (target->GetAreaId() != AREA_FORMATION_GROUNDS)
            return true;

        //! Vehicle must be in use by player
        bool playerFound = false;
        for (SeatMap::const_iterator itr = vehicle->Seats.begin(); itr != vehicle->Seats.end() && !playerFound; ++itr)
            if (itr->second.Passenger.Guid.IsPlayer())
                playerFound = true;

        return !playerFound;
    }
};

class spell_pursue : public SpellScript
{
    PrepareSpellScript(spell_pursue);

public:
    spell_pursue() : _target(nullptr) { }

private:
    void FilterTargets(std::list<WorldObject*>& targets)
    {
        targets.remove_if(FlameLeviathanPursuedTargetSelector(GetCaster()));
        if (!targets.empty())
        {
            //! In the end, only one target should be selected
            _target = Trinity::Containers::SelectRandomContainerElement(targets);
            FilterTargetsSubsequently(targets);
        }
    }

    void FilterTargetsSubsequently(std::list<WorldObject*>& targets)
    {
        targets.clear();
        if (_target)
            targets.push_back(_target);
    }

    void HandleScript(SpellEffIndex /*eff*/)
    {
        Creature* caster = GetCaster()->ToCreature();
        if (!caster)
            return;

        caster->AI()->AttackStart(GetHitUnit());    // Chase target

        for (SeatMap::const_iterator itr = caster->GetVehicleKit()->Seats.begin(); itr != caster->GetVehicleKit()->Seats.end(); ++itr)
        {
            if (Player* passenger = ObjectAccessor::GetPlayer(*caster, itr->second.Passenger.Guid))
            {
                caster->AI()->Talk(EMOTE_PURSUE, passenger);
                return;
            }
        }
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pursue::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_pursue::FilterTargetsSubsequently, EFFECT_1, TARGET_UNIT_SRC_AREA_ENEMY);
        OnEffectHitTarget += SpellEffectFn(spell_pursue::HandleScript, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
    }

    WorldObject* _target;
};

class spell_vehicle_throw_passenger : public SpellScript
{
    PrepareSpellScript(spell_vehicle_throw_passenger);
    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        SpellCastTargets& spellTargets = GetSpell()->m_targets;

        if (spellTargets.HasTraj() && GetCaster()->GetVehicleKit())
        {
            if (Unit* passenger = GetCaster()->GetVehicleKit()->GetPassenger(GetEffectValue() - 1))
            {
                float minDist = std::numeric_limits<float>::max();
                Unit* target = nullptr;
                std::list<Creature*> targets;
                Trinity::AllCreaturesOfEntryInRange u_check(GetCaster(), NPC_SEAT, 150.0f);
                Trinity::CreatureListSearcher<Trinity::AllCreaturesOfEntryInRange> searcher(GetCaster(), targets, u_check);
                Cell::VisitAllObjects(GetCaster(), searcher, 150.0f);

                for (Unit* unit : targets)
                {
                    if (Vehicle* seat = unit->GetVehicleKit())
                        if (!seat->GetPassenger(0))
                            if (seat->GetPassenger(2) && !seat->GetPassenger(2)->GetCurrentSpell(CURRENT_CHANNELED_SPELL))
                            {
                                float dist = unit->GetExactDistSq(spellTargets.GetDstPos());
                                if (dist < minDist)
                                {
                                    minDist = dist;
                                    target = unit;
                                }
                            }
                }

                if (target /*&& target->IsWithinDist2d(targets.GetDstPos(), GetSpellInfo()->Effects[effIndex].CalcRadius() * 2)*/) // now we use *2 because the location of the seat is not correct
                    passenger->EnterVehicle(target, 0);
                else
                {
                    passenger->ExitVehicle();
                    passenger->GetMotionMaster()->MoveJump(*spellTargets.GetDstPos(), spellTargets.GetSpeedXY(), spellTargets.GetSpeedZ());
                }
            }
        }
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_vehicle_throw_passenger::HandleScript, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

class spell_leviathan_freyas_ward : public SpellScript
{
    PrepareSpellScript(spell_leviathan_freyas_ward);

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (GetCaster())
            if (SpellInfo const* info = sSpellMgr->GetSpellInfo(GetEffectValue()))
                GetCaster()->CastSpell(GetSpell()->m_targets, info, nullptr, TRIGGERED_FULL_MASK);
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_leviathan_freyas_ward::HandleDummy, EFFECT_1, SPELL_EFFECT_DUMMY);
    }
};

class spell_leviathan_flames : public SpellScript
{
    PrepareSpellScript(spell_leviathan_flames);

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        if (GetHitUnit()->HasAura(GetEffectValue()))
            GetHitUnit()->RemoveAurasDueToSpell(GetEffectValue());
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_leviathan_flames::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

class spell_leviathan_vehicle_ride : public AuraScript
{
    PrepareAuraScript(spell_leviathan_vehicle_ride);

    enum Misc
    {
        SPELL_RIDE_DEMOLISHER = 62309,
        SPELL_RIDE_SIEGE = 65031,
        NPC_DEMOLISHER_SEAT = 33167,
        NPC_SIEGE_TURRET = 33067,
        SEAT_ID_DEMOLISHER = 1,
        SEAT_ID_SIEGE = 7,
    };

    void OnApply(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (GetCaster()->GetTypeId() != TYPEID_PLAYER)
            return;

        PreventDefaultAction();

        Vehicle* targetVeh = GetTarget()->GetVehicleKit();
        if (!targetVeh)
            return;

        Unit* caster = GetCaster();
        if (caster == GetTarget())
            return;

        if (targetVeh->GetPassenger(0) && targetVeh->GetPassenger(0)->GetTypeId() == TYPEID_PLAYER)
        {
            bool isDemo = GetId() == SPELL_RIDE_DEMOLISHER;
            if (Unit* seat = targetVeh->GetPassenger(isDemo ? SEAT_ID_DEMOLISHER : SEAT_ID_SIEGE))
            {
                if (seat->GetTypeId() == TYPEID_UNIT && seat->GetEntry() == (isDemo ? NPC_DEMOLISHER_SEAT : NPC_SIEGE_TURRET))
                {
                    caster->EnterVehicle(seat, -1);
                    return;
                }
            }
        }

        caster->_EnterVehicle(targetVeh, 0, GetTargetApplication());
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_leviathan_vehicle_ride::OnApply, EFFECT_0, SPELL_AURA_CONTROL_VEHICLE, AURA_EFFECT_HANDLE_REAL);
    }
};

// From SunwellCore
class spell_vehicle_grab_pyrite : public SpellScript
{
    PrepareSpellScript(spell_vehicle_grab_pyrite);

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        if (Unit* target = GetHitUnit())
        {
            if (Unit* seat = GetCaster()->GetVehicleBase())
            {
                if (Vehicle* vSeat = seat->GetVehicleKit())
                    if (Unit* pyrite = vSeat->GetPassenger(1))
                        pyrite->ExitVehicle();

                if (Unit* parent = seat->GetVehicleBase())
                {
                    GetCaster()->CastSpell(parent, SPELL_ADD_PYRITE, true);
                    target->CastSpell(seat, GetEffectValue());

                    if (target->GetTypeId() == TYPEID_UNIT)
                        target->ToCreature()->DespawnOrUnsummon(1300);
                }
            }
        }
    }

    void Register()
    {
        OnEffectHitTarget += SpellEffectFn(spell_vehicle_grab_pyrite::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

void AddSC_boss_flame_leviathan()
{
    RegisterUlduarCreatureAI(boss_flame_leviathan);
    RegisterUlduarCreatureAI(boss_flame_leviathan_seat);
    RegisterUlduarCreatureAI(boss_flame_leviathan_defense_turret);
    RegisterUlduarCreatureAI(boss_flame_leviathan_defense_cannon);
    RegisterUlduarCreatureAI(boss_flame_leviathan_overload_device);
    RegisterUlduarCreatureAI(boss_flame_leviathan_safety_container);
    RegisterUlduarCreatureAI(npc_mechanolift);
    RegisterUlduarCreatureAI(npc_pool_of_tar);
    RegisterUlduarCreatureAI(npc_colossus);
    RegisterUlduarCreatureAI(npc_thorims_hammer);
    RegisterUlduarCreatureAI(npc_mimirons_inferno);
    RegisterUlduarCreatureAI(npc_hodirs_fury);
    RegisterUlduarCreatureAI(npc_freyas_ward);
    RegisterUlduarCreatureAI(npc_freya_ward_summon);
    RegisterUlduarCreatureAI(npc_lorekeeper);
    RegisterUlduarCreatureAI(npc_ulduar_gauntlet_generator);
    new go_ulduar_tower();

    new achievement_three_car_garage_demolisher();
    new achievement_three_car_garage_chopper();
    new achievement_three_car_garage_siege();
    new achievement_shutout();
    new achievement_orbital_bombardment();
    new achievement_orbital_devastation();
    new achievement_nuked_from_orbit();
    new achievement_orbit_uary();

    RegisterAuraScript(spell_load_into_catapult);
    RegisterSpellScript(spell_auto_repair);
    RegisterAuraScript(spell_systems_shutdown);
    RegisterSpellScript(spell_pursue);
    RegisterSpellScript(spell_vehicle_throw_passenger);
    RegisterSpellScript(spell_leviathan_freyas_ward);
    RegisterSpellScript(spell_leviathan_flames);
    RegisterAuraScript(spell_leviathan_vehicle_ride);
    RegisterSpellScript(spell_vehicle_grab_pyrite);
}
