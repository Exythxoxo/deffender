/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "Map.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "SpellAuras.h"
#include "SpellScript.h"
#include "TemporarySummon.h"
#include "ulduar.h"

using namespace std::chrono_literals;

enum FreyaYells
{
    // Freya
    SAY_AGGRO                                    = 0,
    SAY_AGGRO_WITH_ELDER                         = 1,
    SAY_SLAY                                     = 2,
    SAY_DEATH                                    = 3,
    SAY_BERSERK                                  = 4,
    SAY_SUMMON_CONSERVATOR                       = 5,
    SAY_SUMMON_TRIO                              = 6,
    SAY_SUMMON_LASHERS                           = 7,
    EMOTE_LIFEBINDERS_GIFT                       = 8,
    EMOTE_ALLIES_OF_NATURE                       = 9,
    EMOTE_GROUND_TREMOR                          = 10,
    EMOTE_IRON_ROOTS                             = 11,

    // Elder Brightleaf / Elder Ironbranch / Elder Stonebark
    SAY_ELDER_AGGRO                              = 0,
    SAY_ELDER_SLAY                               = 1,
    SAY_ELDER_DEATH                              = 2
};

enum FreyaSpells
{
    SPELL_FEIGN_DEATH                            = 29266,

    // Freya
    SPELL_ATTUNED_TO_NATURE                      = 62519,
    SPELL_TOUCH_OF_EONAR                         = 62528,
    SPELL_SUNBEAM                                = 62623,
    SPELL_ENRAGE                                 = 47008,
    SPELL_FREYA_GROUND_TREMOR                    = 62437,
    SPELL_ROOTS_FREYA                            = 62439,
    SPELL_ROOTS_FREYA_TRIGGER                    = 62438,
    SPELL_STONEBARK_ESSENCE                      = 62483,
    SPELL_IRONBRANCH_ESSENCE                     = 62484,
    SPELL_BRIGHTLEAF_ESSENCE                     = 62485,
    SPELL_DRAINED_OF_POWER                       = 62467,
    SPELL_SUMMON_EONAR_GIFT                      = 62572,

    // Stonebark
    SPELL_FISTS_OF_STONE                         = 62344,
    SPELL_GROUND_TREMOR                          = 62325,
    SPELL_PETRIFIED_BARK                         = 62337,
    SPELL_PETRIFIED_BARK_DMG                     = 62379,

    // Ironbranch
    SPELL_IMPALE                                 = 62310,
    SPELL_ROOTS_IRONBRANCH                       = 62275,
    SPELL_ROOTS_IRONBRANCH_TRIGGER               = 62283,
    SPELL_THORN_SWARM                            = 62285,

    // Brightleaf
    SPELL_FLUX_AURA                              = 62239,
    SPELL_FLUX                                   = 62262,
    SPELL_FLUX_PLUS                              = 62251,
    SPELL_FLUX_MINUS                             = 62252,
    SPELL_SOLAR_FLARE                            = 62240,
    SPELL_UNSTABLE_SUN_BEAM_SUMMON               = 62207, // Trigger 62221

    // Stack Removing of Attuned to Nature
    SPELL_REMOVE_25STACK                         = 62521,
    SPELL_REMOVE_10STACK                         = 62525,
    SPELL_REMOVE_2STACK                          = 62524,

    // Achievement spells
    SPELL_DEFORESTATION_CREDIT                   = 65015,
    SPELL_KNOCK_ON_WOOD_CREDIT                   = 65074,

    // Wave summoning spells
    SPELL_SUMMON_LASHERS                         = 62688,
    SPELL_SUMMON_TRIO                            = 62686,
    SPELL_SUMMON_ANCIENT_CONSERVATOR             = 62685,

    // Detonating Lasher
    SPELL_DETONATE                               = 62598,
    SPELL_FLAME_LASH                             = 62608,

    // Ancient Water Spirit
    SPELL_TIDAL_WAVE                             = 62653,
    SPELL_TIDAL_WAVE_EFFECT                      = 62654,

    // Storm Lasher
    SPELL_LIGHTNING_LASH                         = 62648,
    SPELL_STORMBOLT                              = 62649,

    // Snaplasher
    SPELL_HARDENED_BARK                          = 62664,

    // Ancient Conservator
    SPELL_CONSERVATOR_GRIP                       = 62532,
    SPELL_NATURE_FURY                            = 62589,
    SPELL_SUMMON_PERIODIC                        = 62566,
    SPELL_SPORE_SUMMON_NW                        = 62582, // Not used, triggered by SPELL_SUMMON_PERIODIC
    SPELL_SPORE_SUMMON_NE                        = 62591,
    SPELL_SPORE_SUMMON_SE                        = 62592,
    SPELL_SPORE_SUMMON_SW                        = 62593,

    // Healthly Spore
    SPELL_HEALTHY_SPORE_VISUAL                   = 62538,
    SPELL_GROW                                   = 62559,
    SPELL_POTENT_PHEROMONES                      = 62541,

    // Eonar's Gift
    SPELL_LIFEBINDERS_GIFT                       = 62584,
    SPELL_PHEROMONES                             = 62619,
    SPELL_EONAR_VISUAL                           = 62579,

    // Nature Bomb
    SPELL_NATURE_BOMB                            = 64587,
    SPELL_OBJECT_BOMB                            = 64600,
    SPELL_SUMMON_NATURE_BOMB                     = 64604,

    // Unstable Sun Beam
    SPELL_UNSTABLE_SUN_BEAM                      = 62211,
    SPELL_UNSTABLE_ENERGY                        = 62217,
    SPELL_PHOTOSYNTHESIS                         = 62209,
    SPELL_UNSTABLE_SUN_BEAM_TRIGGERED            = 62243,
    SPELL_FREYA_UNSTABLE_SUNBEAM                 = 62450, // Or maybe 62866?

    // Sun Beam
    SPELL_FREYA_UNSTABLE_ENERGY                  = 62451,
    SPELL_FREYA_UNSTABLE_ENERGY_VISUAL           = 62216,

    // Attuned To Nature spells
    SPELL_ATTUNED_TO_NATURE_2_DOSE_REDUCTION     = 62524,
    SPELL_ATTUNED_TO_NATURE_10_DOSE_REDUCTION    = 62525,
    SPELL_ATTUNED_TO_NATURE_25_DOSE_REDUCTION    = 62521
};

enum FreyaNpcs
{
    NPC_SUN_BEAM                                 = 33170,
    // wave type 1 - 3 mobs
    NPC_ANCIENT_WATER_SPIRIT                     = 33202,
    NPC_STORM_LASHER                             = 32919,
    NPC_SNAPLASHER                               = 32916,
    // wave type 2 - 10 lashers
    NPC_DETONATING_LASHER                        = 32918,
    // wave type 3 - 1 tree
    NPC_ANCIENT_CONSERVATOR                      = 33203,

    NPC_NATURE_BOMB                              = 34129,
    NPC_EONARS_GIFT                              = 33228,
    NPC_HEALTHY_SPORE                            = 33215,
    NPC_UNSTABLE_SUN_BEAM                        = 33050,
    NPC_IRON_ROOTS                               = 33088,
    NPC_STRENGTHENED_IRON_ROOTS                  = 33168,

    OBJECT_NATURE_BOMB                           = 194902
};

enum FreyaActions
{
    ACTION_ELDER_FREYA_EVADE,
    ACTION_ELDER_FREYA_COMBAT,
    ACTION_ELDER_FREYA_KILLED,
    ACTION_MEMBER_RESPAWN,
    ACTION_TRIO_MEMBER_DIED,
};

enum FreyaEvents
{
    // Freya
    EVENT_WAVE                                   = 1,
    EVENT_EONAR_GIFT                             = 2,
    EVENT_NATURE_BOMB                            = 3,
    EVENT_UNSTABLE_ENERGY                        = 4,
    EVENT_STRENGTHENED_IRON_ROOTS                = 5,
    EVENT_GROUND_TREMOR                          = 6,
    EVENT_SUNBEAM                                = 7,
    EVENT_ENRAGE                                 = 8,

    // Elder Stonebark
    EVENT_TREMOR                                 = 9,
    EVENT_BARK                                   = 10,
    EVENT_FISTS                                  = 11,

    // Elder Ironbranch
    EVENT_IMPALE                                 = 12,
    EVENT_IRON_ROOTS                             = 13,
    EVENT_THORN_SWARM                            = 14,

    // Elder Brightleaf
    EVENT_SOLAR_FLARE                            = 15,
    EVENT_UNSTABLE_SUN_BEAM                      = 16,
    EVENT_FLUX                                   = 17
};

enum Misc
{
    WAVE_TIME                                   = 60000, // Normal wave is one minute
    TIME_DIFFERENCE                             = 10000, // If difference between waveTime and WAVE_TIME is bigger then TIME_DIFFERENCE, schedule EVENT_WAVE in 10 seconds
    DATA_GETTING_BACK_TO_NATURE                 = 1,
    DATA_KNOCK_ON_WOOD                          = 2,
    GROUP_REVIVE_ADDS                           = 1,
};

std::array<uint32, 3> const ElderIds = {BOSS_BRIGHTLEAF, BOSS_STONEBARK, BOSS_IRONBRANCH};

std::array<std::array<uint32, 2>, 3> WaveData = {{
    {SAY_SUMMON_LASHERS, SPELL_SUMMON_LASHERS},
    {SAY_SUMMON_TRIO, SPELL_SUMMON_TRIO},
    {SAY_SUMMON_CONSERVATOR, SPELL_SUMMON_ANCIENT_CONSERVATOR},
}};

//! Freya's chest is dynamically spawned on death by different spells.
std::array<std::array<uint32, 4>, 2> ChestSummonSpells = {{
//  0Elder,     1Elder, 2Elder, 3Elder
    {62950,     62952,  62953,  62954}, // 10man
    {62955,     62956,  62957,  62958}, // 25man
}};

int HealingReductionIds[] = {
    13218, 13222, 13223, 13224, 27189, 43461, 57974, 57975, // wound poison
    19434, 20900, 20901, 20902, 20903, 20904, 27065, 49049, 49050, // aimed shot
    12294, 21551, 21552, 21553, 25248, 30330, 47485, 47486, // mortal strike
    56112, // furious attacks
    48301, // Mind Trauma
};

struct boss_freya : public BossAI
{
    boss_freya(Creature* creature) : BossAI(creature, BOSS_FREYA)
    {
        Initialize();
    }

    void Initialize()
    {
        // disable healing reductions
        // we can't apply immunity to SPELL_AURA_MOD_HEALING_PCT
        // because Attuned to Nature has it too
        for(int id : HealingReductionIds)
            me->ApplySpellImmune(0, IMMUNITY_ID, id, true);

        _elderCount = 0;
        _trioCount = 0;
        _trioAlive = 0;
        _attunedToNature = 0;
        _isDeforestation = false;
        _finished = false;

        uint8 i = 0;
        std::generate(_waveOrder.begin(), _waveOrder.end(), [&i]{ return i++; });

        Trinity::Containers::RandomShuffle(_waveOrder);
        _currentWave = 0;
    }

    void Reset() override
    {
        if (_finished)
            return;

        _Reset();
        Initialize();
    }

    void KilledUnit(Unit* who) override
    {
        if (who->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_SLAY);
    }

    void EnterCombat(Unit* /*who*/) override
    {
        _EnterCombat();
        for (auto elder : ElderIds)
        {
            if (!HandleElderAction(elder, ACTION_ELDER_FREYA_COMBAT))
                continue;

            ++_elderCount;

            switch (elder)
            {
                case BOSS_BRIGHTLEAF:
                    scheduler.Schedule(35s, 45s, 0, std::bind(&boss_freya::UnstableEnergy, this, std::placeholders::_1));
                    break;
                case BOSS_STONEBARK:
                    scheduler.Schedule(10s, 20s, 0, std::bind(&boss_freya::GroundTremor, this, std::placeholders::_1));
                    break;
                case BOSS_IRONBRANCH:
                    scheduler.Schedule(10s, 20s, 0, std::bind(&boss_freya::IronRoots, this, std::placeholders::_1));
                    break;
            }
        }

        DoCastSelf(SPELL_TOUCH_OF_EONAR);
        Talk(_elderCount ? SAY_AGGRO_WITH_ELDER : SAY_AGGRO);
        me->CastCustomSpell(SPELL_ATTUNED_TO_NATURE, SPELLVALUE_AURA_STACK, 150, me, true);
    }

    void EnterEvadeMode(EvadeReason why) override
    {
        for (auto elder : ElderIds)
            HandleElderAction(elder, ACTION_ELDER_FREYA_EVADE);

        BossAI::EnterEvadeMode(why);
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(10s, [this](TaskContext wave)
        {
            auto& data = WaveData[_waveOrder[_currentWave++ % _waveOrder.size()]];
            Talk(data[0]);
            Talk(EMOTE_ALLIES_OF_NATURE);
            DoCast(data[1]);

            if (_currentWave < 6) // If set to 6 The Bombs appear during the Final Add wave
                wave.Repeat(60s);
            else
                wave.Schedule(60s, [this](TaskContext nature_bomb)
                {
                    DoCastAOE(SPELL_SUMMON_NATURE_BOMB, true);
                    nature_bomb.Repeat(10s, 12s);
                });
        })
        .Schedule(25s, [this](TaskContext eonar_gift)
        {
            Talk(EMOTE_LIFEBINDERS_GIFT);
            DoCast(me, SPELL_SUMMON_EONAR_GIFT);
            eonar_gift.Repeat(40s, 50s);
        })
        .Schedule(600s, [this](TaskContext /*enrage*/)
        {
            Talk(SAY_BERSERK);
            DoCast(me, SPELL_ENRAGE);
        })
        .Schedule(5s, 15s, [this](TaskContext sunbeam)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
                DoCast(target, SPELL_SUNBEAM);

            sunbeam.Repeat();
        });
    }

    uint32 GetData(uint32 type) const override
    {
        switch (type)
        {
            case DATA_GETTING_BACK_TO_NATURE:
                return _attunedToNature;
            case DATA_KNOCK_ON_WOOD:
                return _elderCount;
        }

        return 0;
    }

    void UpdateAI(uint32 diff) override
    {
        if (me->IsImmuneToPC() || !UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            DoMeleeAttackIfReady();
        });
    }

    void DamageTaken(Unit* who, uint32& damage) override
    {
        if (damage >= me->GetHealth() || _finished)
        {
            damage = 0;
            JustDied(who);
        }
        else if (HealthBelowPct(10))
        {
            // For achievement check, lets do this when we are low, maybe move this to JustDied?
            if (Aura* aura = me->GetAura(SPELL_ATTUNED_TO_NATURE))
                _attunedToNature = aura->GetStackAmount();
        }
    }

    void JustDied(Unit* /*killer*/) override
    {
        if (_finished)
            return;

        _finished = true;
        me->CastSpell(nullptr, ChestSummonSpells[me->GetMap()->GetDifficulty()][_elderCount], true);
        Talk(SAY_DEATH);

        _JustDied();
        me->SetImmuneToPC(true);
        me->RemoveAllAuras();
        me->SetFaction(FACTION_FRIENDLY);
        me->CombatStop(true);
        me->GetThreatManager().ClearAllThreat();
        // this has to be done AFTER faction change
        me->CastSpell(me, SPELL_KNOCK_ON_WOOD_CREDIT, true);
        me->m_Events.AddEvent(new KeeperDespawnEvent(me), me->m_Events.CalculateTime(7500));

        for (auto elder : ElderIds)
            HandleElderAction(elder, ACTION_ELDER_FREYA_KILLED);
    }

    void JustSummoned(Creature* summon) override
    {
        if (summon->GetEntry() == NPC_ANCIENT_WATER_SPIRIT
            || summon->GetEntry() == NPC_STORM_LASHER
            || summon->GetEntry() == NPC_SNAPLASHER)
        {
            _trioGUIDs.push_back(summon->GetGUID());
            ++_trioAlive;
            if (++_trioCount == 6)
                _isDeforestation = true;
        }

        BossAI::JustSummoned(summon);
    }

    void DoAction(int32 action) override
    {
        if (action == ACTION_TRIO_MEMBER_DIED)
            TrioMemberDied();
    }

private:
    void TrioMemberDied()
    {
        --_trioAlive;
        if (_trioAlive == 0)
        {
            if (_isDeforestation)
                instance->DoCastSpellOnPlayers(SPELL_DEFORESTATION_CREDIT);

            scheduler.CancelGroup(GROUP_REVIVE_ADDS);
            for (auto guid : _trioGUIDs)
                if (Creature* summon = ObjectAccessor::GetCreature(*me, guid))
                    summon->KillSelf();

            _trioCount = 0;
        }
        else
        {
            if (_trioAlive == 5)
                scheduler.Schedule(10s, [this](TaskContext /*deforestation_fail*/)
                {
                    _isDeforestation = false;
                });

            scheduler.Schedule(12s, GROUP_REVIVE_ADDS, [this](TaskContext /*revive_adds*/)
            {
                for (auto guid : _trioGUIDs)
                    if (Creature* summon = ObjectAccessor::GetCreature(*me, guid))
                        if (summon->IsAIEnabled)
                            summon->AI()->DoAction(ACTION_MEMBER_RESPAWN);

                _trioAlive = _trioCount;
            });
        }
    }

    bool HandleElderAction(uint32 bossId, int32 action)
    {
        if (Creature* elder = instance->GetCreature(bossId))
        {
            if (elder->IsAlive())
            {
                if (elder->IsAIEnabled)
                    elder->AI()->DoAction(action);

                return true;
            }
        }

        return false;
    }

    // Elder's spells
    void UnstableEnergy(TaskContext context)
    {
        if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
            DoCast(target, SPELL_FREYA_UNSTABLE_SUNBEAM, true);

        context.Repeat();
    }

    void IronRoots(TaskContext context)
    {
        Talk(EMOTE_IRON_ROOTS);
        DoCastAOE(SPELL_ROOTS_FREYA);
        context.Repeat(12s, 20s);
    }

    void GroundTremor(TaskContext context)
    {
        Talk(EMOTE_GROUND_TREMOR);
        DoCastAOE(SPELL_FREYA_GROUND_TREMOR);
        context.Repeat(25s, 28s);
    }

    std::array<uint8, 3> _waveOrder;
    uint8 _elderCount;
    uint8 _attunedToNature;
    uint8 _trioCount;
    uint8 _trioAlive;
    GuidVector _trioGUIDs;
    uint32 _currentWave;
    bool _isDeforestation;
    bool _finished;
};

struct boss_freya_elder : public BossAI
{
    boss_freya_elder(Creature* creature, uint32 bossId, uint32 freyaAura) : BossAI(creature, bossId), _freyaEssence(freyaAura) { }

    void Reset() override
    {
        _Reset();
        if (me->HasAura(SPELL_DRAINED_OF_POWER))
            me->RemoveAurasDueToSpell(SPELL_DRAINED_OF_POWER);
    }

    void KilledUnit(Unit* who) override
    {
        if (who->GetTypeId() == TYPEID_PLAYER)
            Talk(SAY_ELDER_SLAY);
    }

    void JustDied(Unit* /*killer*/) override
    {
        Talk(SAY_ELDER_DEATH);
        _JustDied();
    }

    void EnterCombat(Unit* /*who*/) override
    {
        _EnterCombat();
        if (!me->HasAura(SPELL_DRAINED_OF_POWER))
            Talk(SAY_ELDER_AGGRO);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim() || me->HasAura(SPELL_DRAINED_OF_POWER))
            return;

        scheduler.Update(diff, [this]
        {
            DoMeleeAttackIfReady();
        });
    }

    void DoAction(int32 action) override
    {
        switch (action)
        {
            case ACTION_ELDER_FREYA_COMBAT:
                me->SetLootDisabled(true);
                DoCastSelf(SPELL_DRAINED_OF_POWER);
                DoCastAOE(_freyaEssence);
                DoZoneInCombat();
                break;

            case ACTION_ELDER_FREYA_EVADE:
                EnterEvadeMode(EVADE_REASON_OTHER);
                break;

            case ACTION_ELDER_FREYA_KILLED:
                me->RemoveAllAuras();
                me->CombatStop(true);
                me->GetThreatManager().ClearAllThreat();
                me->DespawnOrUnsummon(10s);
                _JustDied();
                break;
        }
    }

private:
    uint32 _freyaEssence;
};

struct boss_elder_brightleaf : public boss_freya_elder
{
    boss_elder_brightleaf(Creature* creature) : boss_freya_elder(creature, BOSS_BRIGHTLEAF, SPELL_BRIGHTLEAF_ESSENCE) { }

    void ScheduleTasks() override
    {
        scheduler.Schedule(7s, 12s, [this](TaskContext sun_beam)
        {
            me->CastSpell(me, SPELL_UNSTABLE_SUN_BEAM_SUMMON, true);
            sun_beam.Repeat(10s, 15s);
        })
        .Schedule(5s, 7s, [this](TaskContext solar_flare)
        {
            uint8 stackAmount = 0;
            if (Aura* aura = me->GetAura(SPELL_FLUX_AURA))
                stackAmount = aura->GetStackAmount();

            me->CastCustomSpell(SPELL_SOLAR_FLARE, SPELLVALUE_MAX_TARGETS, stackAmount, me, false);
            solar_flare.Repeat(5s, 10s);
        })
        .Schedule(5s, [this](TaskContext flux)
        {
            me->RemoveAurasDueToSpell(SPELL_FLUX_AURA);
            me->AddAura(SPELL_FLUX_AURA, me);
            if (Aura* Flux = me->GetAura(SPELL_FLUX_AURA))
                Flux->SetStackAmount(urand(1, 8));

            flux.Repeat(7500ms);
        });
    }
};

struct boss_elder_stonebark : public boss_freya_elder
{
    boss_elder_stonebark(Creature* creature) : boss_freya_elder(creature, BOSS_STONEBARK, SPELL_STONEBARK_ESSENCE) { }

    void DamageTaken(Unit* who, uint32& damage) override
    {
        if (who == me)
            return;

        if (me->HasAura(SPELL_PETRIFIED_BARK))
        {
            int32 reflect = damage;
            who->CastCustomSpell(who, SPELL_PETRIFIED_BARK_DMG, &reflect, nullptr, nullptr, true);
            damage = 0;
        }
    }

    void ScheduleTasks() override
    {
        scheduler.Schedule(37500ms, 40s, [this](TaskContext bark)
        {
            DoCast(me, SPELL_PETRIFIED_BARK);
            bark.Repeat(30s, 50s);
        })
        .Schedule(25s, 35s, [this](TaskContext fists)
        {
            DoCastVictim(SPELL_FISTS_OF_STONE);
            fists.Repeat(20s, 30s);
        })
        .Schedule(10s, 12s, [this](TaskContext tremor)
        {
            if (!me->HasAura(SPELL_FISTS_OF_STONE))
                DoCastVictim(SPELL_GROUND_TREMOR);

            tremor.Repeat(10s, 20s);
        });
    }
};

struct boss_elder_ironbranch : public boss_freya_elder
{
    boss_elder_ironbranch(Creature* creature) : boss_freya_elder(creature, BOSS_IRONBRANCH, SPELL_IRONBRANCH_ESSENCE) { }

    void ScheduleTasks() override
    {
        scheduler.Schedule(18s, 22s, [this](TaskContext impale)
        {
            DoCastVictim(SPELL_IMPALE);
            impale.Repeat(15s, 25s);
        })
        .Schedule(12s, 17s, [this](TaskContext iron_roots)
        {
            DoCastAOE(SPELL_ROOTS_IRONBRANCH);
            iron_roots.Repeat(10s, 20s);
        })
        .Schedule(5s, [this](TaskContext thorn_swarm)
        {
            DoCastVictim(SPELL_THORN_SWARM);
            thorn_swarm.Repeat(8s, 13s);
        });
    }
};

struct npc_freya_trio : public ScriptedAI
{
    npc_freya_trio(Creature* creature) : ScriptedAI(creature) { }

    void EnterCombat(Unit* /*who*/) override
    {
        ScheduleTasks();
    }

    void DamageTaken(Unit* /*done_by*/, uint32& damage) override
    {
        if (!me->IsEngaged())
            return;

        if (damage < me->GetHealth())
            return;

        damage = 0;
        if (me->IsImmuneToPC())
            return;

        me->RemoveAllAuras();
        me->AttackStop();
        me->SetImmuneToPC(true);
        scheduler.CancelAll();

        DoCastSelf(SPELL_FEIGN_DEATH, true);

        if (Creature* freya = me->GetInstanceScript()->GetCreature(BOSS_FREYA))
            freya->AI()->DoAction(ACTION_TRIO_MEMBER_DIED);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            DoMeleeAttackIfReady();
        });
    }

    void DoAction(int32 action) override
    {
        if (action != ACTION_MEMBER_RESPAWN)
            return;

        me->SetFullHealth();
        if (me->IsImmuneToPC())
        {
            me->RemoveAurasDueToSpell(SPELL_FEIGN_DEATH);
            me->SetImmuneToPC(false);
            me->SetReactState(REACT_AGGRESSIVE);
            ScheduleTasks();
        }
    }

    void JustDied(Unit* /*killer*/) override
    {
        DoCastAOE(SPELL_REMOVE_10STACK, true);
    }

protected:
    virtual void ScheduleTasks() = 0;

    TaskScheduler scheduler;
};

struct npc_ancient_water_spirit : public npc_freya_trio
{
    npc_ancient_water_spirit(Creature* creature) : npc_freya_trio(creature) { }

    void ScheduleTasks() override
    {
        scheduler.Schedule(10s, [this](TaskContext tidal_wave)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 60.0f, true, true))
                DoCast(target, SPELL_TIDAL_WAVE);

            tidal_wave.Repeat(12s, 25s);
		});
    }
};

struct npc_storm_lasher : public npc_freya_trio
{
    npc_storm_lasher(Creature* creature) : npc_freya_trio(creature) { }

    void ScheduleTasks() override
    {
        scheduler.Schedule(10s, [this](TaskContext lightning_lash)
        {
            DoCast(SPELL_LIGHTNING_LASH);
            lightning_lash.Repeat(7s, 14s);
        })
        .Schedule(5s, [this](TaskContext stormbolt)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
                DoCast(target, SPELL_STORMBOLT);

            stormbolt.Repeat(8s, 12s);
        });
    }
};

struct npc_snaplasher : public npc_freya_trio
{
    npc_snaplasher(Creature* creature) : npc_freya_trio(creature) { }

    void ScheduleTasks() override
    {
        DoCastSelf(SPELL_HARDENED_BARK);
    }
};

struct npc_detonating_lasher : public ScriptedAI
{
    npc_detonating_lasher(Creature* creature) : ScriptedAI(creature)
    {
        me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, true);
    }

    void JustAppeared() override
    {
        scheduler.Schedule(5s, [this](TaskContext lash)
        {
            DoCast(SPELL_FLAME_LASH);
            lash.Repeat(5s, 10s);
        })
        .Schedule(7500ms, [this](TaskContext change_target)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true))
            {
                // Switching to other target - modify aggro of new target by 20% from current target's aggro
                AddThreat(target, GetThreat(me->GetVictim()) * 1.2f);
                AttackStart(target);
            }

            change_target.Repeat(5s, 10s);
        });
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            DoMeleeAttackIfReady();
        });
    }

    void JustDied(Unit* /*killer*/) override
    {
        DoCastAOE(SPELL_DETONATE, true);
        DoCastAOE(SPELL_REMOVE_2STACK, true);
    }

private:
    TaskScheduler scheduler;
};

struct npc_ancient_conservator : public ScriptedAI
{
    npc_ancient_conservator(Creature* creature) : ScriptedAI(creature) { }

    void JustAppeared() override
    {
        SummonHealthySpores(2);
    }

    void SummonHealthySpores(uint8 sporesCount)
    {
        for (uint8 n = 0; n < sporesCount; ++n)
        {
            DoCast(SPELL_SUMMON_PERIODIC);
            DoCast(SPELL_SPORE_SUMMON_NE);
            DoCast(SPELL_SPORE_SUMMON_SE);
            DoCast(SPELL_SPORE_SUMMON_SW);
        }
    }

    void EnterCombat(Unit* /*who*/) override
    {
        DoCastSelf(SPELL_CONSERVATOR_GRIP, true);

        scheduler.Schedule(3500ms, [this](TaskContext healthy_spore)
        {
            SummonHealthySpores(1);
            healthy_spore.Repeat(15s, 17.5s);
        })
        .Schedule(7500ms, [this](TaskContext nature_fury)
        {
            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 100.0f, true, true, -SPELL_NATURE_FURY))
                DoCast(target, SPELL_NATURE_FURY);

            nature_fury.Repeat(5s);
        });
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        scheduler.Update(diff, [this]
        {
            DoMeleeAttackIfReady();
        });
    }

    void JustDied(Unit* /*killer*/) override
    {
        DoCastAOE(SPELL_REMOVE_25STACK, true);
    }

private:
    TaskScheduler scheduler;
};

struct npc_sun_beam : public ScriptedAI
{
    npc_sun_beam(Creature* creature) : ScriptedAI(creature)
    {
        _delay = 200;
        SetCombatMovement(false);
        me->SetReactState(REACT_PASSIVE);
    }

    void UpdateAI(uint32 diff)
    {
        if (_delay)
        {
            if (_delay <= diff)
            {
                _delay = 0;
                DoCastAOE(SPELL_FREYA_UNSTABLE_ENERGY_VISUAL, true);
                DoCast(SPELL_FREYA_UNSTABLE_ENERGY);
            }
            else
                _delay -= diff;
        }
    }

private:
    uint32 _delay;
};

struct npc_healthy_spore : public ScriptedAI
{
    npc_healthy_spore(Creature* creature) : ScriptedAI(creature)
    {
        SetCombatMovement(false);
    }

    void JustAppeared() override
    {
        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE | UNIT_FLAG_NON_ATTACKABLE);
        me->SetImmuneToPC(true);
        me->SetReactState(REACT_PASSIVE);
        DoCast(me, SPELL_HEALTHY_SPORE_VISUAL);
        DoCast(me, SPELL_POTENT_PHEROMONES);
        DoCast(me, SPELL_GROW);
        _lifeTimer = urand(22000, 30000);
    }

    void UpdateAI(uint32 diff) override
    {
        if (_lifeTimer <= diff)
        {
            me->RemoveAurasDueToSpell(SPELL_GROW);
            me->DespawnOrUnsummon(2200);
            _lifeTimer = urand(22000, 30000);
        }
        else
            _lifeTimer -= diff;
    }

private:
    uint32 _lifeTimer;
};

struct npc_eonars_gift : public ScriptedAI
{
    npc_eonars_gift(Creature* creature) : ScriptedAI(creature)
    {
        SetCombatMovement(false);

        _lifeBindersGiftTimer = 12000;
        DoCast(me, SPELL_GROW);
        DoCast(me, SPELL_PHEROMONES, true);
        DoCast(me, SPELL_EONAR_VISUAL, true);
    }

    void UpdateAI(uint32 diff) override
    {
        if (_lifeBindersGiftTimer <= diff)
        {
            me->RemoveAurasDueToSpell(SPELL_GROW);
            DoCast(SPELL_LIFEBINDERS_GIFT);
            me->DespawnOrUnsummon(2500);
            _lifeBindersGiftTimer = 12000;
        }
        else
            _lifeBindersGiftTimer -= diff;
    }

private:
    uint32 _lifeBindersGiftTimer;
};

struct npc_nature_bomb : public ScriptedAI
{
    npc_nature_bomb(Creature* creature) : ScriptedAI(creature)
    {
        SetCombatMovement(false);

        bombTimer = urand(8000, 10000);
        DoCast(SPELL_OBJECT_BOMB);
    }

    void UpdateAI(uint32 diff) override
    {
        if (bombTimer <= diff)
        {
            if (GameObject* go = me->FindNearestGameObject(OBJECT_NATURE_BOMB, 1.0f))
            {
                DoCast(me, SPELL_NATURE_BOMB);
                me->RemoveGameObject(go, true);
                me->RemoveFromWorld();
            }

            bombTimer = 10000;
        }
        else
            bombTimer -= diff;
    }

private:
    uint32 bombTimer;
};

struct npc_unstable_sun_beam : public ScriptedAI
{
    struct DespawnEvent : BasicEvent
    {
        DespawnEvent(Creature* owner) : _owner(owner) { }

        bool Execute(uint64 /*eventTime*/, uint32 /*updateTime*/) override
        {
            _owner->CastSpell(nullptr, SPELL_UNSTABLE_ENERGY, true);
            _owner->DisappearAndDie();
            return true;
        }

    private:
        Creature* _owner;
    };

    npc_unstable_sun_beam(Creature* creature) : ScriptedAI(creature)
    {
        SetCombatMovement(false);

        DoCastSelf(SPELL_PHOTOSYNTHESIS);
        DoCastSelf(SPELL_UNSTABLE_SUN_BEAM);
        me->SetReactState(REACT_PASSIVE);
        me->m_Events.AddEvent(new DespawnEvent(me), me->m_Events.CalculateTime(urand(7000, 12000)));
    }

    void SpellHitTarget(Unit* target, SpellInfo const* spell) override
    {
        if (target && spell->Id == SPELL_UNSTABLE_ENERGY)
        {
            target->RemoveAurasDueToSpell(SPELL_UNSTABLE_SUN_BEAM);
            target->RemoveAurasDueToSpell(SPELL_UNSTABLE_SUN_BEAM_TRIGGERED);
        }
    }

private:
    uint32 despawnTimer;
};

struct npc_iron_roots : public ScriptedAI
{
    npc_iron_roots(Creature* creature) : ScriptedAI(creature)
    {
        SetCombatMovement(false);

        me->ApplySpellImmune(0, IMMUNITY_MECHANIC, MECHANIC_GRIP, true);
        me->SetFaction(FACTION_MONSTER);
        me->SetReactState(REACT_PASSIVE);
    }

    void IsSummonedBy(Unit* summoner) override
    {
        if (summoner->GetTypeId() != TYPEID_PLAYER)
            return;

        // Summoner is a player, who should have root aura on self
        _summonerGUID = summoner->GetGUID();
        _checkTimer = 500;
        me->SetFacingToObject(summoner);
        me->EngageWithTarget(summoner);
    }

    void UpdateAI(uint32 diff) override
    {
        if (!UpdateVictim())
            return;

        if (_summonerGUID)
        {
            if (_checkTimer <= diff)
            {
                if (Player* target = ObjectAccessor::GetPlayer(*me, _summonerGUID))
                {
                    if (!target->HasAura(SPELL_ROOTS_IRONBRANCH_TRIGGER) && !target->HasAura(SPELL_ROOTS_FREYA_TRIGGER))
                        me->DespawnOrUnsummon();
                    else
                        _checkTimer = 500;
                }
                else
                    me->DespawnOrUnsummon();
            }
            else
                _checkTimer -= diff;
        }
    }

    void JustDied(Unit* /*killer*/) override
    {
        if (Player* target = ObjectAccessor::GetPlayer(*me, _summonerGUID))
        {
            DoRemoveAura(target, SPELL_ROOTS_IRONBRANCH_TRIGGER);
            DoRemoveAura(target, SPELL_ROOTS_FREYA_TRIGGER);
        }

        me->DespawnOrUnsummon();
    }

private:
    ObjectGuid _summonerGUID;
    uint32 _checkTimer;
};

class spell_freya_attuned_to_nature_dose_reduction : public SpellScript
{
    PrepareSpellScript(spell_freya_attuned_to_nature_dose_reduction);

    void HandleScript(SpellEffIndex /*effIndex*/)
    {
        Aura* aura = GetHitUnit()->GetAura(GetEffectValue());
        if (!aura)
            return;

        switch (GetId())
        {
            case SPELL_ATTUNED_TO_NATURE_2_DOSE_REDUCTION:
                aura->ModStackAmount(-2);
                break;
            case SPELL_ATTUNED_TO_NATURE_10_DOSE_REDUCTION:
                aura->ModStackAmount(-10);
                break;
            case SPELL_ATTUNED_TO_NATURE_25_DOSE_REDUCTION:
                aura->ModStackAmount(-25);
                break;
            default:
                break;
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_freya_attuned_to_nature_dose_reduction::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

class spell_freya_iron_roots_cast : public SpellScript
{
    PrepareSpellScript(spell_freya_iron_roots_cast);

    void FilterTargets(std::list<WorldObject*>& targets)
    {
        Trinity::Containers::RandomResize(targets, 1);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_freya_iron_roots_cast::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
    }
};

class spell_freya_iron_roots : public SpellScript
{
    PrepareSpellScript(spell_freya_iron_roots);

    void HandleSummon(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);

        // Not good at all, but this prevents having roots in a different position than player
        if (Creature* Roots = GetCaster()->SummonCreature(GetSpellInfo()->Effects[effIndex].MiscValue, GetCaster()->GetPosition()))
            GetCaster()->NearTeleportTo(Roots->GetPositionX(), Roots->GetPositionY(), Roots->GetPositionZ(), GetCaster()->GetOrientation());
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_freya_iron_roots::HandleSummon, EFFECT_0, SPELL_EFFECT_SUMMON);
    }
};

class spell_freya_unstable_sun_beam : public SpellScript
{
    PrepareSpellScript(spell_freya_unstable_sun_beam);

    void FilterTargets(std::list<WorldObject*>& targets)
    {
        Trinity::Containers::RandomResize(targets, GetCaster()->GetMap()->Is25ManRaid() ? 3 : 1);
    }

    void Register() override
    {
        OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_freya_unstable_sun_beam::FilterTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENEMY);
    }
};

class spell_freya_summon_lashers : public SpellScript
{
    PrepareSpellScript(spell_freya_summon_lashers);

    void HandleSummon(SpellEffIndex effIndex)
    {
        for (uint8 i = 0; i < 10; ++i)
            GetCaster()->CastSpell(GetCaster(), uint32(GetSpellInfo()->Effects[effIndex].CalcValue()), true);
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_freya_summon_lashers::HandleSummon, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
    }
};

class spell_freya_tidal_wave : public SpellScript
{
    PrepareSpellScript(spell_freya_tidal_wave);

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (GetCaster())
            GetCaster()->CastSpell(nullptr, SPELL_TIDAL_WAVE_EFFECT, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_freya_tidal_wave::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

class achievement_getting_back_to_nature : public AchievementCriteriaScript
{
    public:
        achievement_getting_back_to_nature() : AchievementCriteriaScript("achievement_getting_back_to_nature") { }

        bool OnCheck(Player* /*player*/, Unit* target) override
        {
            return target && target->GetAI()->GetData(DATA_GETTING_BACK_TO_NATURE) >= 25;
        }
};

class achievement_knock_on_wood : public AchievementCriteriaScript
{
   public:
       achievement_knock_on_wood() : AchievementCriteriaScript("achievement_knock_on_wood") { }

       bool OnCheck(Player* /*player*/, Unit* target) override
       {
           return target && target->GetAI()->GetData(DATA_KNOCK_ON_WOOD) >= 1;
       }
};

class achievement_knock_knock_on_wood : public AchievementCriteriaScript
{
   public:
       achievement_knock_knock_on_wood() : AchievementCriteriaScript("achievement_knock_knock_on_wood") { }

       bool OnCheck(Player* /*player*/, Unit* target) override
       {
           return target && target->GetAI()->GetData(DATA_KNOCK_ON_WOOD) >= 2;
       }
};

class achievement_knock_knock_knock_on_wood : public AchievementCriteriaScript
{
   public:
       achievement_knock_knock_knock_on_wood() : AchievementCriteriaScript("achievement_knock_knock_knock_on_wood") { }

       bool OnCheck(Player* /*player*/, Unit* target) override
       {
           return target && target->GetAI()->GetData(DATA_KNOCK_ON_WOOD) == 3;
       }
};

void AddSC_boss_freya()
{
    RegisterUlduarCreatureAI(boss_freya);
    RegisterUlduarCreatureAI(boss_elder_brightleaf);
    RegisterUlduarCreatureAI(boss_elder_ironbranch);
    RegisterUlduarCreatureAI(boss_elder_stonebark);
    RegisterUlduarCreatureAI(npc_ancient_conservator);
    RegisterUlduarCreatureAI(npc_snaplasher);
    RegisterUlduarCreatureAI(npc_storm_lasher);
    RegisterUlduarCreatureAI(npc_ancient_water_spirit);
    RegisterUlduarCreatureAI(npc_detonating_lasher);
    RegisterUlduarCreatureAI(npc_sun_beam);
    RegisterUlduarCreatureAI(npc_nature_bomb);
    RegisterUlduarCreatureAI(npc_eonars_gift);
    RegisterUlduarCreatureAI(npc_healthy_spore);
    RegisterUlduarCreatureAI(npc_unstable_sun_beam);
    RegisterUlduarCreatureAI(npc_iron_roots);
    RegisterSpellScript(spell_freya_attuned_to_nature_dose_reduction);
    RegisterSpellScript(spell_freya_iron_roots_cast);
    RegisterSpellScript(spell_freya_iron_roots);
    RegisterSpellScript(spell_freya_unstable_sun_beam);
    RegisterSpellScript(spell_freya_summon_lashers);
    RegisterSpellScript(spell_freya_tidal_wave);
    new achievement_getting_back_to_nature();
    new achievement_knock_on_wood();
    new achievement_knock_knock_on_wood();
    new achievement_knock_knock_knock_on_wood();
}
