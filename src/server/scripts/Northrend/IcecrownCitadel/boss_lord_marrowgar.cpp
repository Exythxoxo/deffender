/*
 * Copyright (C) 2008-2017 TrinityCore <http://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "icecrown_citadel.h"
#include "InstanceScript.h"
#include "Map.h"
#include "MotionMaster.h"
#include "MoveSplineInit.h"
#include "ObjectAccessor.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "Spell.h"
#include "SpellAuras.h"
#include "SpellScript.h"
#include "TemporarySummon.h"

enum ScriptTexts
{
    SAY_ENTER_ZONE              = 0,
    SAY_AGGRO                   = 1,
    SAY_BONE_STORM              = 2,
    SAY_BONESPIKE               = 3,
    SAY_KILL                    = 4,
    SAY_DEATH                   = 5,
    SAY_BERSERK                 = 6,
    EMOTE_BONE_STORM            = 7,
};

enum Spells
{
    // Lord Marrowgar
    SPELL_BONE_SLICE                        = 69055,
    SPELL_BONE_STORM                        = 69076,
    SPELL_BONE_SPIKE_GRAVEYARD_NORMAL       = 69057,
    SPELL_BONE_SPIKE_GRAVEYARD_BONESTORM    = 73142,
    SPELL_COLDFLAME_NORMAL                  = 69140,
    SPELL_COLDFLAME_BONESTORM               = 72705,

    // Bone Spike
    SPELL_IMPALED                           = 69065,
    SPELL_RIDE_VEHICLE                      = 46598,

    // Coldflame
    SPELL_COLDFLAME_PASSIVE                 = 69145,
    SPELL_COLDFLAME_SUMMON                  = 69147,

    SPELL_TANK_MARKER                       = 69807, // Saber Lash
};

enum Groups
{
    GROUP_SPECIAL           = 1,
};

enum MovementPoints
{
    POINT_TARGET_BONESTORM_PLAYER = 1,
};

enum Actions
{
    ACTION_TALK_ENTER_ZONE  = 1,
    ACTION_TALK_BONE_SPIKE  = 2,
};

enum Misc
{
    BONESTORM_FLAME_COUNT   = 4,
};

const uint32 BoneSpikeSummonId[] = {69062, 72669, 72670};

// Is there function/unit field for this?
#define MARROWGAR_HITBOX 10

class boss_lord_marrowgar : public CreatureScript
{
public:
    boss_lord_marrowgar() : CreatureScript("boss_lord_marrowgar") { }

    struct boss_lord_marrowgarAI : public BossAI
    {
        boss_lord_marrowgarAI(Creature* creature) : BossAI(creature, DATA_LORD_MARROWGAR)
        {
            _baseSpeed = creature->GetSpeedRate(MOVE_RUN);
            _bonestormDuration = me->GetMap()->Is25ManRaid() ? 30000 : 20000;
        }

        void ScheduleTasks() override
        {
            scheduler.Schedule(Seconds(10), [this](TaskContext /*enable_bone_slice*/)
            {
                _boneSlice = true;
            })
            .Schedule(Seconds(15), GROUP_SPECIAL, [this](TaskContext bone_spike_graveyard)
            {
                if (_pauseBoneSpike) {
                    bone_spike_graveyard.Repeat(Milliseconds(500));
                    return;
                }

                uint32 castTime;
                if (_boneStorm)
                {
                    DoCastAOE(SPELL_BONE_SPIKE_GRAVEYARD_BONESTORM);
                    castTime = 1;
                }
                else
                {
                    DoCastAOE(SPELL_BONE_SPIKE_GRAVEYARD_NORMAL);
                    castTime = 3;
                }

                bone_spike_graveyard.Repeat(Seconds(15), Seconds(20));
            })
            .Schedule(Seconds(5), GROUP_SPECIAL, [this](TaskContext coldflame)
            {
                if (_boneStorm)
                    DoCastAOE(SPELL_COLDFLAME_BONESTORM);
                else
                    DoCastAOE(SPELL_COLDFLAME_NORMAL);

                coldflame.Repeat(Seconds(5));
            })
            .Schedule(Seconds(40), Seconds(45), [this](TaskContext warn_bone_storm)
            {
                _boneSlice = false;
                Talk(EMOTE_BONE_STORM);
                me->FinishSpell(CURRENT_MELEE_SPELL, false);
                DoCastSelf(SPELL_BONE_STORM);

                warn_bone_storm.DelayGroup(GROUP_SPECIAL, Seconds(4));
                if (!IsHeroic())
                    _pauseBoneSpike = true;

                warn_bone_storm.Schedule(Milliseconds(3050), [this](TaskContext bone_storm_begin)
                {
                    Talk(SAY_BONE_STORM);
                    _boneStorm = true;

                    me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, true);
                    me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_ATTACK_ME, true);

                    if (Aura* auraBS = me->GetAura(SPELL_BONE_STORM))
                    {
                        auraBS->SetMaxDuration(static_cast<int32>(_bonestormDuration));
                        auraBS->SetDuration(static_cast<int32>(_bonestormDuration));
                    }

                    me->SetSpeedRate(MOVE_RUN, _baseSpeed * 3.0f);
                    me->GetMotionMaster()->Clear();

                    bone_storm_begin.Schedule(Seconds(2), [this](TaskContext bone_storm_move)
                    {
                        Unit* unit = SelectTarget(SELECT_TARGET_RANDOM, 0, 0.0f, true);

                        if (unit) {
                            if (IsHeroic()) // wait with bone spike until we reach target on heroic
                                _pauseBoneSpike = true;

                            me->GetMotionMaster()->MovePoint(POINT_TARGET_BONESTORM_PLAYER, *unit);
                        }

                        if (bone_storm_move.GetRepeatCounter() < 3)
                            bone_storm_move.Repeat(Milliseconds(_bonestormDuration / 4));
                    })
                    .Schedule(Milliseconds(_bonestormDuration + 1), [this](TaskContext bone_storm_end)
                    {
                        me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_MOD_TAUNT, false);
                        me->ApplySpellImmune(0, IMMUNITY_EFFECT, SPELL_EFFECT_ATTACK_ME, false);

                        if (me->GetMotionMaster()->GetCurrentMovementGeneratorType() == POINT_MOTION_TYPE)
                            me->GetMotionMaster()->MovementExpired();

                        me->GetMotionMaster()->MoveChase(me->GetVictim());
                        me->SetSpeedRate(MOVE_RUN, _baseSpeed);

                        _boneStorm = false;

                        if (IsHeroic())
                            _pauseBoneSpike = false;

                        bone_storm_end.Schedule(Seconds(10), [this](TaskContext /*enable_bone_slice*/)
                        {
                            _boneSlice = true;
                        });

                        if (!IsHeroic())
                        {
                            bone_storm_end.Schedule(Seconds(15), [this](TaskContext /*enable_bone_spike*/)
                            {
                                _pauseBoneSpike = false;
                            });
                        }
                    });
                });

                warn_bone_storm.Repeat(Seconds(90), Seconds(95));
            })
            .Schedule(Seconds(600), [this](TaskContext /*enrage*/)
            {
                DoCastSelf(SPELL_BERSERK, true);
                Talk(SAY_BERSERK);
            });
        }

        void Reset() override
        {
            BossAI::Reset();
            me->ApplySpellImmune(0, IMMUNITY_STATE, SPELL_AURA_USE_NORMAL_MOVEMENT_SPEED, true); // judgement of justice
            me->SetSpeedRate(MOVE_RUN, _baseSpeed);
            me->RemoveAurasDueToSpell(SPELL_BONE_STORM);
            me->RemoveAurasDueToSpell(SPELL_BERSERK);
            _boneStorm = false;
            _boneSlice = false;
            _pauseBoneSpike = false;
        }

        void EnterCombat(Unit* who) override
        {
            BossAI::EnterCombat(who);
            Talk(SAY_AGGRO);
        }

        void EnterEvadeMode(EvadeReason why) override
        {
            for (auto unitGuid : summons)
                if (Creature* summon = ObjectAccessor::GetCreature(*me, unitGuid))
                    if (summon->GetEntry() == NPC_BONE_SPIKE)
                        summon->KillSelf(); // needed for aura removal from players

            BossAI::EnterEvadeMode(why);
        }

        void JustDied(Unit* killer) override
        {
            for (auto unitGuid : summons)
                if (Creature* summon = ObjectAccessor::GetCreature(*me, unitGuid))
                    if (summon->GetEntry() == NPC_BONE_SPIKE)
                        summon->KillSelf(); // needed for aura removal from players

            BossAI::JustDied(killer);
            Talk(SAY_DEATH);
        }

        void JustReachedHome() override
        {
            BossAI::JustReachedHome();
            instance->SetBossState(DATA_LORD_MARROWGAR, FAIL);
            instance->SetData(DATA_BONED_ACHIEVEMENT, uint32(true));    // reset
        }

        void KilledUnit(Unit* victim) override
        {
            if (victim->GetTypeId() == TYPEID_PLAYER)
                Talk(SAY_KILL);
        }

        void UpdateAI(uint32 diff) override
        {
            if (!UpdateVictim())
                return;

            scheduler.Update(diff, [this] {
                if (_boneStorm)
                    return;

                if (_boneSlice && !me->GetCurrentSpell(CURRENT_MELEE_SPELL))
                    DoCastVictim(SPELL_BONE_SLICE);

                DoMeleeAttackIfReady();
            });
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type != POINT_MOTION_TYPE || id != POINT_TARGET_BONESTORM_PLAYER)
                return;

            if (IsHeroic())
                _pauseBoneSpike = false;

            // lock movement
            me->GetMotionMaster()->MoveIdle();
        }

        void DoAction(int32 action) override
        {
            switch (action)
            {
                case ACTION_TALK_ENTER_ZONE:
                    Talk(SAY_ENTER_ZONE);
                    break;
                case ACTION_TALK_BONE_SPIKE:
                    Talk(SAY_BONESPIKE);
                    break;
                default:
                    break;
            }
        }

    private:
        uint32 _bonestormDuration;
        float _baseSpeed;
        bool _boneStorm;
        bool _boneSlice;
        bool _pauseBoneSpike;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetIcecrownCitadelAI<boss_lord_marrowgarAI>(creature);
    }
};

class npc_coldflame : public CreatureScript
{
public:
    npc_coldflame() : CreatureScript("npc_coldflame") { }

    struct npc_coldflameAI : public ScriptedAI
    {
        npc_coldflameAI(Creature* creature) : ScriptedAI(creature), _ownerZ(0) { }

        void IsSummonedBy(Unit* owner) override
        {
			_ownerZ = owner->GetPositionZ();

			float ang = Position::NormalizeOrientation(me->GetAngle(owner) + static_cast<float>(M_PI));
			me->SetOrientation(ang);
			DoCast(SPELL_COLDFLAME_SUMMON);

            scheduler.Schedule(Milliseconds(500), [this](TaskContext move)
            {
                DoCast(SPELL_COLDFLAME_SUMMON);
                move.Repeat();
            });
        }

        void UpdateAI(uint32 diff) override
        {
            scheduler.Update(diff);
        }

    private:
        TaskScheduler scheduler;
        float _ownerZ;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetIcecrownCitadelAI<npc_coldflameAI>(creature);
    }
};

class npc_bone_spike : public CreatureScript
{
public:
    npc_bone_spike() : CreatureScript("npc_bone_spike") { }

    struct npc_bone_spikeAI : public ScriptedAI
    {
        npc_bone_spikeAI(Creature* creature) : ScriptedAI(creature), _hasTrappedUnit(false)
        {
            ASSERT(creature->GetVehicleKit());

            SetCombatMovement(false);
            me->SetReactState(REACT_PASSIVE);
        }

        void JustDied(Unit* /*killer*/) override
        {
            if (TempSummon* summ = me->ToTempSummon())
                if (Unit* trapped = summ->GetSummoner())
                    trapped->RemoveAurasDueToSpell(SPELL_IMPALED);

            me->DespawnOrUnsummon();
        }

        void KilledUnit(Unit* victim) override
        {
            me->DespawnOrUnsummon();
            victim->RemoveAurasDueToSpell(SPELL_IMPALED);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            DoCast(summoner, SPELL_IMPALED);
            summoner->CastSpell(me, SPELL_RIDE_VEHICLE, true);
            _hasTrappedUnit = true;

            if (InstanceScript* instance = me->GetInstanceScript())
                if (Creature* marrowgar = ObjectAccessor::GetCreature(*me, instance->GetGuidData(DATA_LORD_MARROWGAR)))
                    marrowgar->AI()->JustSummoned(me);

            scheduler.Schedule(Seconds(8), [this](TaskContext /*fail_boned*/)
            {
                if (InstanceScript* instance = me->GetInstanceScript())
                    instance->SetData(DATA_BONED_ACHIEVEMENT, 0);
            });
        }

        void PassengerBoarded(Unit* passenger, int8 /*seat*/, bool apply) override
        {
            if (!apply)
                return;

            /// @HACK - Change passenger offset to the one taken directly from sniffs
            /// Remove this when proper calculations are implemented.
            /// This fixes healing spiked people
            Movement::MoveSplineInit init(passenger);
            init.DisableTransportPathTransformations();
            init.MoveTo(-0.02206125f, -0.02132235f, 5.514783f, false);
            init.Launch();
        }

        void UpdateAI(uint32 diff) override
        {
            if (!_hasTrappedUnit)
                return;

            scheduler.Update(diff);
        }

    private:
        TaskScheduler scheduler;
        bool _hasTrappedUnit;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return GetIcecrownCitadelAI<npc_bone_spikeAI>(creature);
    }
};

struct ColdflameTargetSelector : public DefaultTargetSelector
{
    ColdflameTargetSelector(Unit* caster) : DefaultTargetSelector(caster, -MARROWGAR_HITBOX, true, true, 0) {}

    bool operator()(WorldObject* obj) const
    {
        return !DefaultTargetSelector::operator()(obj->ToUnit());
    }
};

class spell_marrowgar_coldflame : public SpellScriptLoader
{
public:
    spell_marrowgar_coldflame() : SpellScriptLoader("spell_marrowgar_coldflame") { }

    class spell_marrowgar_coldflame_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_marrowgar_coldflame_SpellScript);

        void SelectTarget(std::list<WorldObject*>& targets)
        {
            if (targets.empty())
                return;

            std::list<WorldObject*> old_targets(targets);
            targets.remove_if(ColdflameTargetSelector(GetCaster()));

            WorldObject* target;
            if (targets.empty())
                target = Trinity::Containers::SelectRandomContainerElement(old_targets);
            else
                target = Trinity::Containers::SelectRandomContainerElement(targets);

            targets.clear();
            targets.push_back(target);
        }

        void HandleScriptEffect(SpellEffIndex effIndex)
        {
            PreventHitDefaultEffect(effIndex);
            GetCaster()->CastSpell(GetHitUnit(), uint32(GetEffectValue()), true);
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_marrowgar_coldflame_SpellScript::SelectTarget, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            OnEffectHitTarget += SpellEffectFn(spell_marrowgar_coldflame_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_marrowgar_coldflame_SpellScript();
    }
};

class spell_marrowgar_coldflame_bonestorm : public SpellScriptLoader
{
public:
    spell_marrowgar_coldflame_bonestorm() : SpellScriptLoader("spell_marrowgar_coldflame_bonestorm") { }

    class spell_marrowgar_coldflame_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_marrowgar_coldflame_SpellScript);

        void HandleScriptEffect(SpellEffIndex effIndex)
        {
            PreventHitDefaultEffect(effIndex);

            for (uint8 i = 0; i < BONESTORM_FLAME_COUNT; ++i)
                GetCaster()->CastSpell(GetHitUnit(), uint32(GetEffectValue() + i), true);
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_marrowgar_coldflame_SpellScript::HandleScriptEffect, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_marrowgar_coldflame_SpellScript();
    }
};

class spell_marrowgar_coldflame_bonestorm_dest : public SpellScriptLoader
{
public:
    spell_marrowgar_coldflame_bonestorm_dest() : SpellScriptLoader("spell_marrowgar_coldflame_bonestorm_dest") { }

    class spell_marrowgar_coldflame_bonestorm_dest_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_marrowgar_coldflame_bonestorm_dest_SpellScript);

        void SelectPosition(SpellDestination& dest)
        {
            Position pos(*GetCaster());

            // calculate position without using hitbox size, default calculation uses it
            GetCaster()->MovePositionToFirstCollision(pos,
                GetSpellInfo()->Effects[EFFECT_0].CalcRadius(GetCaster()),
                GetSpellInfo()->Effects[EFFECT_0].TargetA.CalcDirectionAngle()
            );

            dest.Relocate(pos);
        }

        void Register() override
        {
            if (m_scriptSpellId == 72701)
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_marrowgar_coldflame_bonestorm_dest_SpellScript::SelectPosition,
                    EFFECT_0, TARGET_DEST_CASTER_FRONT_LEFT);
            if (m_scriptSpellId == 72702)
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_marrowgar_coldflame_bonestorm_dest_SpellScript::SelectPosition,
                    EFFECT_0, TARGET_DEST_CASTER_FRONT_RIGHT);
            if (m_scriptSpellId == 72703)
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_marrowgar_coldflame_bonestorm_dest_SpellScript::SelectPosition,
                    EFFECT_0, TARGET_DEST_CASTER_BACK_RIGHT);
            if (m_scriptSpellId == 72704)
                OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_marrowgar_coldflame_bonestorm_dest_SpellScript::SelectPosition,
                    EFFECT_0, TARGET_DEST_CASTER_BACK_LEFT);
        }

        SpellDestination _dest;
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_marrowgar_coldflame_bonestorm_dest_SpellScript();
    }
};

class spell_marrowgar_coldflame_damage : public SpellScriptLoader
{
public:
    spell_marrowgar_coldflame_damage() : SpellScriptLoader("spell_marrowgar_coldflame_damage") { }

    class spell_marrowgar_coldflame_damage_AuraScript : public AuraScript
    {
        PrepareAuraScript(spell_marrowgar_coldflame_damage_AuraScript);

        // this can probably be solved by casting with originalcaster marrowgar, maybe not the impale thing
        bool CanBeAppliedOn(Unit* target)
        {
            if (target->HasAura(SPELL_IMPALED))
                return false;

            if (Aura* aur = target->GetAura(GetId()))
                if (aur->GetOwner() != GetOwner())
                    return false;

            return true;
        }

        void Register() override
        {
            DoCheckAreaTarget += AuraCheckAreaTargetFn(spell_marrowgar_coldflame_damage_AuraScript::CanBeAppliedOn);
        }
    };

    AuraScript* GetAuraScript() const override
    {
        return new spell_marrowgar_coldflame_damage_AuraScript();
    }
};

class BoneSpikeTargetSelector
{
public:
    bool operator()(WorldObject* obj) const
    {
        if (obj->GetTypeId() != TYPEID_PLAYER)
            return false;

        Player* pl = obj->ToPlayer();
        return !pl->HasAura(SPELL_IMPALED) && !pl->HasAura(SPELL_TANK_MARKER);
    }
};

class spell_marrowgar_bone_spike_graveyard : public SpellScriptLoader
{
public:
    spell_marrowgar_bone_spike_graveyard() : SpellScriptLoader("spell_marrowgar_bone_spike_graveyard") { }

    class spell_marrowgar_bone_spike_graveyard_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_marrowgar_bone_spike_graveyard_SpellScript);

        bool Validate(SpellInfo const* /*spell*/) override
        {
            for (auto spellId : BoneSpikeSummonId)
                if (!sSpellMgr->GetSpellInfo(spellId))
                    return false;

            return true;
        }

        void FilterTargets(std::list<WorldObject*>& targets)
        {
            _currentSpell = 0;
            BoneSpikeTargetSelector selector;
            Trinity::Containers::RandomResize(targets, selector, GetCaster()->GetMap()->Is25ManRaid() ? 3 : 1);
        }

        void HandleSpikes(SpellEffIndex)
        {
            ASSERT(_currentSpell < 3);
            if (InstanceScript* instance = GetCaster()->GetInstanceScript())
                if (Creature* marrowgar = ObjectAccessor::GetCreature(*GetCaster(), instance->GetGuidData(DATA_LORD_MARROWGAR)))
                    marrowgar->AI()->DoAction(ACTION_TALK_BONE_SPIKE);

            if (Unit* target = GetHitUnit())
                target->CastSpell(target, BoneSpikeSummonId[_currentSpell]);
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_marrowgar_bone_spike_graveyard_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            OnEffectHitTarget += SpellEffectFn(spell_marrowgar_bone_spike_graveyard_SpellScript::HandleSpikes, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
        }

        size_t _currentSpell;
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_marrowgar_bone_spike_graveyard_SpellScript();
    }
};

class spell_marrowgar_bone_storm : public SpellScriptLoader
{
public:
    spell_marrowgar_bone_storm() : SpellScriptLoader("spell_marrowgar_bone_storm") { }

    class spell_marrowgar_bone_storm_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_marrowgar_bone_storm_SpellScript);

        void RecalculateDamage()
        {
            // Roughly 6k standing on top of him, 4k just inside his hitbox, 2k just outside, and 800 or so at around 30 yards.
            float dist = GetHitUnit()->GetExactDist2d(GetCaster());
            int32 damage = GetHitDamage();

            if (dist <= MARROWGAR_HITBOX)
                damage *= static_cast<int32>(1.0f - (2.0f * dist / 100)); // 100% -> 80%
            else
                damage /= static_cast<int32>(std::sqrt(dist));

            SetHitDamage(damage);
        }

        void Register() override
        {
            OnHit += SpellHitFn(spell_marrowgar_bone_storm_SpellScript::RecalculateDamage);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_marrowgar_bone_storm_SpellScript();
    }
};

class spell_marrowgar_bone_slice : public SpellScriptLoader
{
public:
    spell_marrowgar_bone_slice() : SpellScriptLoader("spell_marrowgar_bone_slice") { }

    class spell_marrowgar_bone_slice_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_marrowgar_bone_slice_SpellScript);

    public:
        spell_marrowgar_bone_slice_SpellScript()
        {
            _targetCount = 0;
        }

    private:
        void CountTargets(std::list<WorldObject*>& targets)
        {
            _targetCount = std::min<uint32>(static_cast<uint32>(targets.size()), GetSpellInfo()->MaxAffectedTargets);
        }

        void SplitDamage()
        {
            // Mark the unit as hit, even if the spell missed or was dodged/parried
            GetCaster()->CastSpell(GetHitUnit(), SPELL_TANK_MARKER, true);

            // This spell can miss all targets
            if (_targetCount != 0)
                SetHitDamage(GetHitDamage() / _targetCount);
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_marrowgar_bone_slice_SpellScript::CountTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            OnHit += SpellHitFn(spell_marrowgar_bone_slice_SpellScript::SplitDamage);
        }

        uint32 _targetCount;
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_marrowgar_bone_slice_SpellScript();
    }
};

class at_lord_marrowgar_entrance : public OnlyOnceAreaTriggerScript
{
public:
    at_lord_marrowgar_entrance() : OnlyOnceAreaTriggerScript("at_lord_marrowgar_entrance") { }

    bool _OnTrigger(Player* player, AreaTriggerEntry const* /*areaTrigger*/) override
    {
        if (InstanceScript* instance = player->GetInstanceScript())
            if (Creature* marrowgar = ObjectAccessor::GetCreature(*player, instance->GetGuidData(DATA_LORD_MARROWGAR)))
                marrowgar->AI()->DoAction(ACTION_TALK_ENTER_ZONE);

        return true;
    }
};

void AddSC_boss_lord_marrowgar()
{
    new boss_lord_marrowgar();
    new npc_coldflame();
    new npc_bone_spike();
    new spell_marrowgar_coldflame();
    new spell_marrowgar_coldflame_bonestorm();
    new spell_marrowgar_coldflame_bonestorm_dest();
    new spell_marrowgar_coldflame_damage();
    new spell_marrowgar_bone_spike_graveyard();
    new spell_marrowgar_bone_storm();
    new spell_marrowgar_bone_slice();
    new at_lord_marrowgar_entrance();
}
