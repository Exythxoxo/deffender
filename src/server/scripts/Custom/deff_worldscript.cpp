#include "AccountPointsMgr.h"
#include "ArmoryMgr.h"
#include "DatabaseEnv.h"
#include "GameTime.h"
#include "ScriptMgr.h"
#include "World.h"

#define WS_VOTERESET_TIME 20008 // next .i u vote reset

struct deff_worldscript : public WorldScript
{
    deff_worldscript() : WorldScript("deff_worldscript") { }

    void OnStartup() override
    {
        _resetTime = sWorld->getWorldState(WS_VOTERESET_TIME);
        if (!_resetTime)
        {
            _resetTime = sWorld->getWorldState(WS_ARENA_DISTRIBUTION_TIME);
            sWorld->setWorldState(WS_VOTERESET_TIME, _resetTime);
        }

        sAccountPointsMgr->Load();
    }

    void OnConfigLoad(bool /*reload*/) override
    {
        sArmoryMgr->Reload();
    }

    void OnUpdate(uint32 /*diff*/) override
    {
        if (GameTime::GetGameTime() > _resetTime)
        {
            CharacterDatabase.DirectExecute("TRUNCATE character_votereset");
            _resetTime = GameTime::GetGameTime() + 7 * DAY;
            sWorld->setWorldState(WS_VOTERESET_TIME, _resetTime);
        }

        sAccountPointsMgr->Update();
    }

private:
    time_t _resetTime;
};

void AddSC_DeffWorldScript()
{
    new deff_worldscript();
}
