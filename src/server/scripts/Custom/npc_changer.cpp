#include "AccountPointsMgr.h"
#include "Creature.h"
#include "Chat.h"
#include "Player.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "WorldSession.h"

enum ChangerEntries : uint8
{
    NPC_CHANGE_CUSTOMIZE,
    NPC_CHANGE_RACE,
    NPC_CHANGE_FACTION,
    NPC_CHANGE_MAX,
};

std::array<std::array<uint32, ACCOUNT_POINTS_MAX>, NPC_CHANGE_MAX> changerPrices = { {
    { 5,  100 },
    { 5,  100 },
    { 10, 200 },
} };

std::array<const char*, NPC_CHANGE_MAX> changerNames = {
    "Customize / Rename",
    "Change race",
    "Change faction",
};

struct npc_changer : public ScriptedAI
{
    npc_changer(Creature* creature) : ScriptedAI(creature) {}

    bool GossipHello(Player* player) override
    {
        if (CanShow(player))
        {
            for (uint8 i = 0; i < ACCOUNT_POINTS_MAX; ++i)
                AddGossipItemFor(player, GOSSIP_ICON_CHAT, AccountPointsNames[i], GOSSIP_SENDER_MAIN + i, GOSSIP_ACTION_INFO_DEF);

            player->PlayerTalkClass->SendGossipMenu(1, me->GetGUID());
        }

        return true;
    }

    bool GossipSelect(Player* player, uint32 sender, uint32 action) override
    {
        if (!CanShow(player))
            return true;

        sender = player->PlayerTalkClass->GetGossipOptionSender(action) - GOSSIP_SENDER_MAIN;
        action = GetGossipActionFor(player, action) - GOSSIP_ACTION_INFO_DEF;
        ClearGossipMenuFor(player);

        if (sender >= ACCOUNT_POINTS_MAX)
            return false;

        if (action)
        {
            HandleAction(player, sender, action - 1);
            CloseGossipMenuFor(player);
        }
        else
            HandleSubmenu(player, sender);

        return true;
    }

private:
    inline ChatHandler GetHandler(Player* plr)
    {
        return ChatHandler(plr->GetSession());
    }

    bool CanShow(Player* player)
    {
        if (player->IsInCombat())
        {
            player->GetSession()->SendNotification("You are in combat!");
            CloseGossipMenuFor(player);
            return false;
        }

        return true;
    }

    inline void HandleSubmenu(Player* player, uint32 sender)
    {
        for (uint8 i = 0; i < NPC_CHANGE_MAX; ++i)
        {
            std::stringstream ss;
            ss  << changerNames[i]
                << " ["
                << changerPrices[i][sender]
                << " "
                << AccountPointsNames[sender]
                << "s]";

            AddGossipItemFor(player, GOSSIP_ICON_MONEY_BAG, ss.str(), GOSSIP_SENDER_MAIN + sender, GOSSIP_ACTION_INFO_DEF + i + 1);
        }

        SendGossipMenuFor(player, 1, me);
    }

    inline void HandleAction(Player* player, uint32 sender, uint32 action)
    {
        if (player->HasAtLoginFlag(AT_LOGIN_CUSTOMIZE) || player->HasAtLoginFlag(AT_LOGIN_CHANGE_RACE) || player->HasAtLoginFlag(AT_LOGIN_CHANGE_FACTION))
        {
            GetHandler(player).PSendSysMessage("You already have some customization option activated, please relog and use it, then you will be able to use another.");
            return;
        }

        AccountPointsInfo info;
        sAccountPointsMgr->GetPoints(AccountPointsType(sender), player->GetSession()->GetAccountId(), info);

        if (info.points < changerPrices[action][sender])
        {
            GetHandler(player).PSendSysMessage("You don't have enough %ss for this service (you need %u but have only %u).", AccountPointsNames[sender], changerPrices[action][sender], info.points);
            return;
        }

        sAccountPointsMgr->SetPoints(AccountPointsType(sender), player->GetSession()->GetAccountId(), info.points - changerPrices[action][sender]);

        switch (action)
        {
            case NPC_CHANGE_CUSTOMIZE:
                player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
                break;

            case NPC_CHANGE_RACE:
                player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                break;

            case NPC_CHANGE_FACTION:
                player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                break;

            default:
                return;
        }

        GetHandler(player).PSendSysMessage("You will be able to customize your character at next login.");
    }
};

void AddSC_npc_changer()
{
    RegisterCreatureAI(npc_changer);
}
