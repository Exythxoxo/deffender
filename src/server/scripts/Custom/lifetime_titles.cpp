 #include "ScriptMgr.h"
 #include "DBCStores.h"
 #include "Player.h"

 class LifetimeTitles : public PlayerScript
 {
 public:
     LifetimeTitles() : PlayerScript("LifetimeTitles") { }
 #define TITLES_TOTAL 14

     const uint32 reqLifetime[TITLES_TOTAL] =
     {
         100,
         500,
         1000,
         2000,
         4000,
         5000,
         6000,
         8000,
         10000,
         15000,
         25000,
         40000,
         45000,
         50000
     };

     void OnKillCredit(Player* killer, Unit* killed) override
     {
         if (killed->GetTypeId() != TYPEID_PLAYER)
             return;

         uint32 lifetime = killer->GetUInt32Value(PLAYER_FIELD_LIFETIME_HONORABLE_KILLS);
         if (lifetime % 100)
             return;

         bool found = false;

         int8 i = TITLES_TOTAL - 1;
         for (; i > 0;)
         {
             if (lifetime >= reqLifetime[--i])
             {
                 found = true;
                 break;
             }
         }

         if (!found)
             return;

         i += 1;

         // get original race to reward correct titles
         ChrRacesEntry const* rEntry = sChrRacesStore.LookupEntry(killer->getOriginalRace());
         // horde player titles start with id 15, alliance with 1, so we need to offset horde id's
         int8 offset = rEntry->TeamID == TEAM_HORDE ? TITLES_TOTAL : 0;
         while (true)
         {
             auto title = sCharTitlesStore.LookupEntry(offset + i);
             if (killer->HasTitle(title))
                 break;

             killer->SetTitle(title);
             if (i < 2)
                 return;
             --i;
         }
     }
 };

 void AddSC_lifetime_titles()
 {
     new LifetimeTitles();
 }
