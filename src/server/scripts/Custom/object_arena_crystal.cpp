#include "ScriptMgr.h"
#include "GameObjectAI.h"
#include "ArenaTeam.h"
#include "Battleground.h"
#include "BattlegroundMgr.h"
#include "Chat.h"
#include "Log.h"
#include "Player.h"
#include "ObjectAccessor.h"
#include "WorldSession.h"

class object_arena_crystal : public GameObjectScript
{
public:
    object_arena_crystal() : GameObjectScript("object_arena_crystal") { }

    struct object_arena_crystalAI : public GameObjectAI
    {
        object_arena_crystalAI(GameObject* creature) : GameObjectAI(creature) { }

        bool GossipHello(Player* plr)
        {
            if (!plr->GetBattleground() || !plr->GetBattleground()->isArena())
                return false;

            if (plr->HasAura(32727))
            {
                if (!plr->isSpectator())
                    plr->m_clicked = true;

                Battleground* bg = plr->GetBattleground();
                uint8 numOfReadyPlrs = 0;
                for (Battleground::BattlegroundPlayerMap::const_iterator itr = bg->GetPlayers().begin(); itr != bg->GetPlayers().end(); ++itr)
                {
                    if (Player* plr = ObjectAccessor::FindPlayer(itr->first))
                    {
                        if (plr->m_clicked == true)
                            ++numOfReadyPlrs;
                    }
                }
                uint8 type = bg->GetArenaType();
                bool all = false;
                uint8 maxplrs = type * 2;
                if (numOfReadyPlrs == maxplrs)
                    all = true;

                if (all)
                    ChatHandler(plr->GetSession()).PSendSysMessage("All players ready");
                else
                    ChatHandler(plr->GetSession()).PSendSysMessage("Players ready: %u / %u.", numOfReadyPlrs, maxplrs);

                if (bg->GetStartDelayTime() <= 15 * IN_MILLISECONDS)
                    plr->GetSession()->SendAreaTriggerMessage("Arena starting soon");
                else
                    plr->GetSession()->SendAreaTriggerMessage("You have been set as ready");
            }
            return true;
        }
    };

    GameObjectAI* GetAI(GameObject* creature) const override
    {
        return new object_arena_crystalAI(creature);
    }
};

void AddSC_object_arena_crystal()
{
    new object_arena_crystal();
}
