ALTER TABLE `game_event_save`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;
