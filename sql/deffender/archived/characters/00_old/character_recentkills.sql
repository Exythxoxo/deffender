CREATE TABLE IF NOT EXISTS `character_recentkills`(
`killer_guid` bigint(20) UNSIGNED NOT NULL,
`victim_guid` bigint(20) NOT NULL, `kills` tinyint(1) UNSIGNED NOT NULL,
PRIMARY KEY (`killer_guid`, `victim_guid`)
)ENGINE=InnoDB;
