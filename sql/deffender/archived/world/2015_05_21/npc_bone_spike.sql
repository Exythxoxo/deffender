-- taunt and death grip immunity for bone spikes @Marrowgar encounter
UPDATE creature_template SET flags_extra = 258, mechanic_immune_mask = mechanic_immune_mask | 32 WHERE entry IN(36619,38233,38459,38460,38711,38970,38971,38972,38712,38973,38974,38975);
