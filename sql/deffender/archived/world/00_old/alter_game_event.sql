ALTER TABLE `game_event`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_arena_seasons`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_battleground_holiday`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_condition`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_creature`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_creature_quest`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_gameobject`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_gameobject_quest`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_model_equip`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_npc_vendor`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_npcflag`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_pool`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_prerequisite`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_quest_condition`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;

ALTER TABLE `game_event_seasonal_questrelation`
MODIFY COLUMN `eventEntry` smallint(5) UNSIGNED NOT NULL COMMENT 'Entry of the game event' FIRST;
