DROP TABLE IF EXISTS `npc_changer`;

CREATE TABLE `npc_changer` (
  `id` smallint(6) NOT NULL,
  `gold` int(11) unsigned DEFAULT NULL,
  `em` int(11) unsigned DEFAULT NULL,
  `allowed` tinyint(1) unsigned DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `npc_changer` VALUES
(0,0,0,0,'zmena pohlavi'),
(1,0,0,0,'zmena rasy'),
(2,0,0,0,'frakce ali -> horda'),
(3,0,0,0,'frakce horda -> ali'),
(4,0,0,0,'zmena pleti'),
(5,0,0,0,'zmena obliceje');
