CREATE TABLE IF NOT EXISTS `account_whitelisted` (
  `id` int(10) unsigned NOT NULL COMMENT 'Account id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Whitelist to override IP bans';