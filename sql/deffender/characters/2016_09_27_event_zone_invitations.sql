CREATE TABLE `event_zone_invitations` 
(
  `guid` INT(15) UNSIGNED NOT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE INDEX `guid_UNIQUE` (`guid` ASC)
);

