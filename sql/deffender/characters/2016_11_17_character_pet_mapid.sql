ALTER TABLE `character_pet` 
ADD COLUMN `last_map_id` INT(10) NOT NULL DEFAULT '-1' AFTER `abdata`,
ADD COLUMN `last_instance_id` INT(10) NOT NULL DEFAULT '-1' AFTER `last_map_id`;
