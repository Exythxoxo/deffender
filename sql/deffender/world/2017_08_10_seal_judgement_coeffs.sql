UPDATE spell_bonus_data SET direct_bonus = 0.25, ap_bonus = 0.16 WHERE entry = 54158; -- SoL, SoW, SoJ
UPDATE spell_bonus_data SET direct_bonus = 0.32, ap_bonus = 0.2 WHERE entry = 20187; -- SoR
UPDATE spell_bonus_data SET direct_bonus = 0.13, ap_bonus = 0.08 WHERE entry = 20467; -- SoC
UPDATE spell_bonus_data SET direct_bonus = 0.22, ap_bonus = 0.14 WHERE entry IN (31804, 53733); -- SoV / SoCorr
