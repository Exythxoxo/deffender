DELETE FROM `creature_text` WHERE `entry` = 37038;
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(37038, 0, 0, 'You... cannot escape us!', 14, 0, 0, 0, 0, 16923, 38562, 0, 'Geist Alarm - SAY_GEIST_ACTIVATE'),
(37038, 0, 1, 'Quickly! We\'ll ambush them from behind!', 14, 0, 0, 0, 0, 16924, 38563, 0, 'Geist Alarm - SAY_GEIST_ACTIVATE'),
(37038, 0, 2, 'The living... here?!', 14, 0, 0, 0, 0, 16925, 38564, 0, 'Geist Alarm - SAY_GEIST_ACTIVATE');

DELETE FROM `spell_script_names` WHERE `ScriptName` = 'spell_icc_geist_alarm';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(70739, 'spell_icc_geist_alarm'),
(70740, 'spell_icc_geist_alarm');

DELETE FROM `creature` WHERE `guid` IN (201276, 201283, 201334, 201376, 201424, 201435, 201437, 201472, 201511, 201521, 201570, 201632);

UPDATE `smart_scripts` SET `event_chance` = 15 WHERE `entryorguid` = 37038;
