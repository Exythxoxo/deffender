UPDATE `creature_template` SET `minlevel` = 82, `maxlevel` = 82, `modelid1` = 19725, `modelid2` = 26767, `InhabitType` = 4, `flags_extra` = 128, `exp` = 2, `scriptname` = 'npc_os_flame_orb' WHERE `entry` = 30702;
DELETE FROM `spell_script_names` WHERE `spell_id` = 57753;
INSERT INTO `spell_script_names` (`spell_id`, `scriptname`) VALUES (57753, 'spell_os_conjure_flame_orb');
