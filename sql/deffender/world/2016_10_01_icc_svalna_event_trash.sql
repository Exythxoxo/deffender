UPDATE creature_template SET ScriptName="npc_ymirjar_frostbinder" WHERE entry=37127;
UPDATE creature_template SET ScriptName="npc_ymirjar_deathbringer" WHERE entry=38125;

DELETE FROM `smart_scripts` WHERE `entryorguid` IN (37127, 38125) AND `source_type` = 0;

DELETE FROM `spell_script_names` WHERE `spell_id` = 71303;
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(71303, 'spell_deathbringer_summon_ymirjar');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceEntry` = 71306;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13,	1,	71306,	0,	0,	29,	0,	38125,	0,	0,	0,	0,	0,	'',	'Twisted Winds - Select Ymirjar Deathbringer');

INSERT INTO creature_formations VALUES
(137765, 137757, 0, 0, 2, 0, 0),
(137765, 137756, 0, 0, 2, 0, 0),
(137765, 137769, 0, 0, 2, 0, 0),
(137765, 137768, 0, 0, 2, 0, 0),

(137774, 137754, 0, 0, 2, 0, 0),

(137770, 137755, 0, 0, 2, 0, 0),
(137770, 137761, 0, 0, 2, 0, 0),
(137770, 137775, 0, 0, 2, 0, 0),
(137770, 137760, 0, 0, 2, 0, 0),
(137770, 137771, 0, 0, 2, 0, 0);
