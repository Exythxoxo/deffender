ALTER TABLE `game_event`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_arena_seasons`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_battleground_holiday`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_condition`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_creature`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_creature_quest`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_gameobject`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_gameobject_quest`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_model_equip`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_npc_vendor`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_npcflag`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_pool`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_prerequisite`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_quest_condition`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

ALTER TABLE `game_event_seasonal_questrelation`
MODIFY COLUMN `eventEntry`  int(10) NOT NULL COMMENT 'Entry of the game event' FIRST ;

-- ids that were removed by making game_event_gameobject eventEntry unsigned
REPLACE INTO `game_event_gameobject` (`eventEntry`, `guid`) VALUES (-4, 26813);

-- ids that were removed by making game_event_creature eventEntry unsigned
REPLACE INTO `game_event_creature` VALUES
(-25,17663),(-25,17905),(-25,17908),(-25,17910),(-25,17911),(-25,18220),(-25,18221),(-25,18235),(-25,18236),(-25,18237),(-25,18238),
(-25,18239),(-25,18291),(-25,18306),(-25,18307),(-25,18309),(-25,18310),(-25,18350),(-25,18354),(-25,18355),(-25,18400),(-25,18404),
(-25,18405),(-25,18407),(-25,18408),(-25,18409),(-25,18411),(-25,18413),(-25,18996),(-25,19002),(-25,19005),(-25,19007),(-25,19008),
(-25,19012),(-25,19015),(-25,19019),(-25,19084),(-25,19219),(-25,19220),(-25,19222),(-25,19223),(-25,19224),(-25,19227),(-25,19228),
(-25,19229),(-25,19316),(-25,19343),(-26,21020),(-26,21022),(-26,21024),(-26,21026),(-26,21034),(-26,21036),(-26,22181),(-26,22188),
(-26,22451),(-26,22473),(-26,24976),(-5,25324),(-5,25489),(-5,25605),(-5,25608),(-5,25609),(-5,26719),(-5,26751),(-5,26752),(-26,38608),
(-3,66156),(-3,66157),(-3,66158),(-3,66428),(-3,66429),(-3,66602),(-12,79648),(-12,79650),(-12,79651),(-12,80341),(-12,80342),(-12,80343),
(-12,80351),(-4,80360),(-4,80362),(-4,80363),(-4,80367);