DELETE FROM `spell_script_names` WHERE `ScriptName` IN ('spell_ignis_scorch', 'spell_leviathan_flames');

INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES
(65044, 'spell_leviathan_flames'),
(65045, 'spell_leviathan_flames');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceEntry` = 62343 AND `SourceGroup` = 1 AND `ConditionTypeOrReference` = 1 AND `ConditionValue1` = 62373;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 1, 62343, 0, 0, 1, 0, 62373, 0, 0, 1, 0, 0, '', 'Heat to Iron Construct without Molten aura');

-- leviathan remove snare immunity
UPDATE `creature_template` SET `mechanic_immune_mask` = 617298815 WHERE `entry` IN (33113, 34003);

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceGroup` = 1 AND `SourceEntry` = 65044 AND `SourceId` = 0;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(13, 1, 65044, 0, 1, 31, 0, 3, 33062, 0, 0, 0, 0, '', 'Spell Flames - target Flame Leviathan vehicles (chopper)'),
(13, 1, 65044, 0, 2, 31, 0, 3, 33109, 0, 0, 0, 0, '', 'Spell Flames - target Flame Leviathan vehicles (demolisher)'),
(13, 1, 65044, 0, 3, 31, 0, 3, 33060, 0, 0, 0, 0, '', 'Spell Flames - target Flame Leviathan vehicles (siege)');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceGroup` = 7 AND `SourceEntry` IN (64482, 65075, 65076, 65077, 65079, 61920);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`,
`ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`,
`ScriptName`, `Comment`) VALUES
(13, 7, 64482, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Life - Only Leviathan area'),
(13, 7, 65075, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Flames - Only Leviathan area'),
(13, 7, 65076, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Storms - Only Leviathan area'),
(13, 7, 65077, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Frost - Only Leviathan area'),
(13, 7, 65079, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Frost - Only Leviathan area'),
(13, 7, 61920, 0, 0, 31, 0, 3, 32867, 0, 0, 0, 0, '', 'Supercharged - Only Assembly of Iron members (Steelbreaker)'),
(13, 7, 61920, 0, 1, 31, 0, 3, 32927, 0, 0, 0, 0, '', 'Supercharged - Only Assembly of Iron members (Molgeim)'),
(13, 7, 61920, 0, 2, 31, 0, 3, 32857, 0, 0, 0, 0, '', 'Supercharged - Only Assembly of Iron members (Brundir)');

DELETE FROM `creature` WHERE `guid` IN (220627, 220628);
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `ScriptName`, `VerifiedBuild`) VALUES
(220627, 34014, 603, 0, 0, 2, 1, 25501, 0, 1999.78, 48.4451, 417.728, 2.68931, 604800, 0, 0, 557800, 0, 0, 0, 0, 0, '', 0),
(220628, 34014, 603, 0, 0, 2, 1, 25501, 0, 1997.79, 44.3652, 417.727, 2.68931, 604800, 0, 0, 557800, 0, 0, 0, 0, 0, '', 0);

UPDATE `creature_template` SET `speed_walk` = 1.66667 WHERE `entry` IN (34014, 34166);

DELETE FROM `creature_formations` WHERE `leaderGUID` = 137496;
INSERT INTO `creature_formations` (`leaderGUID`, `memberGUID`, `dist`, `angle`, `groupAI`, `point_1`, `point_2`) VALUES
(137496, 137496, 0, 0, 515, 0, 0),
(137496, 137490, 5, 300, 515, 0, 0),
(137496, 220627, 5, 340, 515, 0, 0),
(137496, 220628, 5, 20, 515, 0, 0),
(137496, 137491, 5, 60, 515, 0, 0);

-- iron construct not selectable
UPDATE `creature_template` SET `unit_flags` = 33554688 WHERE `entry` = 33191;

-- hodir cache
DELETE FROM `gameobject` WHERE `id` IN (194200, 194201);
INSERT INTO `gameobject` (`id`, `map`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `VerifiedBuild`) VALUES
(194200, 603, 1, 1, 2036.1767, -201.3969, 432.688, 3.328515, 0, 0, 0, 0, 300, 0, 1, 0),
(194201, 603, 2, 1, 2036.1767, -201.3969, 432.688, 3.328515, 0, 0, 0, 0, 300, 0, 1, 0);

-- kologarn arm dead damage
DELETE FROM `spelldifficulty_dbc` WHERE `id` = 63629;
INSERT INTO `spelldifficulty_dbc` (`id`,`spellid0`,`spellid1`) VALUES
(63629, 63629, 63979);

-- ancient gate of the keepers
DELETE FROM `smart_scripts` WHERE `entryorguid` = -137567;
DELETE FROM `gameobject` WHERE `guid` = 55043;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `ScriptName`, `VerifiedBuild`) VALUES
(55043, 194255, 603, 0, 0, 3, 1, 1903.96, 252.687, 418.044, 0, 0, 0, 0, 1, 180, 255, 1, '', 0);

-- vezax & animus taunt immunity
UPDATE `creature_template` SET `flags_extra` = `flags_extra` | 0x100 WHERE `entry` IN (33271, 33524);

-- yogg portal duration
UPDATE `creature_summon_groups` SET `summonTime` = 10000 WHERE `summonerId` = 33280 AND `groupId` IN (1, 2);

-- mimiron magnetic core
UPDATE `creature_template` SET `AIName` = 'SmartAI', `unit_flags` = 33686278 WHERE `entry` = 34068;
DELETE FROM `smart_scripts` WHERE `source_type` = 0 AND `entryorguid` = 34068;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(34068, 0, 0, 0, 11, 0, 100, 0, 0, 0, 0, 0, 11, 64436, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, '');
