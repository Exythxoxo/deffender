DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 13 AND `SourceEntry` IN (64482, 65075, 65076, 65077, 65079);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`,
`ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`,
`ScriptName`, `Comment`) VALUES
(13, 1, 64482, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Life - Only Leviathan area'),
(13, 1, 65075, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Flames - Only Leviathan area'),
(13, 1, 65076, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Storms - Only Leviathan area'),
(13, 1, 65077, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Frost - Only Leviathan area'),
(13, 1, 65079, 0, 0, 23, 0, 4652, 0, 0, 0, 0, 0, '', 'Tower of Frost - Only Leviathan area');
