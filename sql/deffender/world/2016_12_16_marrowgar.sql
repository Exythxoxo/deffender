DELETE FROM spell_script_names WHERE spell_id IN (73142, 73143, 73144, 73145);
INSERT INTO spell_script_names (spell_id, ScriptName) VALUES
(73142, 'spell_marrowgar_bone_spike_graveyard'),
(73143, 'spell_marrowgar_bone_spike_graveyard'),
(73144, 'spell_marrowgar_bone_spike_graveyard'),
(73145, 'spell_marrowgar_bone_spike_graveyard');

UPDATE spell_dbc SET Effect1 = 6, EffectApplyAuraName1 = 4 WHERE Id = 69807;
