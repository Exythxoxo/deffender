CREATE TABLE IF NOT EXISTS `deffender_vendor` (
    `id` SMALLINT NOT NULL,
    `vendor_entry` INT UNSIGNED NOT NULL,
    `parent_id` SMALLINT UNSIGNED NULL DEFAULT NULL,
    `name` VARCHAR(150) NULL DEFAULT NULL,
    `icon` TINYINT UNSIGNED NOT NULL DEFAULT 0,
    `faction` SMALLINT UNSIGNED NOT NULL DEFAULT 0,
    `item_entry` INT UNSIGNED NOT NULL DEFAULT 0,
    `points_type` TINYINT UNSIGNED NULL DEFAULT NULL,
    `price` INT UNSIGNED NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX (`vendor_entry`),
    INDEX (`parent_id`)
) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

ALTER TABLE `dynamic_teleporter` ADD COLUMN `visibility` TINYINT NOT NULL DEFAULT 0;
