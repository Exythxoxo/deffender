DELETE FROM `spell_script_names` WHERE `ScriptName` = 'spell_kologarn_summon_focused_eyebeam';
UPDATE `creature_template` SET `flags_extra` = 128, `ScriptName` = 'npc_kologarn_eyebeam' WHERE `entry` IN (33632, 33802);
DELETE FROM `creature_template_addon` WHERE `entry` IN (33632, 33802);

UPDATE `creature_template` SET `ScriptName` = 'npc_kologarn_arm' WHERE `entry` IN (33933, 32934);
UPDATE `creature_template` SET `ScriptName` = 'npc_kologarn_rubble' WHERE `entry` = 33768;

DELETE FROM `disables` WHERE `sourceType` = 4 AND `entry` IN (10285, 10095, 10286, 10099, 10290, 10133);

-- with open arms
DELETE FROM `achievement_criteria_data` WHERE `criteria_id` IN (10285, 10095) AND `type` = 11;

-- if looks could kill
DELETE FROM `achievement_criteria_data` WHERE `criteria_id` IN (10286, 10099) AND `type` = 11;

-- rubble and roll
DELETE FROM `achievement_criteria_data` WHERE `criteria_id` IN (10290, 10133) AND `type` = 11;

INSERT INTO `achievement_criteria_data` (`criteria_id`, `type`, `value1`, `value2`, `ScriptName`) VALUES
(10285, 11, 0, 0, 'achievement_with_open_arms'),
(10095, 11, 0, 0, 'achievement_with_open_arms'),
(10286, 11, 0, 0, 'achievement_if_looks_could_kill'),
(10099, 11, 0, 0, 'achievement_if_looks_could_kill'),
(10290, 11, 0, 0, 'achievement_rubble_and_roll'),
(10133, 11, 0, 0, 'achievement_rubble_and_roll');

DELETE FROM `creature_text` WHERE `CreatureID` IN (32857, 32867, 32927);

INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`, `Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(32857,	0,	0,	'Whether the world\'s greatest gnats or the world\'s greatest heroes, you are still only mortal.',	14,	0,	100,	0,	0,	15684,	34314,	0,	'Brundir - Aggro'),
(32857,	1,	0,	'A merciful kill!',	14,	0,	100,	0,	0,	15685,	34315,	0,	'Brundir Slay 1'),
(32857,	1,	1,	'HAH!',	14,	0,	100,	0,	0,	15686,	35684,	0,	'Brundir - Slay 2'),
(32857,	2,	0,	'The power of the storm lives on.',	14,	0,	100,	0,	0,	15689,	34318,	0,	'Brundir - Death'),
(32857,	3,	0,	'You rush headlong into the maw of madness!',	14,	0,	100,	0,	0,	15690,	34319,	0,	'Brundir - Encounter defeated'),
(32857,	4,	0,	'This meeting of the Assembly of Iron is adjourned!',	14,	0,	100,	0,	0,	15691,	34335,	0,	'Brundir - Berserk'),
(32857,	5,	0,	'Stand still and stare into the light!',	14,	0,	100,	0,	0,	15687,	33962,	0,	'Brundir - Special'),
(32857,	6,	0,	'Let the storm clouds rise and rain down death from above!',	14,	0,	100,	0,	0,	15688,	34317,	0,	'Brundir - Flight'),
(32857,	7,	0,	'%s begins to Overload!',	41,	0,	100,	0,	0,	0,	34004,	0,	'Brundir - Overload'),

(32867,	0,	0,	'You will not defeat the Assembly of Iron so easily, invaders!',	14,	0,	100,	0,	0,	15674,	34321,	0,	'Steelbreaker - Aggro'),
(32867,	1,	0,	'So fragile and weak!',	14,	0,	100,	0,	0,	15675,	34322,	0,	'Steelbreaker - Slay 1'),
(32867,	1,	1,	'Flesh... such a hindrance.',	14,	0,	100,	0,	0,	15676,	34323,	0,	'Steelbreaker - Slay 2'),
(32867,	2,	0,	'My death only serves to hasten your demise.',	14,	0,	100,	0,	0,	15678,	34325,	0,	'Steelbreaker - Death'),
(32867,	3,	0,	'Impossible...',	14,	0,	100,	0,	0,	15679,	38080,	0,	'Steelbreaker - Encounter defeated'),
(32867,	4,	0,	'This meeting of the Assembly of Iron is adjourned!',	14,	0,	100,	0,	0,	15680,	34335,	0,	'Steelbreaker - Berserk'),
(32867,	5,	0,	'You seek the secrets of Ulduar? Then take them!',	14,	0,	100,	0,	0,	15677,	34324,	0,	'Steelbreaker - Power'),

(32927,	0,	0,	'Nothing short of total decimation will suffice.',	14,	0,	100,	0,	0,	15657,	34328,	0,	'Runemaster Molgeim - Aggro'),
(32927,	1,	0,	'The world suffers yet another insignificant loss.',	14,	0,	100,	0,	0,	15658,	34329,	0,	'Runemaster Molgeim - Slay 1'),
(32927,	1,	1,	'Death is the price of your arrogance.',	14,	0,	100,	0,	0,	15659,	34330,	0,	'Runemaster Molgeim - Slay 2'),
(32927,	2,	0,	'The legacy of storms shall not be undone.',	14,	0,	100,	0,	0,	15662,	34333,	0,	'Runemaster Molgeim - Death'),
(32927,	3,	0,	'What have you gained from my defeat? You are no less doomed, mortals.',	14,	0,	100,	0,	0,	15663,	34334,	0,	'Runemaster Molgeim - Encounter defeated'),
(32927,	4,	0,	'This meeting of the Assembly of Iron is adjourned!',	14,	0,	100,	0,	0,	15664,	34335,	0,	'Runemaster Molgeim - Berserk'),
(32927,	5,	0,	'Decipher this!',	14,	0,	100,	0,	0,	15660,	34331,	0,	'Runemaster Molgeim - Rune of Death'),
(32927,	6,	0,	'Face the lightning\'s surge!',	14,	0,	100,	0,	0,	15661,	34332,	0,	'Runemaster Molgeim - Rune of Summon');
