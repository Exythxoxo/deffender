DELETE FROM `fun_permissions` WHERE `id` IN (2416, 2417);
INSERT INTO `fun_permissions` (`id`, `name`) VALUES (2416, 'Command: currency'), (2417, 'Command: currency change');
DELETE FROM `fun_linked_permissions` WHERE `linkedId` IN (2416, 2417);
INSERT INTO `fun_linked_permissions` (`id`, `linkedId`) VALUES (170, 2416), (180, 2417);
