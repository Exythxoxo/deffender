DROP TABLE IF EXISTS `account_donate`;
CREATE TABLE IF NOT EXISTS `account_donatepoints` (
	`account_id` INT UNSIGNED NOT NULL,
	`points` INT NOT NULL,
	`last_changed` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`account_id`)
);

DELETE FROM `fun_permissions` WHERE `id` = 2415;
INSERT INTO `fun_permissions` (`id`, `name`) VALUES (2415, 'Command: flightpaths');
INSERT INTO `fun_linked_permissions` (`id`, `linkedId`) VALUES (171, 2415);
